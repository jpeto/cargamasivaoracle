package com.mx.santander.cia.app.cargamasiva.vo;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Random;

public class IdGenerator {
	
	/**
	 * Genera Id unico.
	 * @param N/A.
	 * @return Id unico.
	 */
    public static String getId(){
    	Random rnd = new Random();
        Calendar greg = new GregorianCalendar();
        long id = greg.getTimeInMillis() + rnd.nextInt(999);

        return String.valueOf(id);
    }
}
