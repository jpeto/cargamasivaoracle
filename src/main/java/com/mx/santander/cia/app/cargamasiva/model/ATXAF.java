package com.mx.santander.cia.app.cargamasiva.model;

import static com.mx.santander.cia.app.cargamasiva.vo.IdGenerator.getId;

import org.springframework.data.annotation.PersistenceConstructor;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ATXAF {

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String idAplicativoFuncional;
	private String idAplicativoTecnico;
	private int vinculado;
	
	@PersistenceConstructor
	public ATXAF(){
		//this.idATXAF = getId();
	}

	public String getIdAplicativoFuncional() {
		return idAplicativoFuncional;
	}
	public void setIdAplicativoFuncional(String idAplicativoFuncional) {
		this.idAplicativoFuncional = idAplicativoFuncional;
	}
	public int getVinculado() {
		return vinculado;
	}
	public void setVinculado(int vinculado) {
		this.vinculado = vinculado;
	}

	public String getIdAplicativoTecnico() {
		return idAplicativoTecnico;
	}

	public void setIdAplicativoTecnico(String idAplicativoTecnico) {
		this.idAplicativoTecnico = idAplicativoTecnico;
	}
}