package com.mx.santander.cia.app.cargamasiva.properties;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase que controla las propiedades de la aplicacion
 * @author Garcia Aspeitia Luis
 */
public class Propiedades {
	/** Log de la clase */
	private static Logger logger = Logger.getLogger(Propiedades.class.getName());
	/** Instancia unica de la clase */
	private static Propiedades propiedades;
	/** Arvhivo de propiedades */
	private Properties properties;
	
	//////////Header
	public static final String HEADER_FILA = "headerFila";
	public static final String HEADER_IDENTIFICADOR = "headerIdentificador";
	public static final String HEADER_TIPO_MSG = "headerTipoMsg";
	public static final String HEADER_CAMPO = "headerCampo";
	public static final String HEADER_MENSAJE = "headerMensaje";
	
	/////Tipos de mensajes
	public static final String TIPO_MSG_ERROR = "tipoMsgError";
	public static final String TIPO_MSG_ALERTA = "tipoMsgAlerta";
	public static final String TIPO_MSG_REPORTE = "tipoMsgReporte";
	
	///Tipos de identificador
	public static final String TIPO_ID_AF = "tipoIdentificadorAF";
	public static final String TIPO_ID_AT = "tipoIdentificadorAT";
	public static final String TIPO_ID_SERV = "tipoIdentificadorServ";
	
	////////////////////////////////////////////////////////
	//Mensajes
	public static final String MENSAJE_OBLIGATORIO_ERROR = "mensajeObligatorioError";
	public static final String MENSAJE_OBLIGATORIO_ALERTA = "mensajeObligatorioAlerta";
	public static final String MENSAJE_VACIO = "mensajeVacio";
	public static final String MENSAJE_VALIDO_CODIGO = "mensajeValidoCodigo";
	public static final String MENSAJE_VALIDO_CIEN = "mensajeValidoCien";
	public static final String MENSAJE_VALIDO_SERVICE_COMPONENT = "mensajeValidoServiceComponent";
	public static final String MENSAJE_VALIDO_AMBIENTE = "mensajeValidoAmbiente";
	public static final String MENSAJE_VALIDO_FECHA = "mensajeValidoFecha";
	public static final String MENSAJE_VALIDO_FECHA_2 = "mensajeValidoFechaDos";
	public static final String MENSAJE_LONGITUD = "mensajeLongitud";
	public static final String MENSAJE_CATALOGO = "mensajeCatalogo";
	public static final String MENSAJE_CATALOGO_COMBINACION = "mensajeCatalogoCombinacion";
	public static final String MENSAJE_PIPE = "mensajePipe";
	public static final String MENSAJE_AF = "mensajeAF";
	public static final String MENSAJE_AT = "mensajeAT";
	public static final String MENSAJE_AT_REPETIDO = "mensajeATRepetido";
	public static final String MENSAJE_AR = "mensajeAR";
	public static final String MENSAJE_SERV = "mensajeServ";
	public static final String MENSAJE_SERV_AD = "mensajeServAD";
	public static final String MENSAJE_PAIS_MEX = "mensajePaisMex";
	public static final String MENSAJE_SERV_NO_EXISTE = "mensajeServNoExiste";
	
	/**
	 * Crea una nueva instancia de la clase Propiedades
	 */
	private Propiedades (){
		properties = new Properties();
		try {
			InputStream is = Propiedades.class.getResourceAsStream("configuracion.properties");
			Reader reader = new InputStreamReader(is, "UTF-8");
			properties.load(reader);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "ERROR", e);
		}
	}
	
	/**
	 * Obtiene una instancia unica de la clase Propiedades
	 * @return Instancia unica de la clase Propiedades
	 */
	public static Propiedades getInstance(){
		if (propiedades == null){
			propiedades = new Propiedades();
		}
		return propiedades;
	}
	
	/**
	 * Obtiene una propiedad de la clase Propiedades
	 * @param llave LLave a buscar
	 * @return Valor encontrado de la llave
	 */
	public String getPropiedad(String llave){
		return properties.getProperty(llave);
	}
	
	public static String obtenerGetter(String metodo){
		String tmp = "get" + (""+metodo.charAt(0)).toUpperCase() + metodo.substring(1, metodo.length());
		return tmp;
	}
	
	public static String obtenerSetter(String metodo){
		String tmp = "set" + (""+metodo.charAt(0)).toUpperCase() + metodo.substring(1, metodo.length());
		return tmp;
	}
}
