package com.mx.santander.cia.app.cargamasiva.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EntidadMapa {
	@JsonInclude(JsonInclude.Include.NON_NULL)
    private Long entidad;
	private String entidadDesc;
	private Long mapa;
	private String mapaDesc;
	private int estatus;
	

	public int getEstatus() {
		return estatus;
	}
	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}
	public Long getEntidad() {
		return entidad;
	}
	public void setEntidad(Long entidad) {
		this.entidad = entidad;
	}
	public String getEntidadDesc() {
		return entidadDesc;
	}
	public void setEntidadDesc(String entidadDesc) {
		this.entidadDesc = entidadDesc;
	}
	public Long getMapa() {
		return mapa;
	}
	public void setMapa(Long mapa) {
		this.mapa = mapa;
	}
	public String getMapaDesc() {
		return mapaDesc;
	}
	public void setMapaDesc(String mapaDesc) {
		this.mapaDesc = mapaDesc;
	}

	
	

	
	
}
