package com.mx.santander.cia.app.cargamasiva.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BianNivel3 {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long _id;
    private String bian_nivel3;
    private int estatus;

    public Long get_id() {
        return _id;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }

    public String getBian_nivel3() {
        return bian_nivel3;
    }

    public void setBian_nivel3(String bian_nivel3) {
        this.bian_nivel3 = bian_nivel3;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }
}
