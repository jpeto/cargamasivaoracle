package com.mx.santander.cia.app.cargamasiva.vo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import com.mx.santander.cia.app.cargamasiva.model.AplicativoTecnico;
import com.mx.santander.cia.app.cargamasiva.model.ErrorArchivo;
import com.mx.santander.cia.app.cargamasiva.model.InfraestructuraStatus;
import com.mx.santander.cia.app.cargamasiva.model.Servidor;
import com.mx.santander.cia.app.cargamasiva.model.SheetApplicationData;
import com.mx.santander.cia.app.cargamasiva.model.SheetDB;
import com.mx.santander.cia.app.cargamasiva.model.SheetDBP;
import com.mx.santander.cia.app.cargamasiva.model.SheetServidores;
import com.mx.santander.cia.app.cargamasiva.model.Was;
import com.mx.santander.cia.app.cargamasiva.model.Web;
import com.mx.santander.cia.app.cargamasiva.properties.Propiedades;

public class ProcesaCMDB {
	
	List<Servidor> infraestructura = new ArrayList<>();
	List<Was> WAS = new ArrayList<>();
	List<Web> WEB = new ArrayList<>();
	private List<ErrorArchivo> servRechazados = new ArrayList<>();
	
	/**
	 * Mapeo de infraestructura de estructura de archivo a estructura de datos.
	 * @param at Listado de Aplicativos Tecnicos.
	 * @param sad Listado de Application Data de excel.
	 * @param ssl Listado de Servidores de excel.
	 * @param sap Listado de Servidores de Ambientes Previos de excel.
	 * @param db Listado de Base de datos de excel.
	 * @param dbap Listado de Base de datos de Ambientes Previos de excel.
	 * @param WAS Listado de WAS de excel.
	 * @param WASP Listado de WAS de Ambientes Previos de excel.
	 * @param WEB Listado de WEB de excel.
	 * @param WEBP Listado de WEB de Ambientes Previos de excel.
	 * @param infraestructuraStatus Listado de Estatus de Infraestructura.
	 */
	public void obtieneInfraestructura(List<AplicativoTecnico> at,List<SheetApplicationData> sad,List<SheetServidores> ssl,
			                List<SheetServidores> sap,List<SheetDB> db,List<SheetDBP> dbap, List<Was> WAS, List<Was> WASP,
			                List<Web> WEB, List<Web> WEBP, List<InfraestructuraStatus> infraestructuraStatus){
		
		List<SheetApplicationData> aplicationData = new ArrayList<>();
		List<SheetServidores> servidoresP = new ArrayList<>();
		List<SheetServidores> servidoresAP = new ArrayList<>();
		List<Servidor> infraProduccion = new ArrayList<>();
		List<Servidor> infraPrevios = new ArrayList<>();
		List<SheetDB> dbProduccion = new ArrayList<>();
		List<SheetDBP> dbPrevios = new ArrayList<>();
		List<Was> was = new ArrayList<>();
		List<Was> wasp = new ArrayList<>();
		List<Web> web = new ArrayList<>();
		List<Web> webp = new ArrayList<>();
		String appDataKey = "";
		String ciname = "";
		String dbKey = "";
		String dbName = "";
		String wasKey = "";
		String webKey = "";
		
        /*Filtra Application Data
         * */
	    Collections.sort(sad);
	    for(SheetApplicationData as: sad ){
	    	if(as.getOrbisCodeTagNum() != null && as.getSvcId() != null && as.getSvcEnv() != null && 
	    			as.getSvcId().startsWith("SvcC_") && as.getOrbisCodeTagNum().matches("[0-9]*")) {
	    		if (as.getIdAT() != null) {
		    		for(AplicativoTecnico tec : at){
					           if(as.getIdAT().equals(tec.get_id())){
					        	   String listKey = as.getSvcId()+as.getIdAT()+as.getSvcEnv();
					        	   if (!listKey.equals(appDataKey)) {
					        		   appDataKey = listKey;
					        		   aplicationData.add(as);
					        		   break;
					        	   }
					           }
			    	}		    			
	    		} else {
		    		ErrorArchivo errorObligatorio = new ErrorArchivo();
		    		errorObligatorio.setFila(as.getFila()+"");
		    		errorObligatorio.setCampo("Orbis Code_TagNum");
		    		errorObligatorio.setIdentificador("Hoja Application Data");
		    		errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
		    		errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_SERV));
		    		this.servRechazados.add(errorObligatorio);
		    		break;
	    			
	    		}	
	    	} else {
	        	if (as.getOrbisCodeTagNum() == null) {
	        		ErrorArchivo errorObligatorio = new ErrorArchivo();
		    		errorObligatorio.setFila(as.getFila()+"");
		    		errorObligatorio.setIdentificador("Hoja Application Data");
		    		errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
		    		errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ERROR));
		    		errorObligatorio.setCampo("Orbis Code_TagNum");    	
		        	this.servRechazados.add(errorObligatorio);		
	    		} else if (!as.getOrbisCodeTagNum().matches("[0-9]*")) {
	        		ErrorArchivo errorObligatorio = new ErrorArchivo();
		    		errorObligatorio.setFila(as.getFila()+"");
		    		errorObligatorio.setIdentificador("Hoja Application Data");
		    		errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
		    		errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VALIDO_CODIGO));
		    		errorObligatorio.setCampo("Orbis Code_TagNum");    
		    		this.servRechazados.add(errorObligatorio);	
				}
	    		if (as.getSvcId() == null) {
	    			ErrorArchivo errorObligatorio = new ErrorArchivo();
		    		errorObligatorio.setFila(as.getFila()+"");
		    		errorObligatorio.setIdentificador("Hoja Application Data");
		    		errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
		    		errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ERROR));
		    		errorObligatorio.setCampo("Svc ID");
		        	this.servRechazados.add(errorObligatorio);
	    		} else if (!as.getSvcId().startsWith("SvcC_")) {
	    			ErrorArchivo errorObligatorio = new ErrorArchivo();
		    		errorObligatorio.setFila(as.getFila()+"");
		    		errorObligatorio.setIdentificador("Hoja Application Data");
		    		errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
		    		errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VALIDO_SERVICE_COMPONENT));
		    		errorObligatorio.setCampo("Svc ID");
		    		this.servRechazados.add(errorObligatorio);
	    		}
	    		if (as.getSvcEnv() == null) {
	    			ErrorArchivo errorObligatorio = new ErrorArchivo();
		    		errorObligatorio.setFila(as.getFila()+"");
		    		errorObligatorio.setIdentificador("Hoja Application Data");
		    		errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
		    		errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ERROR));
		    		errorObligatorio.setCampo("Svc Env");
		        	this.servRechazados.add(errorObligatorio);
	    		}
			}
	    }
        /*Filtra Servidores Logicos
         * */
	    Collections.sort(ssl);
	    for(SheetServidores sl: ssl) {
	    	if (sl.getSvcId() != null && sl.getCiName() != null && sl.getSvcId().startsWith("SvcC_")) {
	    		if(!sl.getCiName().equals(ciname)){
		    		ciname = sl.getCiName();
		    		servidoresP.add(sl);
		    	} 
	    	} else {
	    		if (sl.getCiName() == null) {
	    			ErrorArchivo errorObligatorio = new ErrorArchivo();
		    		errorObligatorio.setFila(sl.getFila()+"");
		    		errorObligatorio.setIdentificador("Hoja Servidores");
		    		errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
		    		errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ERROR));
		    		errorObligatorio.setCampo("CI Name");    	
		        	this.servRechazados.add(errorObligatorio);		
	    		}
	    		if (sl.getSvcId() == null) {
	    			ErrorArchivo errorObligatorio = new ErrorArchivo();
		    		errorObligatorio.setFila(sl.getFila()+"");
		    		errorObligatorio.setIdentificador("Hoja Servidores");
		    		errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
		    		errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ERROR));
		    		errorObligatorio.setCampo("Svc ID");
		        	this.servRechazados.add(errorObligatorio);
	    		} else if (!sl.getSvcId().startsWith("SvcC_")) {
	    			ErrorArchivo errorObligatorio = new ErrorArchivo();
		    		errorObligatorio.setFila(sl.getFila()+"");
		    		errorObligatorio.setIdentificador("Hoja Servidores");
		    		errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
		    		errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VALIDO_SERVICE_COMPONENT));
		    		errorObligatorio.setCampo("Svc ID");
		    		this.servRechazados.add(errorObligatorio);
	    		}					  
	    	}
	    }
        /*Filtra Servidores Ambientes Previos
         * */
	    Collections.sort(sap);
	    ciname= "";
	    for(SheetServidores sp: sap) {
	    	if (sp.getSvcId() != null && sp.getCiName() != null && sp.getSvcId().startsWith("SvcC_")) {
	    		if(!sp.getCiName().equals(ciname)){
		    		ciname = sp.getCiName();
		    		servidoresAP.add(sp);
		    	} 
	    	} else {
	    		if (sp.getCiName() == null) {
	    			ErrorArchivo errorObligatorio = new ErrorArchivo();
		    		errorObligatorio.setFila(sp.getFila()+"");
		    		errorObligatorio.setIdentificador("Hoja Servidores AP");
		    		errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
		    		errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ERROR));
		    		errorObligatorio.setCampo("CI Name");    	
		        	this.servRechazados.add(errorObligatorio);		
	    		}
	    		if (sp.getSvcId() == null) {
	    			ErrorArchivo errorObligatorio = new ErrorArchivo();
		    		errorObligatorio.setFila(sp.getFila()+"");
		    		errorObligatorio.setIdentificador("Hoja Servidores AP");
		    		errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
		    		errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ERROR));
		    		errorObligatorio.setCampo("SC ID");
		        	this.servRechazados.add(errorObligatorio);
	    		} else if (!sp.getSvcId().startsWith("SvcC_")) {
	    			ErrorArchivo errorObligatorio = new ErrorArchivo();
		    		errorObligatorio.setFila(sp.getFila()+"");
		    		errorObligatorio.setIdentificador("Hoja Servidores AP");
		    		errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
		    		errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VALIDO_SERVICE_COMPONENT));
		    		errorObligatorio.setCampo("SC ID");
		    		this.servRechazados.add(errorObligatorio);
	    		}				  
			  }
	    }
        /*Filtra Bases de Datos
         * */
	    Collections.sort(db);
	    for(SheetDB sdb: db){
	    	if(sdb.getNameLServer() != null && sdb.getScid() != null && sdb.getDbName() != null
	    			 && sdb.getScid().startsWith("SvcC_")){
	    	   String listKey = sdb.getNameLServer()+sdb.getScid();
	    	   if(!listKey.equals(dbKey)){
	    		   dbKey = listKey;
	    		   dbName = sdb.getDbName();
	    		   dbProduccion.add(sdb);
	    	   }else{
	    	      if(!sdb.getDbName().equals(dbName)){
	    	    	  dbName = sdb.getDbName();
	    		      dbProduccion.add(sdb); 
	    	      }
	    	   }
	    	} else {
	    		if (sdb.getNameLServer() == null) {
	    			ErrorArchivo errorObligatorio = new ErrorArchivo();
		    		errorObligatorio.setFila(sdb.getFila()+"");
		    		errorObligatorio.setIdentificador("Hoja Base de Datos");
		    		errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
		    		errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ERROR));
		    		errorObligatorio.setCampo("NAME_LSERVER");    	
		        	this.servRechazados.add(errorObligatorio);		
	    		}
	    		if (sdb.getScid() == null) {
	    			ErrorArchivo errorObligatorio = new ErrorArchivo();
		    		errorObligatorio.setFila(sdb.getFila()+"");
		    		errorObligatorio.setIdentificador("Hoja Base de Datos");
		    		errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
		    		errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ERROR));
		    		errorObligatorio.setCampo("ASSETID_SCOMM");
		        	this.servRechazados.add(errorObligatorio);
	    		} else if (!sdb.getScid().startsWith("SvcC_")) {
	    			ErrorArchivo errorObligatorio = new ErrorArchivo();
		    		errorObligatorio.setFila(sdb.getFila()+"");
		    		errorObligatorio.setIdentificador("Hoja Base de Datos");
		    		errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
		    		errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VALIDO_SERVICE_COMPONENT));
		    		errorObligatorio.setCampo("ASSETID_SCOMM");
		    		this.servRechazados.add(errorObligatorio);
	    		}	
	    		if (sdb.getDbName() == null) {
	    			ErrorArchivo errorObligatorio = new ErrorArchivo();
		    		errorObligatorio.setFila(sdb.getFila()+"");
		    		errorObligatorio.setIdentificador("Hoja Base de Datos");
		    		errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
		    		errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ERROR));
		    		errorObligatorio.setCampo("DB_NAME");
		        	this.servRechazados.add(errorObligatorio);
	    		}
			 }
	    }
	    Collections.sort(dbProduccion);
		dbKey = "";
		dbName = "";
        /*Filtra DB Ambientes Previos
         * */
	    Collections.sort(dbap);
	    for(SheetDBP sdbp: dbap){
	    	if(sdbp.getNameLServer() != null && sdbp.getScid() != null && sdbp.getDbName() != null
	    			 && sdbp.getScid().startsWith("SvcC_")){
	    	   String listKey = sdbp.getNameLServer()+sdbp.getScid();
	    	   if(!listKey.equals(dbKey)){
	    		   dbKey = listKey;
	    		   dbName = sdbp.getDbName();
	    		   dbPrevios.add(sdbp);
	    	   }else{
	    	      if(!sdbp.getDbName().equals(dbName)){
	    	    	  dbName = sdbp.getDbName();
	    	    	  dbPrevios.add(sdbp); 
	    	      }
	    	   }
	    	} else {
	    		if (sdbp.getNameLServer() == null) {
	    			ErrorArchivo errorObligatorio = new ErrorArchivo();
		    		errorObligatorio.setFila(sdbp.getFila()+"");
		    		errorObligatorio.setIdentificador("Hoja Base de Datos AP");
		    		errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
		    		errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ERROR));
		    		errorObligatorio.setCampo("NAME_LSERVER");    	
		        	this.servRechazados.add(errorObligatorio);		
	    		}
	    		if (sdbp.getScid() == null) {
	    			ErrorArchivo errorObligatorio = new ErrorArchivo();
		    		errorObligatorio.setFila(sdbp.getFila()+"");
		    		errorObligatorio.setIdentificador("Hoja Base de Datos AP");
		    		errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
		    		errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ERROR));
		    		errorObligatorio.setCampo("ASSETID_SCOMM");
		        	this.servRechazados.add(errorObligatorio);
	    		} else if (!sdbp.getScid().startsWith("SvcC_")) {
	    			ErrorArchivo errorObligatorio = new ErrorArchivo();
		    		errorObligatorio.setFila(sdbp.getFila()+"");
		    		errorObligatorio.setIdentificador("Hoja Base de Datos AP");
		    		errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
		    		errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VALIDO_SERVICE_COMPONENT));
		    		errorObligatorio.setCampo("ASSETID_SCOMM");
		    		this.servRechazados.add(errorObligatorio);
	    		}	
	    		if (sdbp.getScid() == null) {
	    			ErrorArchivo errorObligatorio = new ErrorArchivo();
		    		errorObligatorio.setFila(sdbp.getFila()+"");
		    		errorObligatorio.setIdentificador("Hoja Base de Datos AP");
		    		errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
		    		errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ERROR));
		    		errorObligatorio.setCampo("DB_NAME");
		        	this.servRechazados.add(errorObligatorio);
	    		}
			 }
	    }
	    Collections.sort(dbPrevios);
	    
	    /*Filtra WAS
         * */
	    Collections.sort(WAS);
		for(Was wprod: WAS){
			if(wprod.getScid() != null && wprod.getNombreServidor() != null && wprod.getNombreWAS()!= null
					 && wprod.getScid().startsWith("SvcC_")){
		       String llaveLi = wprod.getNombreServidor()+wprod.getScid()+wprod.getNombreWAS();
			   if(!wasKey.equals(llaveLi)) {
			      wasKey = llaveLi;
			      was.add(wprod);
			   }
			} else {
	    		if (wprod.getNombreServidor() == null) {
	    			ErrorArchivo errorObligatorio = new ErrorArchivo();
		    		errorObligatorio.setFila(wprod.getFila()+"");
		    		errorObligatorio.setIdentificador("Hoja WAS");
		    		errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
		    		errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ERROR));
		    		errorObligatorio.setCampo("LS_NAME");    	
		        	this.servRechazados.add(errorObligatorio);		
	    		}
	    		if (wprod.getScid() == null) {
	    			ErrorArchivo errorObligatorio = new ErrorArchivo();
		    		errorObligatorio.setFila(wprod.getFila()+"");
		    		errorObligatorio.setIdentificador("Hoja WAS");
		    		errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
		    		errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ERROR));
		    		errorObligatorio.setCampo("SC_ID");
		        	this.servRechazados.add(errorObligatorio);
	    		} else if (!wprod.getScid().startsWith("SvcC_")) {
	    			ErrorArchivo errorObligatorio = new ErrorArchivo();
		    		errorObligatorio.setFila(wprod.getFila()+"");
		    		errorObligatorio.setIdentificador("Hoja WAS");
		    		errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
		    		errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VALIDO_SERVICE_COMPONENT));
		    		errorObligatorio.setCampo("SC_ID");
		    		this.servRechazados.add(errorObligatorio);
	    		}	
	    		if (wprod.getNombreWAS() == null) {
	    			ErrorArchivo errorObligatorio = new ErrorArchivo();
		    		errorObligatorio.setFila(wprod.getFila()+"");
		    		errorObligatorio.setIdentificador("Hoja WAS");
		    		errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
		    		errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ERROR));
		    		errorObligatorio.setCampo("WAS_NAME");    	
		        	this.servRechazados.add(errorObligatorio);		
	    		}
			}
		}
		/*Filtra WAS Previos
         * */
		Collections.sort(WASP);
		wasKey = "";
		for(Was wprev: WASP){
			if(wprev.getScid() != null && wprev.getNombreServidor() != null && wprev.getNombreWAS()!= null
					 && wprev.getScid().startsWith("SvcC_")){
			   String llaveLi = wprev.getNombreServidor()+wprev.getScid()+wprev.getNombreWAS();
		       if(!wasKey.equals(llaveLi)) {
			      wasKey = llaveLi;	
			      wasp.add(wprev);
		       }
			} else {
	    		if (wprev.getNombreServidor() == null) {
	    			ErrorArchivo errorObligatorio = new ErrorArchivo();
		    		errorObligatorio.setFila(wprev.getFila()+"");
		    		errorObligatorio.setIdentificador("Hoja WAS AP");
		    		errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
		    		errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ERROR));
		    		errorObligatorio.setCampo("LS_NAME");    	
		        	this.servRechazados.add(errorObligatorio);		
	    		}
	    		if (wprev.getScid() == null) {
	    			ErrorArchivo errorObligatorio = new ErrorArchivo();
		    		errorObligatorio.setFila(wprev.getFila()+"");
		    		errorObligatorio.setIdentificador("Hoja WAS AP");
		    		errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
		    		errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ERROR));
		    		errorObligatorio.setCampo("SC_ID");
		        	this.servRechazados.add(errorObligatorio);
	    		} else if (!wprev.getScid().startsWith("SvcC_")) {
	    			ErrorArchivo errorObligatorio = new ErrorArchivo();
		    		errorObligatorio.setFila(wprev.getFila()+"");
		    		errorObligatorio.setIdentificador("Hoja WAS AP");
		    		errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
		    		errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VALIDO_SERVICE_COMPONENT));
		    		errorObligatorio.setCampo("SC_ID");
		    		this.servRechazados.add(errorObligatorio);
	    		}	
	    		if (wprev.getNombreWAS() == null) {
	    			ErrorArchivo errorObligatorio = new ErrorArchivo();
		    		errorObligatorio.setFila(wprev.getFila()+"");
		    		errorObligatorio.setIdentificador("Hoja WAS AP");
		    		errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
		    		errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ERROR));
		    		errorObligatorio.setCampo("WAS_NAME");    	
		        	this.servRechazados.add(errorObligatorio);		
	    		}
			}
		}
	    /*Filtra WEB
         * */
	    Collections.sort(WEB);
		for(Web webprod: WEB){
			if(webprod.getScid() != null && webprod.getNombreServidor() != null && webprod.getNombreWEB()!= null
					 && webprod.getScid().startsWith("SvcC_")){
			   String llaveLi = webprod.getNombreServidor()+webprod.getScid()+webprod.getNombreWEB();
			   if(!webKey.equals(llaveLi)) {
				  webKey = llaveLi;
			      web.add(webprod);
			   }
			} else {
	    		if (webprod.getNombreServidor() == null) {
	    			ErrorArchivo errorObligatorio = new ErrorArchivo();
		    		errorObligatorio.setFila(webprod.getFila()+"");
		    		errorObligatorio.setIdentificador("Hoja WEB");
		    		errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
		    		errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ERROR));
		    		errorObligatorio.setCampo("LS_NAME");    	
		        	this.servRechazados.add(errorObligatorio);		
	    		}
	    		if (webprod.getScid() == null) {
	    			ErrorArchivo errorObligatorio = new ErrorArchivo();
		    		errorObligatorio.setFila(webprod.getFila()+"");
		    		errorObligatorio.setIdentificador("Hoja WEB");
		    		errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
		    		errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ERROR));
		    		errorObligatorio.setCampo("SC_ID");
		        	this.servRechazados.add(errorObligatorio);
	    		} else if (!webprod.getScid().startsWith("SvcC_")) {
	    			ErrorArchivo errorObligatorio = new ErrorArchivo();
		    		errorObligatorio.setFila(webprod.getFila()+"");
		    		errorObligatorio.setIdentificador("Hoja WEB");
		    		errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
		    		errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VALIDO_SERVICE_COMPONENT));
		    		errorObligatorio.setCampo("SC_ID");
		    		this.servRechazados.add(errorObligatorio);
	    		}	
	    		if (webprod.getNombreWEB() == null) {
	    			ErrorArchivo errorObligatorio = new ErrorArchivo();
		    		errorObligatorio.setFila(webprod.getFila()+"");
		    		errorObligatorio.setIdentificador("Hoja WEB");
		    		errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
		    		errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ERROR));
		    		errorObligatorio.setCampo("WEB Name");    	
		        	this.servRechazados.add(errorObligatorio);		
	    		}
			}
		}
		/*Filtra WEB Previos
         * */
		Collections.sort(WEBP);
		webKey = "";
		for(Web webprev: WEBP){
			if(webprev.getScid() != null && webprev.getNombreServidor() != null && webprev.getNombreWEB()!= null
					 && webprev.getScid().startsWith("SvcC_")){
			   String llaveLi = webprev.getNombreServidor()+webprev.getScid()+webprev.getNombreWEB();
			   if(!webKey.equals(llaveLi)) {
				  webKey = llaveLi;
			      webp.add(webprev);
			   }
			} else {
	    		if (webprev.getNombreServidor() == null) {
	    			ErrorArchivo errorObligatorio = new ErrorArchivo();
		    		errorObligatorio.setFila(webprev.getFila()+"");
		    		errorObligatorio.setIdentificador("Hoja WEB AP");
		    		errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
		    		errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ERROR));
		    		errorObligatorio.setCampo("LS_NAME");    	
		        	this.servRechazados.add(errorObligatorio);		
	    		}
	    		if (webprev.getScid() == null) {
	    			ErrorArchivo errorObligatorio = new ErrorArchivo();
		    		errorObligatorio.setFila(webprev.getFila()+"");
		    		errorObligatorio.setIdentificador("Hoja WEB AP");
		    		errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
		    		errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ERROR));
		    		errorObligatorio.setCampo("SC_ID");
		        	this.servRechazados.add(errorObligatorio);
	    		} else if (!webprev.getScid().startsWith("SvcC_")) {
	    			ErrorArchivo errorObligatorio = new ErrorArchivo();
		    		errorObligatorio.setFila(webprev.getFila()+"");
		    		errorObligatorio.setIdentificador("Hoja WEB AP");
		    		errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
		    		errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VALIDO_SERVICE_COMPONENT));
		    		errorObligatorio.setCampo("SC_ID");
		    		this.servRechazados.add(errorObligatorio);
	    		}	
	    		if (webprev.getNombreWEB() == null) {
	    			ErrorArchivo errorObligatorio = new ErrorArchivo();
		    		errorObligatorio.setFila(webprev.getFila()+"");
		    		errorObligatorio.setIdentificador("Hoja WEB AP");
		    		errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
		    		errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ERROR));
		    		errorObligatorio.setCampo("WEB Name");    	
		        	this.servRechazados.add(errorObligatorio);		
	    		}
			}
		}
		
		for(SheetApplicationData data : aplicationData){	        
		   if(data.getSvcEnv().equals("Production")){
			  boolean existeServ = false;
			  for(SheetServidores s: servidoresP){
				  if(s.getSvcId().equals(data.getSvcId())){
					existeServ = true;
					Servidor i = new Servidor();
					i.setIdAplicativoTecnico(data.getIdAT());
					i.setScid(s.getSvcId());
					i.setCompania(s.getLsCompany());
					i.setNombreServidor(s.getCiName());
					i.setAmbienteServidor(s.getFuntionalEnviroment());
					i.setFuncionServidor(s.getLsFunction());
					i.setPlataforma(s.getPlatform());
					i.setSo(s.getOperatingSystem());
				    i.setEolServidor(s.getEol());				    
				    for (InfraestructuraStatus infst: infraestructuraStatus) {
				    	if (s.getLsStatus() != null && infst.getEstatus().equals(s.getLsStatus())) {
				    		i.setEstatusServidor(infst.get_id());
				    		break;
				    	}
				    }
				    
					String llaveSer = s.getCiName() + s.getSvcId();
					
					for(SheetDB ldb: dbProduccion){
						 String ldbKey = ldb.getNameLServer()+ldb.getScid(); 
						  if(ldbKey.equals(llaveSer)){
				    	     i.setNombreDB(ldb.getDbName());
				    	     i.setAmbienteDB(ldb.getFuntionalEnviroment());
				    	     i.setManejadorDB(ldb.getDbSystem());
				    	     i.setVersionDB(ldb.getDbVersion());
				    	     i.setEolDB(ldb.getEol());
				    	     for (InfraestructuraStatus infst: infraestructuraStatus) {
						    	i.setEstatusDB(infst.get_id());
						    	if (ldb.getStatusDb() != null && infst.getEstatus().equals(ldb.getStatusDb())) {
						    		i.setEstatusDB(infst.get_id());
						    		break;
						    	}
						     }
				    	     for (ErrorArchivo error: ldb.getErroresMapeo()) {
								error.setFila(ldb.getFila()+"");
								error.setIdentificador("Hoja Base de datos");
								this.servRechazados.add(error);
							 }
						  }	
				    }
					
					for(Was w: was){
					      String lwas = w.getNombreServidor()+w.getScid(); 
						  if(lwas.equals(llaveSer)){
						     this.WAS.add(w);
							 for (ErrorArchivo error: w.getErroresMapeo()) {
									error.setFila(w.getFila()+"");
									error.setIdentificador("Hoja WAS");
									this.servRechazados.add(error);
								}	
						  }
					}
					for(Web wb: web){
						String lweb = wb.getNombreServidor()+wb.getScid(); 
							if(lweb.equals(llaveSer)){
							this.WEB.add(wb);
							 for (ErrorArchivo error: wb.getErroresMapeo()) {
									error.setFila(wb.getFila()+"");
									error.setIdentificador("Hoja WEB");
									this.servRechazados.add(error);
								}	
						}
					}	
					infraProduccion.add(i);
					for (ErrorArchivo error: s.getErroresMapeo()) {
						error.setFila(s.getFila()+"");
						error.setIdentificador("Hoja Servidores");
						this.servRechazados.add(error);
					}
				  }
			  } 
			  if (!existeServ) {//error de serv de appData no esta en servidores
				  ErrorArchivo errorServNoExiste = new ErrorArchivo();
				  errorServNoExiste.setFila(data.getFila()+"");
				  errorServNoExiste.setCampo("N/A");
				  errorServNoExiste.setIdentificador("Hoja Application Data");
				  errorServNoExiste.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
				  errorServNoExiste.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_SERV_AD) + data.getSvcId());
		          this.servRechazados.add(errorServNoExiste);
			  }
		   } else if(data.getSvcEnv().equals("QA / Certification") || data.getSvcEnv().equals("Development") || data.getSvcEnv().equals("Pre-Production")){
			   boolean existeServ = false;
			   for(SheetServidores s: servidoresAP){
				  if(s.getSvcId().equals(data.getSvcId())){
					existeServ = true;
					Servidor i = new Servidor();
					i.setIdAplicativoTecnico(data.getIdAT());
				    i.setScid(s.getSvcId());
				    i.setCompania(s.getLsCompany());
				    i.setNombreServidor(s.getCiName());
				    i.setAmbienteServidor(s.getFuntionalEnviroment());
				    i.setFuncionServidor(s.getLsFunction());
				    i.setPlataforma(s.getPlatform());
				    i.setSo(s.getOperatingSystem());
				    i.setEolServidor(s.getEol());
				    for (InfraestructuraStatus infst: infraestructuraStatus) {
				    	i.setEstatusServidor(infst.get_id());
				    	if (s.getLsStatus() != null && infst.getEstatus().equals(s.getLsStatus())) {
				    		i.setEstatusServidor(infst.get_id());
				    		break;
				    	}
				    }
				    
					String llaveSer = s.getCiName() + s.getSvcId();
					
					for(SheetDBP ldbp: dbPrevios){
						 String ldbKey = ldbp.getNameLServer()+ldbp.getScid(); 
						  if(ldbKey.equals(llaveSer)){
				    	     i.setNombreDB(ldbp.getDbName());
				    	     i.setAmbienteDB(ldbp.getFuntionalEnviroment());
				    	     i.setManejadorDB(ldbp.getDbSystem());
				    	     i.setVersionDB(ldbp.getDbVersion());
				    	     i.setEolDB(ldbp.getEol());
				    	     for (InfraestructuraStatus infst: infraestructuraStatus) {
						    	i.setEstatusDB(infst.get_id());
						    	if (ldbp.getStatusDb() != null && infst.getEstatus().equals(ldbp.getStatusDb())) {
						    		i.setEstatusDB(infst.get_id());
						    		break;
						    	}
						     }
				    	     for (ErrorArchivo error: ldbp.getErroresMapeo()) {
									error.setFila(ldbp.getFila()+"");
									error.setIdentificador("Hoja Base de datos AP");
									this.servRechazados.add(error);
							 }	
				    	     
						  }								  
				   }
				   for(Was wp: wasp){
				      String lwas = wp.getNombreServidor()+wp.getScid(); 
					  if(lwas.equals(llaveSer)){
					     this.WAS.add(wp);
						 for (ErrorArchivo error: wp.getErroresMapeo()) {
								error.setFila(wp.getFila()+"");
								error.setIdentificador("Hoja WAS AP");
								this.servRechazados.add(error);
							}	
					  }
				   }
				   for(Web wbp: webp){
				      String lwebp = wbp.getNombreServidor()+wbp.getScid(); 
					  if(lwebp.equals(llaveSer)){
					     this.WEB.add(wbp);
						 for (ErrorArchivo error: wbp.getErroresMapeo()) {
								error.setFila(wbp.getFila()+"");
								error.setIdentificador("Hoja WEB AP");
								this.servRechazados.add(error);
							}	
					  }
				   }
				   infraPrevios.add(i);
				   for (ErrorArchivo error: s.getErroresMapeo()) {
						error.setFila(s.getFila()+"");
						error.setIdentificador("Hoja Servidores AP");
						this.servRechazados.add(error);
					}
				  }
			  } 
			  if (!existeServ) {//error de serv de appData no esta en servidores
				  ErrorArchivo errorServNoExiste = new ErrorArchivo();
				  errorServNoExiste.setFila(data.getFila()+"");
				  errorServNoExiste.setCampo("N/A");
				  errorServNoExiste.setIdentificador("Hoja Application Data");
				  errorServNoExiste.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
				  errorServNoExiste.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_SERV_AD) + data.getSvcId());
				  this.servRechazados.add(errorServNoExiste);
			  }
		   } else {
				ErrorArchivo errorAmbienteNoExiste = new ErrorArchivo();
				errorAmbienteNoExiste.setFila(data.getFila()+"");
				errorAmbienteNoExiste.setCampo("Svc Env");
				errorAmbienteNoExiste.setIdentificador("Hoja Application Data");
				errorAmbienteNoExiste.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
				errorAmbienteNoExiste.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VALIDO_AMBIENTE));
	        	this.servRechazados.add(errorAmbienteNoExiste);
		   }
		}
		
		this.infraestructura.addAll(infraProduccion);
		this.infraestructura.addAll(infraPrevios);

	}
	

	public List<Servidor> getInfraestructura() {
		return infraestructura;
	}

	public void setInfraestructura(List<Servidor> infraestructura) {
		this.infraestructura = infraestructura;
	}

	public List<Was> getWAS() {
		return WAS;
	}

	public void setWAS(List<Was> wAS) {
		WAS = wAS;
	}

	public List<Web> getWEB() {
		return WEB;
	}


	public void setWEB(List<Web> wEB) {
		WEB = wEB;
	}

	public List<ErrorArchivo> getServRechazados() {
		return servRechazados;
	}


	public void setServRechazados(List<ErrorArchivo> servRechazados) {
		this.servRechazados = servRechazados;
	}
}
