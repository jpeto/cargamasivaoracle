package com.mx.santander.cia.app.cargamasiva.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BianNivel2 {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long _id;
    private String bian_nivel2;
    private int estatus;

    public Long get_id() {
        return _id;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }

    public String getBian_nivel2() {
        return bian_nivel2;
    }

    public void setBian_nivel2(String bian_nivel2) {
        this.bian_nivel2 = bian_nivel2;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }
}
