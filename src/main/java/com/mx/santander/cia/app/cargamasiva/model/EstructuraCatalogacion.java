package com.mx.santander.cia.app.cargamasiva.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EstructuraCatalogacion {
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Long dominio;
	private String dominioDesc;
	private String codigoCapa;
	private Long areaFuncional;
	private String areaFuncionalDesc;
	private String codigoSistema;
	private Long subareaFuncional;
	private String subAreaDesc;
	private String codigoSubsistema;
	private int estatus;

	public int getEstatus() {
		return estatus;
	}
	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}
	public Long getDominio() {
		return dominio;
	}
	public void setDominio(Long dominio) {
		this.dominio = dominio;
	}
	public Long getAreaFuncional() {
		return areaFuncional;
	}
	public void setAreaFuncional(Long areaFuncional) {
		this.areaFuncional = areaFuncional;
	}
	public Long getSubareaFuncional() {
		return subareaFuncional;
	}
	public void setSubareaFuncional(Long subareaFuncional) {
		this.subareaFuncional = subareaFuncional;
	}
	public String getDominioDesc() {
		return dominioDesc;
	}
	public void setDominioDesc(String dominioDesc) {
		this.dominioDesc = dominioDesc;
	}
	public String getCodigoCapa() {
		return codigoCapa;
	}
	public void setCodigoCapa(String codigoCapa) {
		this.codigoCapa = codigoCapa;
	}
	public String getAreaFuncionalDesc() {
		return areaFuncionalDesc;
	}
	public void setAreaFuncionalDesc(String areaFuncionalDesc) {
		this.areaFuncionalDesc = areaFuncionalDesc;
	}
	public String getCodigoSistema() {
		return codigoSistema;
	}
	public void setCodigoSistema(String codigoSistema) {
		this.codigoSistema = codigoSistema;
	}
	public String getSubAreaDesc() {
		return subAreaDesc;
	}
	public void setSubAreaDesc(String subAreaDesc) {
		this.subAreaDesc = subAreaDesc;
	}
	public String getCodigoSubsistema() {
		return codigoSubsistema;
	}
	public void setCodigoSubsistema(String codigoSubsistema) {
		this.codigoSubsistema = codigoSubsistema;
	}
}