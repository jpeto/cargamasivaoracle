package com.mx.santander.cia.app.cargamasiva.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TipologiaSoftware {
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Long _id;
	private String gradoHomologacion;
	private String tipologia;
	private String definicion;
	private int estatus;
	
	public Long get_id() {
		return _id;
	}
	public void set_id(Long _id) {
		this._id = _id;
	}
	public String getGradoHomologacion() {
		return gradoHomologacion;
	}
	public void setGradoHomologacion(String gradoHomologacion) {
		this.gradoHomologacion = gradoHomologacion;
	}
	public String getTipologia() {
		return tipologia;
	}
	public void setTipologia(String tipologia) {
		this.tipologia = tipologia;
	}
	public String getDefinicion() {
		return definicion;
	}
	public void setDefinicion(String definicion) {
		this.definicion = definicion;
	}
	public int getEstatus() {
		return estatus;
	}
	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}
	
}
