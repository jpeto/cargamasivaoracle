package com.mx.santander.cia.app.cargamasiva.model;

import static com.mx.santander.cia.app.cargamasiva.vo.IdGenerator.getId;

import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.data.annotation.PersistenceConstructor;

import com.mx.santander.cia.app.cargamasiva.properties.Propiedades;

public class Web implements Comparable<Web>{
	
	private String _id;
	private String nombreWEB;
	private String ambienteWEB;
	private Long estatusWEB;
	private String productoWEB;
	private String modeloWEB;
	private String eolWEB;
	private String idServidor;
	private String scid;
	private String nombreServidor;
	private int fila;
	private List<ErrorArchivo> erroresMapeo;
	private boolean existeEnArchivo;

	@PersistenceConstructor
	public Web(){
		//this._id = get_id();
	}
	
	/**
	 * Mapeo de una fila del excel de Infraestructura (hoja de Wweb y Web Amb. Prev) y validacion por campo.
	 * @param row Fila de Was.
	 * @param software Lista de Softwares.
	 * @param software Lista de Estatus de Infraestrutura.
	 */
	public void mapeaValores(Row row, List<Software> software, List<InfraestructuraStatus> infraestructuraStatus){
		this.erroresMapeo = new ArrayList<>();
		this.nombreWEB = valida(row.getCell(6), "WEB Name");
		this.ambienteWEB = valida(row.getCell(8), "WEB_ENV");
		this.estatusWEB = validaEstatus(row.getCell(3), infraestructuraStatus);
		this.productoWEB = valida(row.getCell(9), "PRODUCT_NAME");
		this.modeloWEB = valida(row.getCell(10), "MODEL_VERSION");
		this.nombreServidor = valida(row.getCell(11), "LS_NAME");
		this.scid = valida(row.getCell(15), "SC_ID");
		this.eolWEB = validaEOL(this.productoWEB, this.modeloWEB, software);
	}
	
	/**
	 * Valida valor de una celda de excel: campo vacio, generando las alertas correspondientes.
	 * @param c Celda de excel a validar.
	 * @param campo Nombre del campo de Web a validar.
	 * @return Valor de celda de excel.
	 */
    private String valida(Cell c, String campo) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null ){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") 
    			&& !c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){
    			return c.getStringCellValue();    			
    		}
    	}    	
    	error.setCampo(campo);
    	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VACIO));
		this.erroresMapeo.add(error);
		return null;
    }
    
    /**
	 * Valida existencia de Sistema operativo y Version en catalogo de Software para obtener EoL, generando las alertas correspondientes.
	 * @param productoWEB String con sistema operativo.
	 * @param modeloWEB String con version.
	 * @param software Lista de Softwares.
	 * @return String con valor de End Of Life.
	 */
    private String validaEOL(String productoWEB, String modeloWEB, List<Software> software) {
    	ErrorArchivo error = new ErrorArchivo();    	
    	if (productoWEB != null && modeloWEB != null) {
        	boolean esta = false;
        	String llavePM = productoWEB + " " + modeloWEB;
    		for(Software s: software){
        		if(s.getName()!=null && s.getVersion()!=null) {
        			String llaveOS = s.getName() + " " + s.getVersion();
        				if(llavePM.equals(llaveOS)){
        					esta=true;
        					if(s.getObsolescence()!=null) {
        						return s.getObsolescence();
        					}
        					else {
        						error.setCampo("End of life");
        						error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
        						error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VACIO));
        						this.erroresMapeo.add(error);
        						return null;					
        					}

        				}
        		}
        	}
        	if(!esta) {
        		error.setCampo("PRODUCT NAME y MODEL_VERSION");
        		error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
        		error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    			this.erroresMapeo.add(error);
    			return null;
        	} 
    	}
		return null;
    }
    
    /**
	 * Valida campo Infraestructura Estatus de una celda de excel: campo vacio, obligatoriedad y valores permitidos, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param entidades Lista de Infraestructura Estatus.
	 * @return Valor de Id de estatus de infraestructura identificado.
	 */
    private Long validaEstatus(Cell c, List<InfraestructuraStatus> infraestructuraStatus) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") && 
				!c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){
				for(InfraestructuraStatus e:infraestructuraStatus){
					if(e.getEstatus() != null && e.getEstatus().equals(c.getStringCellValue())){
						return e.get_id();
					}
				}
				error.setCampo("STATUS");
	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
	    	} else {
	        	error.setCampo("STATUS");
	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));	    		
	    	}
    	} else {
        	error.setCampo("STATUS");
        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));    		
    	}
		this.erroresMapeo.add(error);   	
    	return null;
    }
    
    /**
	 * Compara 2 Web, para determinar si son iguales por Nombre de Servidor, Nombre y Service Component.
	 * @param o Web.
	 * @return Entero indicando si los Web son o no iguales.
	 */
    @Override
    public int compareTo(Web o) {
    	String a = new String(this.nombreServidor+this.scid+this.nombreWEB);
        String b = new String(o.nombreServidor+o.scid+o.nombreWEB);
        return a.compareTo(b);
    }
    
	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getNombreWEB() {
		return nombreWEB;
	}

	public void setNombreWEB(String nombreWEB) {
		this.nombreWEB = nombreWEB;
	}

	public String getAmbienteWEB() {
		return ambienteWEB;
	}

	public void setAmbienteWEB(String ambienteWEB) {
		this.ambienteWEB = ambienteWEB;
	}

	public Long getEstatusWEB() {
		return estatusWEB;
	}

	public void setEstatusWEB(Long estatusWEB) {
		this.estatusWEB = estatusWEB;
	}

	public String getProductoWEB() {
		return productoWEB;
	}

	public void setProductoWEB(String productoWEB) {
		this.productoWEB = productoWEB;
	}

	public String getModeloWEB() {
		return modeloWEB;
	}

	public void setModeloWEB(String modeloWEB) {
		this.modeloWEB = modeloWEB;
	}

	public String getEolWEB() {
		return eolWEB;
	}

	public void setEolWEB(String eolWEB) {
		this.eolWEB = eolWEB;
	}

	public String getIdServidor() {
		return idServidor;
	}

	public void setIdServidor(String idServidor) {
		this.idServidor = idServidor;
	}
	public String getScid() {
		return scid;
	}
	public void setScid(String scid) {
		this.scid = scid;
	}
	public String getNombreServidor() {
		return nombreServidor;
	}
	public void setNombreServidor(String nombreServidor) {
		this.nombreServidor = nombreServidor;
	}

	public int getFila() {
		return fila;
	}

	public void setFila(int fila) {
		this.fila = fila;
	}

	public List<ErrorArchivo> getErroresMapeo() {
		return erroresMapeo;
	}

	public void setErroresMapeo(List<ErrorArchivo> erroresMapeo) {
		this.erroresMapeo = erroresMapeo;
	}

	public boolean isExisteEnArchivo() {
		return existeEnArchivo;
	}

	public void setExisteEnArchivo(boolean existeEnArchivo) {
		this.existeEnArchivo = existeEnArchivo;
	}
}