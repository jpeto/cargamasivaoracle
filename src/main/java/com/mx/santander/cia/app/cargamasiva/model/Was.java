package com.mx.santander.cia.app.cargamasiva.model;

import static com.mx.santander.cia.app.cargamasiva.vo.IdGenerator.getId;

import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.data.annotation.PersistenceConstructor;

import com.mx.santander.cia.app.cargamasiva.properties.Propiedades;

public class Was implements Comparable<Was>{
	
	private String _id;
	private String nombreWAS;
	private String ambienteWAS;
	private Long estatusWAS;
	private String productoWAS;
	private String modeloWAS;
	private String eolWAS;
	private String idServidor;
	private String scid;
	private String nombreServidor;
	private int fila;
	private List<ErrorArchivo> erroresMapeo;
	private boolean existeEnArchivo;
	
	@PersistenceConstructor
	public Was(){
		//this._id = get_id();
	}
	
	/**
	 * Mapeo de una fila del excel de Infraestructura (hoja de Was) y validacion por campo.
	 * @param row Fila de Was.
	 * @param software Lista de Softwares.
	 * @param software Lista de Estatus de Infraestrutura.
	 */
	public void mapeaValores(Row row, List<Software> software, List<InfraestructuraStatus> infraestructuraStatus){
    	this.erroresMapeo = new ArrayList<>();
		this.nombreWAS = valida(row.getCell(1), "WAS_NAME");
		this.ambienteWAS = valida(row.getCell(3), "WAS_ENV");
		this.estatusWAS = validaEstatus(row.getCell(5), infraestructuraStatus);
		this.productoWAS = valida(row.getCell(7), "PRODUCT NAME1");
		this.modeloWAS = valida(row.getCell(8), "MODEL_VERSION");
		this.nombreServidor = valida(row.getCell(12), "LS_NAME");
		this.scid = valida(row.getCell(16), "SC_ID");
		this.eolWAS = validaEOL(this.productoWAS, this.modeloWAS, software);
	}
	
	/**
	 * Mapeo de una fila del excel de Infraestructura (hoja de Was Amb. Prev) y validacion por campo.
	 * @param row Fila de Was.
	 * @param software Lista de Softwares.
	 */
	public void mapeaValoresPrevios(Row row, List<Software> software, List<InfraestructuraStatus> infraestructuraStatus){
    	this.erroresMapeo = new ArrayList<>();
		this.nombreWAS = valida(row.getCell(1), "WAS_NAME");
		this.ambienteWAS = valida(row.getCell(3), "WAS_ENV");
		this.estatusWAS = validaEstatus(row.getCell(5), infraestructuraStatus);
		this.productoWAS = valida(row.getCell(7), "PRODUCT NAME1");
		this.modeloWAS = valida(row.getCell(8), "MODEL_VERSION");
		this.nombreServidor = valida(row.getCell(12), "LS_NAME");
		this.scid = valida(row.getCell(17), "SC_ID");
		this.eolWAS = validaEOL(this.productoWAS, this.modeloWAS, software);
	}
	
	/**
	 * Valida valor de una celda de excel: campo vacio, generando las alertas correspondientes.
	 * @param c Celda de excel a validar.
	 * @param campo Nombre del campo de Was a validar.
	 * @return Valor de celda de excel.
	 */
    private String valida(Cell c, String campo) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null ){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") 
    			&& !c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){
    			return c.getStringCellValue();    			
    		}
    	}    	
    	error.setCampo(campo);
    	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VACIO));
		this.erroresMapeo.add(error);
		return null;
    }
    
    /**
	 * Valida existencia de Sistema operativo y Version en catalogo de Software para obtener EoL, generando las alertas correspondientes.
	 * @param productoWAS String con sistema operativo.
	 * @param modeloWAS String con version.
	 * @param software Lista de Softwares.
	 * @return String con valor de End Of Life.
	 */
    private String validaEOL(String productoWAS, String modeloWAS, List<Software> software) {
    	ErrorArchivo error = new ErrorArchivo();    	
    	if (productoWAS != null && modeloWAS != null) {
        	boolean esta = false;
        	String llavePM = productoWAS + " " + modeloWAS;
    		for(Software s: software){
        		if(s.getName()!=null && s.getVersion()!=null) {
        			String llaveOS = s.getName() + " " + s.getVersion();
        				if(llavePM.equals(llaveOS)){
        					esta=true;
        					if(s.getObsolescence()!=null) {
        						return s.getObsolescence();
        					}
        					else {
        						error.setCampo("End of life");
        						error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
        						error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VACIO));
        						this.erroresMapeo.add(error);
        						return null;					
        					}

        				}
        		}
        	}
        	if(!esta) {
        		error.setCampo("PRODUCT NAME1 y MODEL_VERSION");
        		error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
        		error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    			this.erroresMapeo.add(error);
    			return null;
        	} 
    	}
		return null;
    }
   
    /**
	 * Valida campo Infraestructura Estatus de una celda de excel: campo vacio, obligatoriedad y valores permitidos, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param entidades Lista de Infraestructura Estatus.
	 * @return Valor de Id de estatus de infraestructura identificado.
	 */
    private Long validaEstatus(Cell c, List<InfraestructuraStatus> infraestructuraStatus) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") && 
				!c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){
				for(InfraestructuraStatus e:infraestructuraStatus){
					if(e.getEstatus() != null && e.getEstatus().equals(c.getStringCellValue())){
						return e.get_id();
					}
				}
				error.setCampo("WAS_STATUS");
	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
	    	} else {
	        	error.setCampo("WAS_STATUS");
	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));	    		
	    	}
    	} else {
        	error.setCampo("WAS_STATUS");
        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));    		
    	}
		this.erroresMapeo.add(error);   	
    	return null;
    }
    
    /**
	 * Compara 2 Was, para determinar si son iguales por Nombre de Servidor, Nombre y Service Component.
	 * @param o Was.
	 * @return Entero indicando si los Was son o no iguales.
	 */
    @Override
    public int compareTo(Was o) {
    	String a = new String(this.nombreServidor+this.scid+this.nombreWAS);
        String b = new String(o.nombreServidor+o.scid+o.nombreWAS);
        return a.compareTo(b);
    }

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getNombreWAS() {
		return nombreWAS;
	}

	public void setNombreWAS(String nombreWAS) {
		this.nombreWAS = nombreWAS;
	}

	public String getAmbienteWAS() {
		return ambienteWAS;
	}

	public void setAmbienteWAS(String ambienteWAS) {
		this.ambienteWAS = ambienteWAS;
	}

	public Long getEstatusWAS() {
		return estatusWAS;
	}

	public void setEstatusWAS(Long estatusWAS) {
		this.estatusWAS = estatusWAS;
	}

	public String getProductoWAS() {
		return productoWAS;
	}

	public void setProductoWAS(String productoWAS) {
		this.productoWAS = productoWAS;
	}

	public String getModeloWAS() {
		return modeloWAS;
	}

	public void setModeloWAS(String modeloWAS) {
		this.modeloWAS = modeloWAS;
	}

	public String getEolWAS() {
		return eolWAS;
	}

	public void setEolWAS(String eolWAS) {
		this.eolWAS = eolWAS;
	}

	public String getIdServidor() {
		return idServidor;
	}

	public void setIdServidor(String idServidor) {
		this.idServidor = idServidor;
	}
	
	public String getScid() {
		return scid;
	}

	public void setScid(String scid) {
		this.scid = scid;
	}



	public String getNombreServidor() {
		return nombreServidor;
	}



	public void setNombreServidor(String nombreServidor) {
		this.nombreServidor = nombreServidor;
	}

	public int getFila() {
		return fila;
	}

	public void setFila(int fila) {
		this.fila = fila;
	}

	public List<ErrorArchivo> getErroresMapeo() {
		return erroresMapeo;
	}

	public void setErroresMapeo(List<ErrorArchivo> erroresMapeo) {
		this.erroresMapeo = erroresMapeo;
	}
	
	public boolean isExisteEnArchivo() {
		return existeEnArchivo;
	}

	public void setExisteEnArchivo(boolean existeEnArchivo) {
		this.existeEnArchivo = existeEnArchivo;
	}
}