package com.mx.santander.cia.app.cargamasiva.model;

public class AplicacionAsociada {
	
	private String codigoOrbis;
	private String nombreTecnico;
	private int vinculado;

	public String getCodigoOrbis() {
		return codigoOrbis;
	}

	public void setCodigoOrbis(String codigoOrbis) {
		this.codigoOrbis = codigoOrbis;
	}

	public String getNombreTecnico() {
		return nombreTecnico;
	}

	public void setNombreTecnico(String nombreTecnico) {
		this.nombreTecnico = nombreTecnico;
	}

	public int getVinculado() {
		return vinculado;
	}

	public void setVinculado(int vinculado) {
		this.vinculado = vinculado;
	}
}