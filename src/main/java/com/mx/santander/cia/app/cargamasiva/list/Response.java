package com.mx.santander.cia.app.cargamasiva.list;

public class Response {
	private Embedded _embedded;

	public Embedded get_embedded() {
		return _embedded;
	}

	public void set_embedded(Embedded _embedded) {
		this._embedded = _embedded;
	}
}
