package com.mx.santander.cia.app.cargamasiva.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AplicativoTecnicoBan {
	
	private String idAplicativoTecnicoBan;
	private int codigoOrbis;
	private int codigoTecnico;
	private int nombreTecnico;
	private int siglasAltair;
	private int estadoAplicacion;
	private int nombreComercial;
	private int descripcion;
	private int entidadPropietaria;
	private int categoriaBanco;
	private int nombreProveedor;
	private int ambitoUso;
	private int repProveedor;
	private int tipoSoporte;
	private int tipologiaSoftware;
	private int criticidad;
	private int resServicio;
	private int telServicio;
	private int emailServicio;
	private int resMantenimiento;
	private int telMantenimiento;
	private int emailMantenimiento;
	private int hosting;
	private int cpd;
	private int codigoRepositorio;
	private int apPortalizada;
	private int repAutorizacion;
	private int mecAutenticacion;
	private int repAutenticacion;
	private int mecAutorizacion;
	private int tecnologia;
	private int version;
	private int release;
	private int aRemedy;
	
	public String getIdAplicativoTecnicoBan() {
		return idAplicativoTecnicoBan;
	}
	public void setIdAplicativoTecnicoBan(String idAplicativoTecnicoBan) {
		this.idAplicativoTecnicoBan = idAplicativoTecnicoBan;
	}
	public int getCodigoOrbis() {
		return codigoOrbis;
	}
	public void setCodigoOrbis(int codigoOrbis) {
		this.codigoOrbis = codigoOrbis;
	}
	public int getCodigoTecnico() {
		return codigoTecnico;
	}
	public void setCodigoTecnico(int codigoTecnico) {
		this.codigoTecnico = codigoTecnico;
	}
	public int getNombreTecnico() {
		return nombreTecnico;
	}
	public void setNombreTecnico(int nombreTecnico) {
		this.nombreTecnico = nombreTecnico;
	}
	public int getSiglasAltair() {
		return siglasAltair;
	}
	public void setSiglasAltair(int siglasAltair) {
		this.siglasAltair = siglasAltair;
	}
	public int getEstadoAplicacion() {
		return estadoAplicacion;
	}
	public void setEstadoAplicacion(int estadoAplicacion) {
		this.estadoAplicacion = estadoAplicacion;
	}
	public int getNombreComercial() {
		return nombreComercial;
	}
	public void setNombreComercial(int nombreComercial) {
		this.nombreComercial = nombreComercial;
	}
	public int getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(int descripcion) {
		this.descripcion = descripcion;
	}
	public int getEntidadPropietaria() {
		return entidadPropietaria;
	}
	public void setEntidadPropietaria(int entidadPropietaria) {
		this.entidadPropietaria = entidadPropietaria;
	}
	public int getCategoriaBanco() {
		return categoriaBanco;
	}
	public void setCategoriaBanco(int categoriaBanco) {
		this.categoriaBanco = categoriaBanco;
	}
	public int getNombreProveedor() {
		return nombreProveedor;
	}
	public void setNombreProveedor(int nombreProveedor) {
		this.nombreProveedor = nombreProveedor;
	}
	public int getAmbitoUso() {
		return ambitoUso;
	}
	public void setAmbitoUso(int ambitoUso) {
		this.ambitoUso = ambitoUso;
	}
	public int getRepProveedor() {
		return repProveedor;
	}
	public void setRepProveedor(int repProveedor) {
		this.repProveedor = repProveedor;
	}
	public int getTipoSoporte() {
		return tipoSoporte;
	}
	public void setTipoSoporte(int tipoSoporte) {
		this.tipoSoporte = tipoSoporte;
	}
	public int getTipologiaSoftware() {
		return tipologiaSoftware;
	}
	public void setTipologiaSoftware(int tipologiaSoftware) {
		this.tipologiaSoftware = tipologiaSoftware;
	}
	public int getCriticidad() {
		return criticidad;
	}
	public void setCriticidad(int criticidad) {
		this.criticidad = criticidad;
	}
	public int getResServicio() {
		return resServicio;
	}
	public void setResServicio(int resServicio) {
		this.resServicio = resServicio;
	}
	public int getTelServicio() {
		return telServicio;
	}
	public void setTelServicio(int telServicio) {
		this.telServicio = telServicio;
	}
	public int getEmailServicio() {
		return emailServicio;
	}
	public void setEmailServicio(int emailServicio) {
		this.emailServicio = emailServicio;
	}
	public int getResMantenimiento() {
		return resMantenimiento;
	}
	public void setResMantenimiento(int resMantenimiento) {
		this.resMantenimiento = resMantenimiento;
	}
	public int getTelMantenimiento() {
		return telMantenimiento;
	}
	public void setTelMantenimiento(int telMantenimiento) {
		this.telMantenimiento = telMantenimiento;
	}
	public int getEmailMantenimiento() {
		return emailMantenimiento;
	}
	public void setEmailMantenimiento(int emailMantenimiento) {
		this.emailMantenimiento = emailMantenimiento;
	}
	public int getHosting() {
		return hosting;
	}
	public void setHosting(int hosting) {
		this.hosting = hosting;
	}
	public int getCpd() {
		return cpd;
	}
	public void setCpd(int cpd) {
		this.cpd = cpd;
	}
	public int getCodigoRepositorio() {
		return codigoRepositorio;
	}
	public void setCodigoRepositorio(int codigoRepositorio) {
		this.codigoRepositorio = codigoRepositorio;
	}
	public int getApPortalizada() {
		return apPortalizada;
	}
	public void setApPortalizada(int apPortalizada) {
		this.apPortalizada = apPortalizada;
	}
	public int getRepAutorizacion() {
		return repAutorizacion;
	}
	public void setRepAutorizacion(int repAutorizacion) {
		this.repAutorizacion = repAutorizacion;
	}
	public int getMecAutenticacion() {
		return mecAutenticacion;
	}
	public void setMecAutenticacion(int mecAutenticacion) {
		this.mecAutenticacion = mecAutenticacion;
	}
	public int getRepAutenticacion() {
		return repAutenticacion;
	}
	public void setRepAutenticacion(int repAutenticacion) {
		this.repAutenticacion = repAutenticacion;
	}
	public int getMecAutorizacion() {
		return mecAutorizacion;
	}
	public void setMecAutorizacion(int mecAutorizacion) {
		this.mecAutorizacion = mecAutorizacion;
	}
	public int getTecnologia() {
		return tecnologia;
	}
	public void setTecnologia(int tecnologia) {
		this.tecnologia = tecnologia;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	public int getRelease() {
		return release;
	}
	public void setRelease(int release) {
		this.release = release;
	}
	public int getaRemedy() {
		return aRemedy;
	}
	public void setaRemedy(int aRemedy) {
		this.aRemedy = aRemedy;
	}
}