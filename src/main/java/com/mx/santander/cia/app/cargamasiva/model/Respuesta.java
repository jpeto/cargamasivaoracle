package com.mx.santander.cia.app.cargamasiva.model;

public class Respuesta {
	//@JsonInclude(JsonInclude.Include.NON_NULL)
	private String timestamp;
	private String status;
	private String error;
	private String message;
	private String nombreA;
	private String nombreB;
	private byte[] archivoErroresA;
	private byte[] archivoErroresB;
	
	
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public byte[] getArchivoErroresA() {
		return archivoErroresA;
	}
	public void setArchivoErroresA(byte[] archivoErroresA) {
		this.archivoErroresA = archivoErroresA;
	}
	public byte[] getArchivoErroresB() {
		return archivoErroresB;
	}
	public void setArchivoErroresB(byte[] archivoErroresB) {
		this.archivoErroresB = archivoErroresB;
	}
	public String getNombreA() {
		return nombreA;
	}
	public void setNombreA(String nombreA) {
		this.nombreA = nombreA;
	}
	public String getNombreB() {
		return nombreB;
	}
	public void setNombreB(String nombreB) {
		this.nombreB = nombreB;
	}
    

	

}
