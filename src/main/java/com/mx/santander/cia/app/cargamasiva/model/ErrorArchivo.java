package com.mx.santander.cia.app.cargamasiva.model;

public class ErrorArchivo {

	private String fila;
	private String identificador;
	private String tipoMsj;
	private String campo;
	private String mensaje;
	public String getIdentificador() {
		return identificador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public String getTipoMsj() {
		return tipoMsj;
	}
	public void setTipoMsj(String tipoMsj) {
		this.tipoMsj = tipoMsj;
	}
	public String getCampo() {
		return campo;
	}
	public void setCampo(String campo) {
		this.campo = campo;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public String getFila() {
		return fila;
	}
	public void setFila(String fila) {
		this.fila = fila;
	}
}
