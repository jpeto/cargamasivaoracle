package com.mx.santander.cia.app.cargamasiva.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AplicativoFuncionalBan {
	
	private String idAplicativoFuncionalBan;
	private int codigoFuncional;
	private int nombreFuncional;
	private int estadoAplicacion;
	private int categoriaAplicacion;
	private int descFuncionalidad;
	private int pais;
	private int  entidad;
	private int mapa;
	private int capa;
	private int sistema;
	private int subsistema;
	private int gp1;
	private int gp2;
	private int areaOperadora;
	private int resSeguridad;
	private int nombreAdmon;
	private int areaNegocio;
	private int resDatPrimario;
	private int resDatSecundario;
	private int resDatTerciario;
	private int confidencialidadInfo;
	private int integridadInfo;
	private int disponibilidadInfo;
	private int vinculadoProceso;
	private int ultimaPrueba;
	private int existeRecuperacion;
	private int tiempoRecuperacion;
	private int puntoRecuperacion;
	private int pertenecePruebasCon;
	private int adecuadaNegocio;
	private int dentroModelo;
	private int pilaresAdecuado;
	private int deploymentYear;
	private int apiEnabled;
	private int publicCloud;
	private int bnombre;
	private int bcriticidad;
	private int bnivel1;
	private int bnivel2;
	private int bnivel3;
	private int bpaquete;
	private int bgranularidad;
	
	public int getCodigoFuncional() {
		return codigoFuncional;
	}
	public void setCodigoFuncional(int codigoFuncional) {
		this.codigoFuncional = codigoFuncional;
	}
	public int getNombreFuncional() {
		return nombreFuncional;
	}
	public void setNombreFuncional(int nombreFuncional) {
		this.nombreFuncional = nombreFuncional;
	}
	public int getEstadoAplicacion() {
		return estadoAplicacion;
	}
	public void setEstadoAplicacion(int estadoAplicacion) {
		this.estadoAplicacion = estadoAplicacion;
	}
	public int getCategoriaAplicacion() {
		return categoriaAplicacion;
	}
	public void setCategoriaAplicacion(int categoriaAplicacion) {
		this.categoriaAplicacion = categoriaAplicacion;
	}
	public int getDescFuncionalidad() {
		return descFuncionalidad;
	}
	public void setDescFuncionalidad(int descFuncionalidad) {
		this.descFuncionalidad = descFuncionalidad;
	}
	public int getPais() {
		return pais;
	}
	public void setPais(int pais) {
		this.pais = pais;
	}
	public int getEntidad() {
		return entidad;
	}
	public void setEntidad(int entidad) {
		this.entidad = entidad;
	}
	public int getMapa() {
		return mapa;
	}
	public void setMapa(int mapa) {
		this.mapa = mapa;
	}
	public int getCapa() {
		return capa;
	}
	public void setCapa(int capa) {
		this.capa = capa;
	}
	public int getSistema() {
		return sistema;
	}
	public void setSistema(int sistema) {
		this.sistema = sistema;
	}
	public int getSubsistema() {
		return subsistema;
	}
	public void setSubsistema(int subsistema) {
		this.subsistema = subsistema;
	}
	public int getGp1() {
		return gp1;
	}
	public void setGp1(int gp1) {
		this.gp1 = gp1;
	}
	public int getGp2() {
		return gp2;
	}
	public void setGp2(int gp2) {
		this.gp2 = gp2;
	}
	public int getAreaOperadora() {
		return areaOperadora;
	}
	public void setAreaOperadora(int areaOperadora) {
		this.areaOperadora = areaOperadora;
	}
	public int getResSeguridad() {
		return resSeguridad;
	}
	public void setResSeguridad(int resSeguridad) {
		this.resSeguridad = resSeguridad;
	}
	public int getNombreAdmon() {
		return nombreAdmon;
	}
	public void setNombreAdmon(int nombreAdmon) {
		this.nombreAdmon = nombreAdmon;
	}
	public int getAreaNegocio() {
		return areaNegocio;
	}
	public void setAreaNegocio(int areaNegocio) {
		this.areaNegocio = areaNegocio;
	}
	public int getResDatPrimario() {
		return resDatPrimario;
	}
	public void setResDatPrimario(int resDatPrimario) {
		this.resDatPrimario = resDatPrimario;
	}
	public int getResDatSecundario() {
		return resDatSecundario;
	}
	public void setResDatSecundario(int resDatSecundario) {
		this.resDatSecundario = resDatSecundario;
	}
	public int getResDatTerciario() {
		return resDatTerciario;
	}
	public void setResDatTerciario(int resDatTerciario) {
		this.resDatTerciario = resDatTerciario;
	}
	public int getConfidencialidadInfo() {
		return confidencialidadInfo;
	}
	public void setConfidencialidadInfo(int confidencialidadInfo) {
		this.confidencialidadInfo = confidencialidadInfo;
	}
	public int getIntegridadInfo() {
		return integridadInfo;
	}
	public void setIntegridadInfo(int integridadInfo) {
		this.integridadInfo = integridadInfo;
	}
	public int getDisponibilidadInfo() {
		return disponibilidadInfo;
	}
	public void setDisponibilidadInfo(int disponibilidadInfo) {
		this.disponibilidadInfo = disponibilidadInfo;
	}
	public int getVinculadoProceso() {
		return vinculadoProceso;
	}
	public void setVinculadoProceso(int vinculadoProceso) {
		this.vinculadoProceso = vinculadoProceso;
	}
	public int getUltimaPrueba() {
		return ultimaPrueba;
	}
	public void setUltimaPrueba(int ultimaPrueba) {
		this.ultimaPrueba = ultimaPrueba;
	}
	public int getExisteRecuperacion() {
		return existeRecuperacion;
	}
	public void setExisteRecuperacion(int existeRecuperacion) {
		this.existeRecuperacion = existeRecuperacion;
	}
	public int getTiempoRecuperacion() {
		return tiempoRecuperacion;
	}
	public void setTiempoRecuperacion(int tiempoRecuperacion) {
		this.tiempoRecuperacion = tiempoRecuperacion;
	}
	public int getPuntoRecuperacion() {
		return puntoRecuperacion;
	}
	public void setPuntoRecuperacion(int puntoRecuperacion) {
		this.puntoRecuperacion = puntoRecuperacion;
	}
	public int getPertenecePruebasCon() {
		return pertenecePruebasCon;
	}
	public void setPertenecePruebasCon(int pertenecePruebasCon) {
		this.pertenecePruebasCon = pertenecePruebasCon;
	}
	public int getAdecuadaNegocio() {
		return adecuadaNegocio;
	}
	public void setAdecuadaNegocio(int adecuadaNegocio) {
		this.adecuadaNegocio = adecuadaNegocio;
	}
	public int getDentroModelo() {
		return dentroModelo;
	}
	public void setDentroModelo(int dentroModelo) {
		this.dentroModelo = dentroModelo;
	}
	public int getPilaresAdecuado() {
		return pilaresAdecuado;
	}
	public void setPilaresAdecuado(int pilaresAdecuado) {
		this.pilaresAdecuado = pilaresAdecuado;
	}
	public int getDeploymentYear() {
		return deploymentYear;
	}
	public void setDeploymentYear(int deploymentYear) {
		this.deploymentYear = deploymentYear;
	}
	public int getApiEnabled() {
		return apiEnabled;
	}
	public void setApiEnabled(int apiEnabled) {
		this.apiEnabled = apiEnabled;
	}
	public int getPublicCloud() {
		return publicCloud;
	}
	public void setPublicCloud(int publicCloud) {
		this.publicCloud = publicCloud;
	}
	public int getBnombre() {
		return bnombre;
	}
	public void setBnombre(int bnombre) {
		this.bnombre = bnombre;
	}
	public int getBcriticidad() {
		return bcriticidad;
	}
	public void setBcriticidad(int bcriticidad) {
		this.bcriticidad = bcriticidad;
	}
	public int getBnivel1() {
		return bnivel1;
	}
	public void setBnivel1(int bnivel1) {
		this.bnivel1 = bnivel1;
	}
	public int getBnivel2() {
		return bnivel2;
	}
	public void setBnivel2(int bnivel2) {
		this.bnivel2 = bnivel2;
	}
	public int getBnivel3() {
		return bnivel3;
	}
	public void setBnivel3(int bnivel3) {
		this.bnivel3 = bnivel3;
	}
	public int getBpaquete() {
		return bpaquete;
	}
	public void setBpaquete(int bpaquete) {
		this.bpaquete = bpaquete;
	}
	public int getBgranularidad() {
		return bgranularidad;
	}
	public void setBgranularidad(int bgranularidad) {
		this.bgranularidad = bgranularidad;
	}
	public String getIdAplicativoFuncionalBan() {
		return idAplicativoFuncionalBan;
	}
	public void setIdAplicativoFuncionalBan(String idAplicativoFuncionalBan) {
		this.idAplicativoFuncionalBan = idAplicativoFuncionalBan;
	}
	
}