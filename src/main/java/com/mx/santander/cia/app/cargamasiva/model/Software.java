package com.mx.santander.cia.app.cargamasiva.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Software implements Comparable<Software>{
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String _id;
	private String ciType;
	private String manufacturer;
	private String name;
	private String version;
	private String obsolescence;
	private String std;
	private int estatus;
	
	/**
	 * Compara 2 Softwares, para determinar si son iguales por Nombre y Version.
	 * @param o Software.
	 * @return Entero indicando si los Softwares son o no iguales.
	 */
    @Override
    public int compareTo(Software o) {
    	String a = new String(this.name+this.version);
        String b = new String(o.name+o.version);
        return a.compareTo(b);
    }
	
	public String get_id() {
		return _id;
	}
	public void set_id(String _id) {
		this._id = _id;
	}
	public String getCiType() {
		return ciType;
	}
	public void setCiType(String ciType) {
		this.ciType = ciType;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getObsolescence() {
		return obsolescence;
	}
	public void setObsolescence(String obsolescence) {
		this.obsolescence = obsolescence;
	}
	public String getStd() {
		return std;
	}
	public void setStd(String std) {
		this.std = std;
	}
	public int getEstatus() {
		return estatus;
	}
	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}

	
	
	

}
