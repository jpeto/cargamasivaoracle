package com.mx.santander.cia.app.cargamasiva.model;

import java.text.Collator;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.data.annotation.PersistenceConstructor;

import com.mx.santander.cia.app.cargamasiva.properties.Propiedades;

public class AplicativoFuncional {
	
	private String _id;
	private String codigoFuncional;
	private String nombreFuncional;
	private String estadoAplicacion;
	private Long categoriaAplicacion;
	private String descFuncionalidad;
	private String pais;
	private Long entidad;
	private Long mapa;
	private Long capa;
	private String codigoCapa;
	private Long sistema;
	private String codigoSistema;
	private Long subsistema;
	private String codigoSubsistema;
	private List<Responsables> responsables;
	private String confidencialidadInfo;
	private String integridadInfo;
	private String disponibilidadInfo;
	private String vinculadoProceso;
	private String ultimaPrueba;
	private String existeRecuperacion;
	private String tiempoRecuperacion;
	private String puntoRecuperacion;
	private String pertenecePruebasCon;
	private String adecuadaNegocio;
	private String dentroModelo;
	private String pilaresAdecuado;
	private String deploymentYear;
	private String apiEnabled;
	private String publicCloud;
	private Long bnombre;
	private Long bcriticidad;
	private Long bnivel1;
	private Long bnivel2;
	private Long bnivel3;
	private Long bpaquete;
	private Long bgranularidad;
	private int activo;
	private int finalizado;
	private List<ErrorArchivo> erroresMapeo;
	private int estado;
	
	@PersistenceConstructor
	public AplicativoFuncional(){
		this.activo = 1;
		this.finalizado = 0;
	}
	
	/**
	 * Mapeo de una fila del excel de Aplicativos Funcionales y validacion por campo.
	 * @param row Fila de Aplicativo Funcional.
	 * @param entidades Lista de Entidades.
	 * @param mapas Lista de Mapas.
	 * @param capas Lista de Capas.
	 * @param sistemas Lista de Sistemas.
	 * @param subsistemas Lista de Subsistemas.
	 * @param categorias Lista de Categorias.
	 * @param dominios Lista de Dominios.
	 * @param biancriticidades Lista de Bian Criticidades.
	 * @param bianafrs Lista de Bian Afr.
	 * @param bianniveles2 Lista de Bian Niveles 2.
	 * @param bianniveles3 Lista de Bian Niveles 3.
	 * @param paquetesbian Lista de Paquetes Bian.
	 * @param granularidadesbian Lista de Granularidades Bian.
	 */
    public void mapeaValores(Row row, List<Entidad> entidades, 
			List<Mapa> mapas, List<Capa> capas, List<Sistema> sistemas, List<Subsistema> subsistemas, List<CategoriaAplicativo> categorias, 
			List<Dominio> dominios, List<BianCriticidad> biancriticidades, List<BianAfr> bianafrs, List<BianNivel2> bianniveles2, 
			List<BianNivel3> bianniveles3, List<PaqueteBian> paquetesbian, List<GranularidadBian> granularidadesbian){
    	this.erroresMapeo = new ArrayList<>();
    	this.pais = validaPais(row.getCell(3),6);
    	this.entidad = validaEntidad(row.getCell(4), entidades);
    	this.mapa = validaMapa(row.getCell(5), mapas);
    	this.codigoCapa = validaCodigoCapa(row.getCell(6), capas);
    	this.capa = validaCapa(row.getCell(7), capas);
    	validaCapaYCodigoCapa(capas);
    	this.codigoSistema = validaCodigoSistema(row.getCell(8), sistemas);
    	this.sistema = validaSistema(row.getCell(9), sistemas);
    	validaSistemaYCodigoSistema(sistemas);
    	this.codigoSubsistema = validaCodigoSubsistema(row.getCell(10), subsistemas);
    	this.subsistema = validaSubsistema(row.getCell(11), subsistemas);
    	validaSubsistemayCodigoSubsistema(subsistemas);
    	this.codigoFuncional = validaCodigo(row.getCell(12), "Código Funcional",10);
    	this.nombreFuncional = validaNombre(row.getCell(13), "Nombre Funcional",120);
    	this.descFuncionalidad = valida(row.getCell(14), "Descripción Funcional",1200);
    	this.estadoAplicacion = validaEstadoAplicacion(row.getCell(16), "Estado Aplicación");
    	this.categoriaAplicacion = validaCategoria(row.getCell(18), categorias);
    	this.responsables = new ArrayList<>();
    	this.responsables = validaResponsables(row);
    	this.confidencialidadInfo = validaConfidencialidad(row.getCell(28), "Confidencialidad de la Información");
    	this.integridadInfo = validaIntegridad(row.getCell(29), "Integridad de la Información");
    	this.disponibilidadInfo = validaDisponibilidad(row.getCell(30), "Disponibilidad de la Información");
    	this.vinculadoProceso = validaVinculadoProceso(row.getCell(34), "Vinculado a Proceso Crítico");
    	this.ultimaPrueba = validaFecha(row.getCell(35), "Fecha de la Última Prueba de Contingencia",10);
    	this.existeRecuperacion = validaSiNo(row.getCell(36), "Existencia de Procedimiento de Recuperación de la Aplicación");
    	this.tiempoRecuperacion = validaTiempoRecuperacion(row.getCell(37), "Tiempo de Recuperación Objetivo");
    	this.puntoRecuperacion = validaPuntoRecuperacion(row.getCell(38), "Punto de Recuperación Objetivo");
    	this.pertenecePruebasCon = validaSiNo(row.getCell(39), "Aplicación Pertenece al Plan de Pruebas de Contingencia");
    	this.adecuadaNegocio = validaAdecuada_Dentro_Pilares(row.getCell(40), "Adecuada Negocio");
    	this.dentroModelo = validaAdecuada_Dentro_Pilares(row.getCell(41), "Dentro Modelo");
    	this.pilaresAdecuado = validaAdecuada_Dentro_Pilares(row.getCell(42), "Pilares Adecuado");
    	this.deploymentYear = validaDepYear(row.getCell(44), "Año de primera puesta en producción",4);
    	this.apiEnabled = validaSN(row.getCell(45), "¿Aplicación APIficada?", 1);
    	this.publicCloud = validaCloud(row.getCell(46), "% Cloud pública",6);
    	this.bnombre = validaBnombre(row.getCell(47), bianafrs);
    	this.bcriticidad = validaBCriticidad(row.getCell(48), biancriticidades);
    	this.bnivel1 = validaBNivel1(row.getCell(49), dominios);
    	this.bnivel2 = validaBNivel2(row.getCell(50), bianniveles2);
    	this.bnivel3 = validaBNivel3(row.getCell(51), bianniveles3);
    	this.bpaquete = validaBPaquete(row.getCell(52), paquetesbian);
    	this.bgranularidad = validaBGranularidad(row.getCell(53), granularidadesbian);
    }

    /**
	 * Valida valor de una celda de excel: obligatoriedad, campo vacio y longitud de campo, generando las alertas correspondientes.
	 * @param c Celda de excel a validar.
	 * @param campo Nombre del campo de Aplicativo Funcional a validar.
	 * @param longitud Longitud del campo de Aplicativo Funcional a validar.
	 * @return Valor de celda de excel.
	 */
    private String valida(Cell c, String campo, int longitud) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") 
    				&& !c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){
    			if(c.getStringCellValue().length()>longitud) {
    				String mensaje = Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_LONGITUD);
    				mensaje = mensaje.replaceAll("<LONG>", longitud+"");
    				error.setCampo(campo);
    		    	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    		    	error.setMensaje(mensaje);
    				this.erroresMapeo.add(error);
    				return c.getStringCellValue().substring(0, longitud);
    			}else {
    				return c.getStringCellValue();
    			}
    			
    		}
    	} else {
			error.setCampo(campo);
			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));			
		}
		this.erroresMapeo.add(error);
		return null;
    }
    
    /**
	 * Valida valor de un string: obligatoriedad, campo vacio y longitud de campo, generando las alertas correspondientes.
	 * @param c String a validar.
	 * @param campo Nombre del campo de Aplicativo Funcional a validar.
	 * @param longitud Longitud del campo de Aplicativo Funcional a validar.
	 * @return Valor de celda de excel.
	 */
    private String valida(String c, String campo, int longitud) {
    	ErrorArchivo error = new ErrorArchivo();
    	if (c != null && c != null){
    		if(!c.equals("Sin Informar") && !c.equals("Sin Información") 
    				&& !c.equals("Sin Datos") && !c.trim().equals("")){
    			if(c.length()>longitud) {
    				String mensaje = Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_LONGITUD);
    				mensaje = mensaje.replaceAll("<LONG>", longitud+"");
    				error.setCampo(campo);
    		    	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    		    	error.setMensaje(mensaje);
    				this.erroresMapeo.add(error);
    				return c.substring(0, longitud);
    			}else {
    				return c;
    			}
    			
    		}
    	} else {
			error.setCampo(campo);
			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));			
		}
		this.erroresMapeo.add(error);
		return null;
    }
    
    /**
	 * Valida valor de un string: campo vacio y longitud de campo, generando las alertas correspondientes.
	 * @param c String a validar.
	 * @param campo Nombre del campo de Aplicativo Funcional a validar.
	 * @param longitud Longitud del campo de Aplicativo Funcional a validar.
	 * @return Valor de celda de excel.
	 */
    private String validaNoObl(String c, String campo, int longitud) {
    	ErrorArchivo error = new ErrorArchivo();
    	if (c != null){
    		if(!c.equals("Sin Informar") && !c.equals("Sin Información") 
    				&& !c.equals("Sin Datos") && !c.trim().equals("")){
    			if(c.length()>longitud) {
    				String mensaje = Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_LONGITUD);
    				mensaje = mensaje.replaceAll("<LONG>", longitud+"");
    				error.setCampo(campo);
    		    	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    		    	error.setMensaje(mensaje);
    				this.erroresMapeo.add(error);
    				return c.substring(0, longitud);
    			}else {
    				return c;
    			}
    			
    		}
    	} else {
			error.setCampo(campo);
			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VACIO));
		}
		this.erroresMapeo.add(error);    	
		return null;
    }
    
    /**
	 * Valida responsables de una fila de excel: presencia de pipes, campo vacio, estructura numerica y longitud, generando las alertas correspondientes.
	 * @param row Fila de excel.
	 * @return Lista de responsables en fila de excel.
	 */
    private List<Responsables> validaResponsables(Row row) {
    	List<Responsables> resp = new ArrayList<>();
    	
    	if (row.getCell(22) != null && row.getCell(22).getStringCellValue() != null) {
    		if (!row.getCell(22).getStringCellValue().contains("|")) {
    			ErrorArchivo error = new ErrorArchivo();
    			error.setCampo("Gestor de Plan (N1)");
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_PIPE));
    			this.erroresMapeo.add(error);    
    		} else {
            	String[] g1 = row.getCell(22).getStringCellValue().split("\\|");
            	if (g1.length == 2) {
            		Responsables responsable = new Responsables();
            		responsable.setNombre(valida(g1[0], "Nombre Gestor de Plan (N1)",69));
            		responsable.setCorreo(valida(g1[1], "Correo Gestor de Plan (N1)",30));
            		responsable.setIdTipoResponsable(0);
            		if (responsable.getNombre() != null) {
            			resp.add(responsable);
            		}
            	} else {
            		ErrorArchivo error = new ErrorArchivo();
        			error.setCampo("Gestor de Plan (N1)");
        			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
        			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_PIPE));
        			this.erroresMapeo.add(error);  
            	}
        		
    		}
    	} else {
    		valida(row.getCell(22), "Gestor de Plan (N1)",100);
    	}

    	if (row.getCell(23) != null && row.getCell(23).getStringCellValue() != null) {
    		if (!row.getCell(23).getStringCellValue().contains("|")) {
    			ErrorArchivo error = new ErrorArchivo();
    			error.setCampo("Gestor de Plan (N2)");
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_PIPE));
    			this.erroresMapeo.add(error);    
    		} else {
        		String[] g2 = row.getCell(23).getStringCellValue().split("\\|");
        		if (g2.length == 2) {
            		Responsables responsable1 = new Responsables();
            		responsable1.setNombre(valida(g2[0], "Nombre Gestor de Plan (N2)",69));
            		responsable1.setCorreo(valida(g2[1], "Correo Gestor de Plan (N2)",30));
            		responsable1.setIdTipoResponsable(1);
            		if (responsable1.getNombre() != null) {
            			resp.add(responsable1);
            		}    	
	    		} else {
	        		ErrorArchivo error = new ErrorArchivo();
	    			error.setCampo("Gestor de Plan (N2)");
	    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_PIPE));
	    			this.erroresMapeo.add(error);  
	        	}
    		}    		
    	} else {
    		valida(row.getCell(23), "Gestor de Plan (N2)",100);
    	}

    	if (row.getCell(31) != null && row.getCell(31).getStringCellValue() != null) {
    		if (!row.getCell(31).getStringCellValue().contains("|")) {
    			ErrorArchivo error = new ErrorArchivo();
    			error.setCampo("Responsable de Datos Primario");
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_PIPE));
    			this.erroresMapeo.add(error);    
    		} else {
        		String[] prim = row.getCell(31).getStringCellValue().split("\\|");
        		if (prim.length == 2) {
            		Responsables responsable2 = new Responsables();
            		responsable2.setNombre(valida(prim[0], "Nombre Responsable de Datos Primario",69));
            		responsable2.setCorreo(valida(prim[1], "Correo Responsable de Datos Primario",30));
            		responsable2.setIdTipoResponsable(2);
            		if (responsable2.getNombre() != null) {
            			resp.add(responsable2);
            		}    	
	    		} else {
	        		ErrorArchivo error = new ErrorArchivo();
	    			error.setCampo("Responsable de Datos Primario");
	    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_PIPE));
	    			this.erroresMapeo.add(error);  
	        	}
    		}
    	} else {
    		valida(row.getCell(31), "Responsable de Datos Primario",100);
    	}
    	
    	if (row.getCell(32) != null && row.getCell(32).getStringCellValue() != null) {
    		if (!row.getCell(32).getStringCellValue().contains("|")) {
    			ErrorArchivo error = new ErrorArchivo();
    			error.setCampo("Responsable de Datos Secundario");
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_PIPE));
    			this.erroresMapeo.add(error);    
    		} else {
        		String[] sec  = row.getCell(32).getStringCellValue().split("\\|");   	
        		if (sec.length == 2) {
            		Responsables responsable3 = new Responsables();
            		responsable3.setNombre(valida(sec[0], "Nombre Responsable de Datos Secundario",69));
            		responsable3.setCorreo(valida(sec[1], "Correo Responsable de Datos Secundario",30));
            		responsable3.setIdTipoResponsable(3);
            		if (responsable3.getNombre() != null) {
            			resp.add(responsable3);
            		} 
	    		} else {
	        		ErrorArchivo error = new ErrorArchivo();
	    			error.setCampo("Responsable de Datos Secundario");
	    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_PIPE));
	    			this.erroresMapeo.add(error);  
	        	}
    		}
    	} else {
    		valida(row.getCell(32), "Responsable de Datos Secundario",100);
    	}
    	
    	if (row.getCell(33) != null && row.getCell(33).getStringCellValue() != null) {
    		if (!row.getCell(33).getStringCellValue().contains("|")) {
    			ErrorArchivo error = new ErrorArchivo();
    			error.setCampo("Responsable de Datos Terciario");
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_PIPE));
    			this.erroresMapeo.add(error);    
    		} else {
        		String[] ter = row.getCell(33).getStringCellValue().split("\\|");  	
        		if (ter.length == 2) {
            		Responsables responsable4 = new Responsables();
            		responsable4.setNombre(valida(ter[0], "Nombre Responsable de Datos Terciario",69));
            		responsable4.setCorreo(valida(ter[1], "Correo Responsable de Datos Terciario",30));
            		responsable4.setIdTipoResponsable(4);
            		if (responsable4.getNombre() != null) {
            			resp.add(responsable4);
            		}  
	    		} else {
	        		ErrorArchivo error = new ErrorArchivo();
	    			error.setCampo("Responsable de Datos Terciario");
	    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_PIPE));
	    			this.erroresMapeo.add(error);  
	        	}
    		}
    	} else {
    		valida(row.getCell(33), "Responsable de Datos Terciario",100);
    	}
    	
    	if (row.getCell(24) != null && row.getCell(24).getStringCellValue() != null) {
    		if (!row.getCell(24).getStringCellValue().contains("|")) {
    			ErrorArchivo error = new ErrorArchivo();
    			error.setCampo("Responsable Operaciones");
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_PIPE));
    			this.erroresMapeo.add(error);    
    		} else {
        		String[] ope = row.getCell(24).getStringCellValue().split("\\|"); 	
        		if (ope.length == 4) {
                		Responsables responsable5 = new Responsables();
                		responsable5.setNombre(valida(ope[0], "Nombre Responsable Operaciones",279));
            			if (ope[2].matches("[0-9]*")) {
            				responsable5.setExtension(validaNoObl(ope[2], "Extensión Responsable Operaciones",6));
                		} else {
        	        		ErrorArchivo error = new ErrorArchivo();
             		    	error.setCampo("Extensión Responsable Operaciones");
             	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
             	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VALIDO_CODIGO));
        	    			this.erroresMapeo.add(error);  
                		}     
                		responsable5.setCorreo(valida(ope[3], "Correo Responsable Operaciones",30));
                		responsable5.setAreaResponsable(validaNoObl(ope[1], "Área Responsable Responsable Operaciones",282));
                		responsable5.setIdTipoResponsable(5);
                		if (responsable5.getNombre() != null) {
                			resp.add(responsable5);
                		}   
            		   			
	    		} else {
	        		ErrorArchivo error = new ErrorArchivo();
	    			error.setCampo("Responsable Operaciones");
	    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_PIPE));
	    			this.erroresMapeo.add(error);  
	        	}
    		}
    	} else {
    		valida(row.getCell(24), "Responsable Operaciones",100);
    	}
    	
    	if (row.getCell(26) != null && row.getCell(26).getStringCellValue() != null) {
    		if (!row.getCell(26).getStringCellValue().contains("|")) {
    			ErrorArchivo error = new ErrorArchivo();
    			error.setCampo("Usuario Administrador");
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_PIPE));
    			this.erroresMapeo.add(error);    
    		} else {
        		String[] adm = row.getCell(26).getStringCellValue().split("\\|");
        		if (adm.length == 4) {        			
                		Responsables responsable6 = new Responsables();
                		responsable6.setNombre(valida(adm[0], "Nombre Usuario Administrador",279));
                		if (adm[2].matches("[0-9]*")) {
                			responsable6.setExtension(validaNoObl(adm[2], "Extensión Usuario Administrador",6));
    	        		} else {
    		        		ErrorArchivo error = new ErrorArchivo();
    	     		    	error.setCampo("Extensión Usuario Administrador");
    	     	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    	     	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VALIDO_CODIGO));
    		    			this.erroresMapeo.add(error);  
    	        		}  
                		responsable6.setCorreo(valida(adm[3], "Correo Usuario Administrador",30));
                		responsable6.setAreaResponsable(validaNoObl(adm[1], "Área Responsable Usuario Administrador",282));
                		responsable6.setIdTipoResponsable(6);
                		if (responsable6.getNombre() != null) {
                			resp.add(responsable6);
                		}   
	    		} else {
	        		ErrorArchivo error = new ErrorArchivo();
	    			error.setCampo("Usuario Administrador");
	    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_PIPE));
	    			this.erroresMapeo.add(error);  
	        	}
    		}
    	} else {
    		valida(row.getCell(26), "Usuario Administrador",100);
    	}
    	
    	if (row.getCell(27) != null && row.getCell(27).getStringCellValue() != null) {
    		if (!row.getCell(27).getStringCellValue().contains("|")) {
    			ErrorArchivo error = new ErrorArchivo();
    			error.setCampo("Responsable Negocio");
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_PIPE));
    			this.erroresMapeo.add(error);    
    		} else {
        		String[] neg = row.getCell(27).getStringCellValue().split("\\|");	
        		if (neg.length == 4) {		
                		Responsables responsable7 = new Responsables();
                		responsable7.setNombre(valida(neg[0], "Nombre Responsable Negocio",279));
                		if (neg[2].matches("[0-9]*")) {
                			responsable7.setExtension(validaNoObl(neg[2], "Extensión Responsable Negocio",6));              				
    	        		} else {
    		        		ErrorArchivo error = new ErrorArchivo();
    	     		    	error.setCampo("Extensión Responsable Negocio");
    	     	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    	     	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VALIDO_CODIGO));
    		    			this.erroresMapeo.add(error);  
    	        		} 
                		responsable7.setCorreo(valida(neg[3], "Nombre Responsable Negocio",30));
                		responsable7.setAreaResponsable(validaNoObl(neg[1], "Área Responsable Responsable Negocio",282));
                		responsable7.setIdTipoResponsable(7);
                		if (responsable7.getNombre() != null) {
                			resp.add(responsable7);
                		}     
	    		} else {
	        		ErrorArchivo error = new ErrorArchivo();
	    			error.setCampo("Responsable Negocio");
	    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_PIPE));
	    			this.erroresMapeo.add(error);  
	        	}
    		}
    	} else {
    		valida(row.getCell(27), "Responsable Negocio",100);
    	}
    	
    	if (row.getCell(25) != null && row.getCell(25).getStringCellValue() != null) {
    		if (!row.getCell(25).getStringCellValue().contains("|")) {
    			ErrorArchivo error = new ErrorArchivo();
    			error.setCampo("Responsable Seguridad");
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_PIPE));
    			this.erroresMapeo.add(error);    
    		} else {
        		String[] seg = row.getCell(25).getStringCellValue().split("\\|");  	
        		if (seg.length == 4) {           
        				Responsables responsable8 = new Responsables();
                		responsable8.setNombre(valida(seg[0], "Nombre Responsable Seguridad",279)); 		 		          		
            			if (seg[2].matches("[0-9]*")) {
            				responsable8.setExtension(validaNoObl(seg[2], "Extensión Responsable Seguridad",6));
    	        		} else {
    		        		ErrorArchivo error = new ErrorArchivo();
    	     		    	error.setCampo("Extensión Responsable Seguridad");
    	     	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    	     	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VALIDO_CODIGO));
    		    			this.erroresMapeo.add(error);  
    	        		} 	
                		responsable8.setCorreo(valida(seg[3], "Correo Responsable Seguridad",30));
                		responsable8.setAreaResponsable(validaNoObl(seg[1], "Área Responsable Responsable Seguridad",282));
                		responsable8.setIdTipoResponsable(8);
                		if (responsable8.getNombre() != null) {
                			resp.add(responsable8);
                		}         
	    		} else {
	        		ErrorArchivo error = new ErrorArchivo();
	    			error.setCampo("Responsable Seguridad");
	    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_PIPE));
	    			this.erroresMapeo.add(error);  
	        	}    		
    		}
    	} else {
    		valida(row.getCell(25), "Responsable Seguridad",100);
    	}
    		
    	return resp;
    }
    
    /**
	 * Valida campo Nombre de una celda de excel: campo vacio y longitud de campo, generando las alertas o errores correspondientes.
	 * @param c Celda de excel.
	 * @param campo Nombre del campo de Aplicativo Funcional a validar.
	 * @param longitud Longitud del campo de Aplicativo Funcional a validar.
	 * @return Nombre de celda de excel.
	 */
    private String validaNombre(Cell c, String campo, int longitud) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") 
    				&& !c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){
    			if(c.getStringCellValue().length()>longitud) {
    				String mensaje = Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_LONGITUD);
    				mensaje = mensaje.replaceAll("<LONG>", longitud+"");
    				error.setCampo(campo);
    		    	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    		    	error.setMensaje(mensaje);
    				this.erroresMapeo.add(error);
    				return c.getStringCellValue().substring(0, longitud);
    			}else {
    				return c.getStringCellValue();
    			}
    			
    		}
    	} else {
			error.setCampo(campo);
			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));			
		}
		this.erroresMapeo.add(error);
		return null;
    }
    
    /**
	 * Valida campo Pais de una celda de excel: campo vacio, valor permitido y longitud de campo, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param longitud Longitud del campo de Aplicativo Funcional a validar.
	 * @return Valor de celda de excel.
	 */
    private String validaPais(Cell c, int longitud) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") 
    				&& !c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){
    			if (!c.getStringCellValue().equals("México")) {
    				error.setCampo("País");
    				error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    				error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_PAIS_MEX));
    				this.erroresMapeo.add(error);
    				return null;	
    			} else if(c.getStringCellValue().length()>longitud) {
    				String mensaje = Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_LONGITUD);
    				mensaje = mensaje.replaceAll("<LONG>", longitud+"");
    				error.setCampo("País");
    		    	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    		    	error.setMensaje(mensaje);
    				this.erroresMapeo.add(error);
    				return c.getStringCellValue().substring(0, longitud);
    			}else {
    				return c.getStringCellValue();
    			}
    			
    		}
    	} else {
			error.setCampo("País");
			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));			
		}
		this.erroresMapeo.add(error);
		return null;
    }
    
    /**
   	 * Valida valor de una celda de excel: campo vacio y longitud de campo, generando las alertas correspondientes.
   	 * @param c Celda de excel.
   	 * @param campo Nombre del campo de Aplicativo Funcional a validar.
   	 * @param longitud Longitud del campo de Aplicativo Funcional a validar.
   	 * @return Valor de celda de excel.
   	 */
    private String validaNoObl(Cell c, String campo, int longitud) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") 
    				&& !c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){
    			if(c.getStringCellValue().length()>longitud) {
    				String mensaje = Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_LONGITUD);
    				mensaje = mensaje.replaceAll("<LONG>", longitud+"");
    				error.setCampo(campo);
    		    	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    		    	error.setMensaje(mensaje);
    				this.erroresMapeo.add(error);
    				return c.getStringCellValue().substring(0, longitud);
    			}else {
    				return c.getStringCellValue();
    			}
    			
    		}
    	} else {
			error.setCampo(campo);
			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VACIO));
		}
		this.erroresMapeo.add(error);    	
		return null;
    }
    
    /**
	 * Valida si un string es numero.
	 * @param strNum String a validar.
	 * @return Booleano indicando si es o no numero.
	 */
    public static boolean isNumeric(String strNum) {
        try {
            int d = Integer.parseInt(strNum);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }

	final static Collator instance = Collator.getInstance();
	
	/**
	 * Compara 2 strings removiendo sus acentos, para determinar si son iguales.
	 * @param a String.
	 * @param b String.
	 * @return Booleano indicando si los strings son o no iguales.
	 */
    private boolean removeAccents(String a, String b) {
        instance.setStrength(Collator.NO_DECOMPOSITION);  
        return (instance.compare(a,b) == 0 || a.equals(b));
    }
    
    /**
	 * Valida valor de una celda de excel: campo vacio, formato de fecha y longitud de campo, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param campo Nombre del campo de Aplicativo Funcional a validar.
	 * @param longitud Longitud del campo de Aplicativo Funcional a validar.
	 * @return Valor de celda de excel.
	 */
    private String validaFecha(Cell c, String campo, int longitud) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") 
    				&& !c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){
    			if(c.getStringCellValue().length()>longitud) {
    				String mensaje = Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_LONGITUD);
    				mensaje = mensaje.replaceAll("<LONG>", longitud+"");
    				error.setCampo(campo);
    		    	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    		    	error.setMensaje(mensaje);
    			} else {
    			    if(c.getStringCellValue().contains("-")){
    				   String[] div = c.getStringCellValue().trim().split("-");
    				   String year = div[0];
    				   String month = div[1];
    				   String day = div[2];
    				   if (isNumeric(year) && isNumeric(month) && isNumeric(day)  &&
    						   year.length() == 4 && month.length() == 2 && day.length() == 2) {
    					String sDate1 = c.getStringCellValue();
    					Date evaluada = null;
    					Date hoy = new Date();
						try {
							evaluada = new SimpleDateFormat("yyyy-MM-dd").parse(sDate1);
							if (evaluada.compareTo(hoy) <= 0) {
								return c.getStringCellValue();
							} else {
								error.setCampo(campo);
	    						error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	    						error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VALIDO_FECHA_2));
							}
						} catch (ParseException e) {
							e.printStackTrace();
						}	
    				   } else {
    						error.setCampo(campo);
    						error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    						error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VALIDO_FECHA));    					   
    				   }
    				} else {
						error.setCampo(campo);
						error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
						error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VALIDO_FECHA));     					
    				}
    			}    			
    		}
    	} else {
			error.setCampo(campo);
			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VACIO));
		}
		this.erroresMapeo.add(error);    	
		return null;
    }

	/**
	 * Valida campo Codigo de una celda de excel: campo vacio, estructura numerica y longitud de campo, generando las alertas y errores correspondientes.
	 * @param c Celda de excel.
	 * @param campo Nombre del campo de Aplicativo Funcional a validar.
	 * @param longitud Longitud del campo de Aplicativo Funcional a validar.
	 * @return Valor de celda de excel.
	 */
    private String validaCodigo(Cell c, String campo, int longitud) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") 
    				&& !c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().equals("")){
    			String corb = null;
			    if(c.getStringCellValue().contains("_")){
				   String[] div = c.getStringCellValue().trim().split("_");
				   corb = div[1];
				}else {
				   corb = c.getStringCellValue();
				}
    			if (corb.matches("[0-9]*")) {
    				if(corb.length()>longitud) {
        				String mensaje = Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_LONGITUD);
        				mensaje = mensaje.replaceAll("<LONG>", longitud+"");
        				error.setCampo(campo);
        		    	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
        		    	error.setMensaje(mensaje);
        				this.erroresMapeo.add(error);
        				return corb.substring(0, longitud);
        			}else {
        				return corb;
        			}
    		    }
    			 else {
     		    	error.setCampo(campo);
     	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
     	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VALIDO_CODIGO));
     	        	this.erroresMapeo.add(error);
    				return corb;
     		    }
    		} else {
	        	error.setCampo(campo);
	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ERROR));	    		
	    	}
    	} else {
        	error.setCampo(campo);
        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ERROR));    		
    	}
		this.erroresMapeo.add(error);  
		return null;
    }

	/**
	 * Valida valor de una celda de excel: campo vacio, estructura numerica y longitud de campo, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param campo Nombre del campo de Aplicativo Funcional a validar.
	 * @param longitud Longitud del campo de Aplicativo Funcional a validar.
	 * @return Valor de celda de excel.
	 */
    private String validaDepYear(Cell c, String campo, int longitud) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") 
    				&& !c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().equals("")){
    			if (c.getStringCellValue().matches("[0-9]*")) {
    				if(c.getStringCellValue().length()>longitud) {
        				String mensaje = Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_LONGITUD);
        				mensaje = mensaje.replaceAll("<LONG>", longitud+"");
        				error.setCampo(campo);
        		    	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
        		    	error.setMensaje(mensaje);
        				this.erroresMapeo.add(error);
        				return c.getStringCellValue().substring(0, longitud);
        			}else {
        				return c.getStringCellValue();
        			}
    		    }
    			 else {
     		    	error.setCampo(campo);
     	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
     	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VALIDO_CODIGO));
     	        	this.erroresMapeo.add(error);
    				return c.getStringCellValue();
     		    }
    		} else {
	        	error.setCampo(campo);
	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));	    		
	    	}
    	} else {
        	error.setCampo(campo);
        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));    		
    	}
		this.erroresMapeo.add(error);  
		return null;
    }

    /**
	 * Valida si un string es numero real.
	 * @param strNum String a validar.
	 * @return Booleano indicando si es o no numero real.
	 */
    public static boolean isNumericDouble(String strNum) {
        try {
        	 double d = Double.parseDouble(strNum);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }

	/**
	 * Valida campo Cloud de una celda de excel: campo vacio, estructura numerica, 
	 * rango de valores permitidos y longitud de campo, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param campo Nombre del campo de Aplicativo Funcional a validar.
	 * @param longitud Longitud del campo de Aplicativo Funcional a validar.
	 * @return Valor de celda de excel.
	 */
    private String validaCloud(Cell c, String campo, int longitud) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") 
    				&& !c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().equals("")){
    			if (isNumericDouble(c.getStringCellValue())) {
    				if(c.getStringCellValue().length()>longitud) {
        				String mensaje = Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_LONGITUD);
        				mensaje = mensaje.replaceAll("<LONG>", longitud+"");
        				error.setCampo(campo);
        		    	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
        		    	error.setMensaje(mensaje);
        		    	if (isNumericDouble(c.getStringCellValue().substring(0, longitud))) {
        		    		if (Double.parseDouble(c.getStringCellValue()) > 100 || Double.parseDouble(c.getStringCellValue()) < 0) {
                 		    	error.setCampo(campo);
                 	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
                 	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VALIDO_CIEN));      
            				} else {
	        		    		this.erroresMapeo.add(error);
	            				return c.getStringCellValue().substring(0, longitud);
            				}
        		    	} else {
             		    	error.setCampo(campo);
             	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
             	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VALIDO_CODIGO));        		    		
        		    	}
        			}else {
        				if (Double.parseDouble(c.getStringCellValue()) > 100 || Double.parseDouble(c.getStringCellValue()) < 0) {
             		    	error.setCampo(campo);
             	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
             	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VALIDO_CIEN));      
        				} else {
        					return c.getStringCellValue();
        				}
        			}
    		    } else {
     		    	error.setCampo(campo);
     	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
     	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VALIDO_CODIGO));
     		    }
    		} else {
	        	error.setCampo(campo);
	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));	    		
	    	}
    	} else {
        	error.setCampo(campo);
        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));    		
    	}
		this.erroresMapeo.add(error);  
		return null;
    }

	/**
	 * Valida campo Punto de Recuperacion de una celda de excel: campo vacio y valores permitidos, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param campo Nombre del campo de Aplicativo Funcional a validar.
	 * @return Valor de celda de excel.
	 */
    private String validaPuntoRecuperacion(Cell c, String campo) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Información") && !c.getStringCellValue().equals("Sin Datos") && 
    				!c.getStringCellValue().trim().equals("")){

    			if(c.getStringCellValue().equals("0") || c.getStringCellValue().equals("1") || 
    					c.getStringCellValue().equals("2") || c.getStringCellValue().equals("3") || 
    					c.getStringCellValue().equals("4") || c.getStringCellValue().equals("5") || 
    					c.getStringCellValue().equals("6") || c.getStringCellValue().equals("7") || 
    					c.getStringCellValue().equals("8") || c.getStringCellValue().equals("9") || 
    					c.getStringCellValue().equals("10") || c.getStringCellValue().equals("12") || 
    					c.getStringCellValue().equals("24") || c.getStringCellValue().equals("48") || 
    					c.getStringCellValue().equals("72") || c.getStringCellValue().equals("1 semana") || 
    					c.getStringCellValue().equals("Sin informar") || c.getStringCellValue().equals("NA")) {
    				return c.getStringCellValue();
    			}
    			else {
    				error.setCampo(campo);
    				error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    				error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    			}

    		}
    		else {
    			error.setCampo(campo);
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VACIO));
    		}

    	}else {
    		error.setCampo(campo);
    		error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    		error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VACIO));
    	}
		this.erroresMapeo.add(error);
		return null;
    }

    /**
	 * Valida campo Tiempo de Recuperacion de una celda de excel: campo vacio y valores permitidos, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param campo Nombre del campo de Aplicativo Funcional a validar.
	 * @return Valor de celda de excel.
	 */
    private String validaTiempoRecuperacion(Cell c, String campo) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Información") && !c.getStringCellValue().equals("Sin Datos") && 
    				!c.getStringCellValue().trim().equals("")){

    			if(c.getStringCellValue().equals("0") || c.getStringCellValue().equals("1") || 
    					c.getStringCellValue().equals("2") || c.getStringCellValue().equals("3") || 
    					c.getStringCellValue().equals("4") || c.getStringCellValue().equals("5") || 
    					c.getStringCellValue().equals("6") || c.getStringCellValue().equals("7") || 
    					c.getStringCellValue().equals("8") || c.getStringCellValue().equals("9") || 
    					c.getStringCellValue().equals("10") || c.getStringCellValue().equals("12") || 
    					c.getStringCellValue().equals("24") || c.getStringCellValue().equals("48") || 
    					c.getStringCellValue().equals("72") || c.getStringCellValue().equals("1 semana") || 
    					c.getStringCellValue().equals("Sin informar") || c.getStringCellValue().equals("NA")) {
    				return c.getStringCellValue();
    			}
    			else {
    				error.setCampo(campo);
    				error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    				error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    			}

    		}
    		else {
    			error.setCampo(campo);
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VACIO));
    		}

    	}else {
    		error.setCampo(campo);
    		error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    		error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VACIO));
    	}
		this.erroresMapeo.add(error);
		return null;
    }

	/**
	 * Valida campo Vinculado Proceso una celda de excel: campo vacio y obligatoriedad, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param campo Nombre del campo de Aplicativo Funcional a validar.
	 * @return Valor de celda de excel.
	 */
    private String validaVinculadoProceso(Cell c, String campo) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Información") && !c.getStringCellValue().equals("Sin Datos") && 
    				!c.getStringCellValue().trim().equals("")){

    			if(c.getStringCellValue().equals("Si") || c.getStringCellValue().equals("No") ||
    					c.getStringCellValue().equals("Sin Informar")) {
    				return c.getStringCellValue();
    			}
    			else {
    				error.setCampo(campo);
    				error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    				error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    			}

    		}
    		else {
    			error.setCampo(campo);
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));
    		}

    	}else {
    		error.setCampo(campo);
    		error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    		error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));
    	}
		this.erroresMapeo.add(error);
		return null;
    }

	/**
	 * Valida campo Estado de Aplicacion de una celda de excel: campo vacio y valores permitidos, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param campo Nombre del campo de Aplicativo Funcional a validar.
	 * @return Valor de celda de excel.
	 */
    private String validaEstadoAplicacion(Cell c, String campo) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") 
    				&& !c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){

    			if(c.getStringCellValue().equals("Baja") || c.getStringCellValue().equals("Decomisionada") 
    					|| c.getStringCellValue().equals("Desarrollo") || c.getStringCellValue().trim().equals("Desuso/Inactiva")
    					|| c.getStringCellValue().equals("Producción") || c.getStringCellValue().equals("Preproducción")) {
    				return c.getStringCellValue();
    			}
    			else {
    				error.setCampo(campo);
    				error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    				error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    			}

    		}
    		else {
    			error.setCampo(campo);
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));
    		}

    	}else {
    		error.setCampo(campo);
    		error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    		error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));
    	}
		this.erroresMapeo.add(error);
		return null;
    }

    /**
	 * Valida campo Confidencialidad de una celda de excel: campo vacio, obligatoriedad y valores permitidos, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param campo Nombre del campo de Aplicativo Funcional a validar.
	 * @return Valor de celda de excel.
	 */
    private String validaConfidencialidad(Cell c, String campo) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") 
    				&& !c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){
    			if(c.getStringCellValue().equals("A - Confidencial") || c.getStringCellValue().equals("A - Secreto") 
    					|| c.getStringCellValue().equals("B - Confidencial") || c.getStringCellValue().trim().equals("C - Uso interno")
    					|| c.getStringCellValue().equals("C - Público")) {
    				return c.getStringCellValue();
    			}
    			else {
    				error.setCampo(campo);
    				error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    				error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    			}

    		}
    		else {
    			error.setCampo(campo);
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));
    		}

    	}else {
    		error.setCampo(campo);
    		error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    		error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));
    	}
		this.erroresMapeo.add(error);
		return null;
    }

    /**
	 * Valida campo Integridad de una celda de excel: campo vacio, obligatoriedad y valores permitidos, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param campo Nombre del campo de Aplicativo Funcional a validar.
	 * @return Valor de celda de excel.
	 */
    private String validaIntegridad(Cell c, String campo) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") 
    				&& !c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){

    			if(c.getStringCellValue().equals("A - Alta") || c.getStringCellValue().equals("B - Moderada") 
    					|| c.getStringCellValue().equals("C - Baja")) {
    				return c.getStringCellValue();
    			}
    			else {
    				error.setCampo(campo);
    				error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    				error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    			}

    		}
    		else {
    			error.setCampo(campo);
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));
    		}

    	}else {
    		error.setCampo(campo);
    		error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    		error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));
    	}
		this.erroresMapeo.add(error);
		return null;
    }

    /**
	 * Valida campo Disponibilidad de una celda de excel: campo vacio, obligatoriedad y valores permitidos, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param campo Nombre del campo de Aplicativo Funcional a validar.
	 * @return Valor de celda de excel.
	 */
    private String validaDisponibilidad(Cell c, String campo) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") 
    				&& !c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){

    			if(c.getStringCellValue().equals("A - Critica") || c.getStringCellValue().equals("B - Media") 
    					|| c.getStringCellValue().equals("C - Baja")) {
    				return c.getStringCellValue();
    			}
    			else {
    				error.setCampo(campo);
    				error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    				error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    			}

    		}
    		else {
    			error.setCampo(campo);
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));
    		}

    	}else {
    		error.setCampo(campo);
    		error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    		error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));
    	}
		this.erroresMapeo.add(error);
		return null;
    }

    /**
	 * Valida campo Adecuada Dentro Pilares de una celda de excel: campo vacio, obligatoriedad y valores permitidos, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param campo Nombre del campo de Aplicativo Funcional a validar.
	 * @return Valor de celda de excel.
	 */
    private String validaAdecuada_Dentro_Pilares(Cell c, String campo) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") 
    				&& !c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){

    			if(c.getStringCellValue().equals("No y no permite mejora") || c.getStringCellValue().equals("No o Parcialmente, permitiendo mejora") 
    					|| c.getStringCellValue().equals("Si, Totalmente")) {
    				return c.getStringCellValue();
    			}
    			else {
    				error.setCampo(campo);
    				error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    				error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    			}

    		}
    		else {
    			error.setCampo(campo);
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));
    		}
    	}else {
    		error.setCampo(campo);
    		error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    		error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));
    	}
		this.erroresMapeo.add(error);
		return null;
    }

    /**
	 * Valida campo con valores Si o No de una celda de excel: campo vacio, obligatoriedad y valores permitidos, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param campo Nombre del campo de Aplicativo Funcional a validar.
	 * @return Valor de celda de excel.
	 */
    private String validaSiNo(Cell c, String campo) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") 
    				&& !c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){

    			if(c.getStringCellValue().equals("Si") || c.getStringCellValue().equals("No")) {
    				return c.getStringCellValue();
    			}
    			else {
    				error.setCampo(campo);
    				error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    				error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    			}

    		}
    		else {
    			error.setCampo(campo);
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));
    		}
    	}else {
    		error.setCampo(campo);
    		error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    		error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));
    	}
		this.erroresMapeo.add(error);
		return null;
    }

    /**
	 * Valida campo con valores S o N de una celda de excel: campo vacio, obligatoriedad y valores permitidos, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param campo Nombre del campo de Aplicativo Funcional a validar.
	 * @return Valor de celda de excel.
	 */
    private String validaSN(Cell c, String campo, int longitud) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") 
    				&& !c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){
    			
    			String valor = c.getStringCellValue();
    			if(c.getStringCellValue().length()>longitud) {
    				String mensaje = Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_LONGITUD);
    				mensaje = mensaje.replaceAll("<LONG>", longitud+"");
    				error.setCampo(campo);
    		    	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    		    	error.setMensaje(mensaje);
    				this.erroresMapeo.add(error);
    				valor = c.getStringCellValue().substring(0, longitud);
    			}   			
    			
    			if(c.getStringCellValue().equals("S") || c.getStringCellValue().equals("N")) {
    				return valor;
    			}
    			else {
    				error.setCampo(campo);
    				error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    				error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    			}

    		}
    		else {
    			error.setCampo(campo);
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));
    		}
    	}else {
    		error.setCampo(campo);
    		error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    		error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));
    	}
		this.erroresMapeo.add(error);
		return null;
    }

    /**
	 * Valida campo Entidad de una celda de excel: campo vacio, obligatoriedad y valores permitidos, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param entidades Lista de Entidades.
	 * @return Valor de Id de entidad identificada.
	 */
    private Long validaEntidad(Cell c, List<Entidad> entidades) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") && 
				!c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){
				for(Entidad e:entidades){
					if(e.getEntidad() != null && removeAccents(c.getStringCellValue(), e.getEntidad())){
						if(e.getEstatus() == 1) {
							return e.get_id();
						}else {
							error.setCampo("Entidad");
							error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
							error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
						}
					}
				}
				error.setCampo("Entidad");
	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
	    	} else {
	        	error.setCampo("Entidad");
	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));	    		
	    	}
    	} else {
        	error.setCampo("Entidad");
        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));    		
    	}
		this.erroresMapeo.add(error);   	
    	return null;
    }

    /**
	 * Valida campo Mapa de una celda de excel: campo vacio, obligatoriedad y valores permitidos, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param mapas Lista de Mapas.
	 * @return Valor de Id de mapa identificado.
	 */
    private Long validaMapa(Cell c, List<Mapa> mapas) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") && 
    				!c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){
    			for(Mapa e:mapas){
    				if(e.getMapa() != null && removeAccents(c.getStringCellValue(), e.getMapa())){
    					if(e.getEstatus() == 1) {
    						return e.get_id();
    					}else {
    						error.setCampo("Mapa");
    						error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    						error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    					}
    				}
    			}
    			error.setCampo("Mapa");
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    		} else {
	        	error.setCampo("Mapa");
	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));	    		
	    	}
    	} else {
        	error.setCampo("Mapa");
        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));    		
    	}  	
		this.erroresMapeo.add(error);  
    	return null;
    }

    /**
	 * Valida campo Capa de una celda de excel: campo vacio, obligatoriedad y valores permitidos, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param capas Lista de Capas.
	 * @return Valor de Id de capa identificada.
	 */
    private Long validaCapa(Cell c, List<Capa> capas) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") && 
    				!c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){
    			for(Capa e:capas){
    				if(e.getDominio() != null && removeAccents(c.getStringCellValue(), e.getDominio())){
    					if(e.getEstatus() == 1) {
    						return e.get_id();
    					}else {
    						error.setCampo("Dominio de Arquitectura (Capa)");
    						error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    						error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    					}
    				}
    			} 
    			error.setCampo("Dominio de Arquitectura (Capa)");
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    		} else {
    			error.setCampo("Dominio de Arquitectura (Capa)");
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));	    		
    		}
    	} else {
    		error.setCampo("Dominio de Arquitectura (Capa)");
    		error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    		error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));    		
    	} 
    	this.erroresMapeo.add(error);  
    	return null;
    }
    
    /**
	 * Valida campo Codigo Capa de una celda de excel: campo vacio, obligatoriedad y valores permitidos, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param capas Lista de Capas.
	 * @return Valor de celda de excel.
	 */
    private String validaCodigoCapa(Cell c, List<Capa> capas) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") && 
    				!c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){
    			for(Capa e:capas){
    				if(e.getCodigoCapa() != null && e.getCodigoCapa().equals(c.getStringCellValue())){
    					if(e.getEstatus() == 1) {
    						return c.getStringCellValue();
    					}else {
    						error.setCampo("Código de Dominio de arquitectura (Capa)");
    						error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    						error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    					}
    				}
    			} 
    			error.setCampo("Código de Dominio de Arquitectura (Capa)");
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    		} else {
    			error.setCampo("Código de Dominio de Arquitectura (Capa)");
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));	    		
    		}
    	} else {
    		error.setCampo("Código de Dominio de Arquitectura (Capa)");
    		error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    		error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));    		
    	} 
    	this.erroresMapeo.add(error);  
    	return null;
    }

	/**
	 * Valida combinacion de Capa y Codigo Capa: combinaciones permitidas, generando las alertas correspondientes.
	 * @param capas Lista de Capas.
	 */
    private void validaCapaYCodigoCapa(List<Capa> capas) {
    	ErrorArchivo error = new ErrorArchivo();
    	boolean esta = false;
    	if (capa != null && codigoCapa != null){
    		for(Capa e:capas){
    			if(e.getDominio() != null && e.getCodigoCapa() != null && 
    					e.get_id().equals(capa) && 
    					e.getCodigoCapa().equals(codigoCapa)){
    				if(e.getEstatus() == 1) {
    					esta = true;
    				} else {
    					capa = null;
    					codigoCapa = null;
    					error.setCampo("Dominio de Arquitectura (Capa) y Código");
    					error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    					error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO_COMBINACION));
    			    	this.erroresMapeo.add(error); 
    				}
    			}
    		} 
    		if (!esta) {
    			capa = null;
        		codigoCapa = null;
        		error.setCampo("Dominio de Arquitectura (Capa) y Código");
        		error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
        		error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO_COMBINACION));
            	this.erroresMapeo.add(error); 
    		}   		
    	} else {
			capa = null;
			codigoCapa = null;
			error.setCampo("Dominio de Arquitectura (Capa) y Código");
			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));
	    	this.erroresMapeo.add(error); 	
		}
    }

    /**
	 * Valida campo Sistema de una celda de excel: campo vacio, obligatoriedad y valores permitidos, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param sistemas Lista de Sistemas.
	 * @return Valor de Id de sistema identificado.
	 */
    private Long validaSistema(Cell c, List<Sistema> sistemas) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") && 
    				!c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){
    			for(Sistema e:sistemas){
    				  if(e.getAreaFuncional() != null && removeAccents(c.getStringCellValue(), e.getAreaFuncional())){
    					  if(e.getEstatus() == 1) {
    							return e.get_id();
    						}else {
    							error.setCampo("Área Funcional (Sistema)");
    							error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    							error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    						}
    				  }
    			} 
    			error.setCampo("Área Funcional (Sistema)");
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    		} else {
    			error.setCampo("Área Funcional (Sistema)");
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));	    		
    		}
    	} else {
    		error.setCampo("Área Funcional (Sistema)");
    		error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    		error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));    		
    	} 
    	this.erroresMapeo.add(error);  
    	return null;
    }

    /**
	 * Valida campo Codigo Sistema de una celda de excel: campo vacio, obligatoriedad y valores permitidos, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param sistemas Lista de Sistemas.
	 * @return Valor de celda de excel.
	 */
    private String validaCodigoSistema(Cell c, List<Sistema> sistemas) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") && 
    				!c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){
    			for(Sistema e:sistemas){
    				  if(e.getCodigoSistema() != null && e.getCodigoSistema().equals(c.getStringCellValue())){
    					  if(e.getEstatus() == 1) {
    							return c.getStringCellValue();
    						}else {
    							error.setCampo("Código de Área Funcional (Sistema)");
    							error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    							error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    						}
    				  }
    			} 
    			error.setCampo("Código de Área Funcional (Sistema)");
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    		} else {
    			error.setCampo("Código de Área Funcional (Sistema)");
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));	    		
    		}
    	} else {
    		error.setCampo("Código de Área Funcional (Sistema)");
    		error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    		error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));    		
    	} 
    	this.erroresMapeo.add(error);  
    	return null;
    }

    /**
	 * Valida combinacion de Sistema y Codigo Sistema: combinaciones permitidas, generando las alertas correspondientes.
	 * @param sistemas Lista de Sistemas.
	 */
    private void validaSistemaYCodigoSistema(List<Sistema> sistemas) {
    	ErrorArchivo error = new ErrorArchivo();
    	boolean esta = false;
    	if (sistema != null && codigoSistema != null){
    		for(Sistema e:sistemas){
    			  if(e.get_id() != null && e.getCodigoSistema() != null && 
    				e.get_id().equals(sistema) && 
    				e.getCodigoSistema().equals(codigoSistema)){
    				  if(e.getEstatus() == 1) {
    					  esta = true;	
    				  } else {
    					sistema =  null;
    					codigoSistema = null;
  						error.setCampo("Área Funcional (Sistema) y Código");
  						error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
  						error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO_COMBINACION));
  				    	this.erroresMapeo.add(error); 
    				  }
    			  }
    		}
    		if (!esta) {
    			sistema =  null;
        		codigoSistema = null;
        		error.setCampo("Área Funcional (Sistema) y Código");
        		error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
        		error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO_COMBINACION));
            	this.erroresMapeo.add(error); 
    		}    		
    	} else {
			sistema =  null;
			codigoSistema = null;
			error.setCampo("Área Funcional (Sistema) y Código");
			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));
	    	this.erroresMapeo.add(error); 
		}    		
    }

    /**
	 * Valida campo Subsistema de una celda de excel: campo vacio, obligatoriedad y valores permitidos, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param subsistemas Lista de Subsistemas.
	 * @return Valor de Id de subsistema identificado.
	 */
    private Long validaSubsistema(Cell c, List<Subsistema> subsistemas) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") && 
    				!c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){
    			for(Subsistema e:subsistemas){
    				 if(e.getSubArea() != null && removeAccents(c.getStringCellValue(), e.getSubArea())){
    					  if(e.getEstatus() == 1) {
    							return e.get_id();
    						}else {
    							error.setCampo("Subárea Funcional (Subsistema)");
    							error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    							error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    						}
    				  }
    			} 
    			error.setCampo("Subárea Funcional (Subsistema)");
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    		} else {
    			error.setCampo("Subárea Funcional (Subsistema)");
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));	    		
    		}
    	} else {
    		error.setCampo("Subárea Funcional (Subsistema)");
    		error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    		error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));    		
    	} 
    	this.erroresMapeo.add(error);  
    	return null;
    }

    /**
	 * Valida campo Codigo Subsistema de una celda de excel: campo vacio, obligatoriedad y valores permitidos, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param subsistemas Lista de Subistemas.
	 * @return Valor de celda de excel.
	 */
    private String validaCodigoSubsistema(Cell c, List<Subsistema> subsistemas) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") && 
    				!c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){
    			for(Subsistema e:subsistemas){
    				  if(e.getCodigoSubsistema() != null && e.getCodigoSubsistema().equals(c.getStringCellValue())){
    					  if(e.getEstatus() == 1) {
    							return c.getStringCellValue();
    						}else {
    							error.setCampo("Subárea Funcional (Subsistema)");
    							error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    							error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    						}
    				  }
    			} 
    			error.setCampo("Código de Subárea Funcional (Subsistema)");
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    		} else {
    			error.setCampo("Código de Subárea Funcional (Subsistema)");
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));	    		
    		}
    	} else {
    		error.setCampo("Código de Subárea Funcional (Subsistema)");
    		error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    		error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));    		
    	} 
    	this.erroresMapeo.add(error);  
    	return null;
    }

    /**
	 * Valida combinacion de Subsistema y Codigo Subsistema: combinaciones permitidas, generando las alertas correspondientes.
	 * @param subsistemas Lista de Subsistemas.
	 */	
    private void validaSubsistemayCodigoSubsistema(List<Subsistema> subsistemas) {
    	ErrorArchivo error = new ErrorArchivo();
    	boolean esta = false;
    	if (subsistema != null && codigoSubsistema != null){
    		for(Subsistema e:subsistemas){
    			  if(e.getSubArea() != null && e.getCodigoSubsistema() != null && 
    				e.get_id().equals(subsistema) && 
    				e.getCodigoSubsistema().equals(codigoSubsistema)){
    				  if(e.getEstatus() == 1) {
    					  esta = true;
    				  } else {
    						subsistema =  null;
    						codigoSubsistema = null;
    						error.setCampo("Subárea Funcional (Subsistema) y Código");
    						error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    						error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO_COMBINACION));
    				    	this.erroresMapeo.add(error); 
    					}
    			  }
    		}
    		if (!esta) {
    			subsistema =  null;
        		codigoSubsistema = null;
        		error.setCampo("Subárea Funcional (Subsistema) y Código");
        		error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
        		error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO_COMBINACION));
            	this.erroresMapeo.add(error); 
    		}    		
    	} else {
			subsistema =  null;
			codigoSubsistema = null;
			error.setCampo("Subárea Funcional (Subsistema) y Código");
			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));
	    	this.erroresMapeo.add(error); 	
		}
    }

    /**
	 * Valida campo Categoria de una celda de excel: campo vacio, obligatoriedad y valores permitidos, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param categorias Lista de Categorias.
	 * @return Valor de Id de categoria identificada.
	 */
    private Long validaCategoria(Cell c, List<CategoriaAplicativo> categorias) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") && 
    				!c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){
    			for(CategoriaAplicativo e:categorias){
    				if(e.getCategoria() != null && removeAccents(c.getStringCellValue(), e.getCategoria())){
    					if(e.getEstatus() == 1) {
    						return e.get_id();
    					}else {
    						error.setCampo("Categoría Aplicación");
    						error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    						error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    					}
    				}
    			} 
				error.setCampo("Categoría Aplicación");
	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
	    	} else {
	        	error.setCampo("Categoría Aplicación");
	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));	    		
	    	}
    	} else {
        	error.setCampo("Categoría Aplicación");
        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));    		
    	}   	
		this.erroresMapeo.add(error);  
    	return null;
    }

    /**
	 * Valida campo Bian Afr de una celda de excel: campo vacio, obligatoriedad y valores permitidos, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param bianafrs Lista de Bian Afr.
	 * @return Valor de Id de Bian Afr identificada.
	 */
    private Long validaBnombre(Cell c, List<BianAfr> bianafrs) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") && 
    				!c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){
    			for(BianAfr e: bianafrs){    				
    				if(e.getBianAfr() != null && removeAccents(c.getStringCellValue(), e.getBianAfr())){		    				
    					if(e.getEstatus() == 1) {
    						return e.get_id();
    					}else {
    						error.setCampo("BIAN - Nombre");
    						error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    						error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    					}
    				}
    			} 
				error.setCampo("BIAN - Nombre");
	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
	    	} else {
	        	error.setCampo("BIAN - Nombre");
	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));	    		
	    	}
    	} else {
        	error.setCampo("BIAN - Nombre");
        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));    		
    	}   	
		this.erroresMapeo.add(error);  
    	return null;
    }

    /**
	 * Valida campo Bian Criticidad de una celda de excel: campo vacio, obligatoriedad y valores permitidos, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param biancriticidades Lista de Bian Criticidades.
	 * @return Valor de Id de Bian Criticidad identificada.
	 */
    private Long validaBCriticidad(Cell c, List<BianCriticidad> biancriticidades) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") && 
    				!c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){
    			for(BianCriticidad e:biancriticidades){
    				if(e.getBianCriticidad() != null && removeAccents(c.getStringCellValue(), e.getBianCriticidad())){
    					if(e.getEstatus() == 1) {
    						return e.get_id();
    					}else {
    						error.setCampo("BIAN - Criticidad");
    						error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    						error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    					}
    				}
    			} 
				error.setCampo("BIAN - Criticidad");
	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
	    	} else {
	        	error.setCampo("BIAN - Criticidad");
	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));	    		
	    	}
    	} else {
        	error.setCampo("BIAN - Criticidad");
        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));    		
    	}  	
		this.erroresMapeo.add(error);  
    	return null;
    }

    /**
	 * Valida campo Bian Nivel 1 de una celda de excel: campo vacio, obligatoriedad y valores permitidos, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param dominios Lista de Bian Nivel 1.
	 * @return Valor de Id de Bian Nivel 1 identificada.
	 */
    private Long validaBNivel1(Cell c, List<Dominio> dominios) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") && 
    				!c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){
    			for(Dominio e:dominios){
    				if(e.getDominio() != null && removeAccents(c.getStringCellValue(), e.getDominio())){
    					if(e.getEstatus() == 1) {
    						return e.get_id();
    					}else {
    						error.setCampo("BIAN - Nivel 1");
    						error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    						error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    					}
    				}
    			} 
				error.setCampo("BIAN - Nivel 1");
	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
	    	} else {
	        	error.setCampo("BIAN - Nivel 1");
	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));	    		
	    	}
    	} else {
        	error.setCampo("BIAN - Nivel 1");
        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));    		
    	}   	
		this.erroresMapeo.add(error);  
    	return null;
    }

    /**
	 * Valida campo Bian Nivel 2 de una celda de excel: campo vacio, obligatoriedad y valores permitidos, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param bianniveles2 Lista de Bian Nivel 2.
	 * @return Valor de Id de Bian Nivel 2 identificada.
	 */
    private Long validaBNivel2(Cell c, List<BianNivel2> bianniveles2) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") && 
    				!c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){
    			for(BianNivel2 e: bianniveles2){
    				if(e.getBian_nivel2() != null && removeAccents(c.getStringCellValue(), e.getBian_nivel2())){
    					if(e.getEstatus() == 1) {
    						return e.get_id();
    					}else {
    						error.setCampo("BIAN - Nivel 2");
    						error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    						error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    					}
    				}
    			} 
				error.setCampo("BIAN - Nivel 2");
	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
	    	} else {
	        	error.setCampo("BIAN - Nivel 2");
	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));	    		
	    	}
    	} else {
        	error.setCampo("BIAN - Nivel 2");
        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));    		
    	} 	
		this.erroresMapeo.add(error);  
    	return null;
    }

    /**
	 * Valida campo Bian Nivel 3 de una celda de excel: campo vacio, obligatoriedad y valores permitidos, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param bianniveles3 Lista de Bian Nivel 3.
	 * @return Valor de Id de Bian Nivel 3 identificada.
	 */
    private Long validaBNivel3(Cell c, List<BianNivel3> bianniveles3) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") && 
    				!c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){
    			for(BianNivel3 e: bianniveles3){
    				if(e.getBian_nivel3() != null && removeAccents(c.getStringCellValue(), e.getBian_nivel3())){
    					if(e.getEstatus() == 1) {
    						return e.get_id();
    					}else {
    						error.setCampo("BIAN - Nivel 3");
    						error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    						error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    					}
    				}
    			} 
				error.setCampo("BIAN - Nivel 3");
	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
	    	} else {
	        	error.setCampo("BIAN - Nivel 3");
	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));	    		
	    	}
    	} else {
        	error.setCampo("BIAN - Nivel 3");
        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));    		
    	}   	
		this.erroresMapeo.add(error);  
    	return null;
    }

    /**
	 * Valida campo Paquete Bian de una celda de excel: campo vacio, obligatoriedad y valores permitidos, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param paquetesbian Lista de Paquetes Bian.
	 * @return Valor de Id de Bian Paquete identificada.
	 */
    private Long validaBPaquete(Cell c, List<PaqueteBian> paquetesbian) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") && 
    				!c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){
    			for(PaqueteBian e:paquetesbian){
    				if(e.getPaqueteBian() != null && removeAccents(c.getStringCellValue(), e.getPaqueteBian())){
    					if(e.getEstatus() == 1) {
    						return e.get_id();
    					}else {
    						error.setCampo("BIAN - Paquete");
    						error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    						error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    					}
    				}
    			} 
				error.setCampo("BIAN - Paquete");
	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
	    	} else {
	        	error.setCampo("BIAN - Paquete");
	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));	    		
	    	}
    	} else {
        	error.setCampo("BIAN - Paquete");
        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));    		
    	}   	
		this.erroresMapeo.add(error);  
    	return null;
    }

    /**
	 * Valida campo Granularidad Bian de una celda de excel: campo vacio, obligatoriedad y valores permitidos, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param granularidadesbian Lista de Granularidades Bian.
	 * @return Valor de Id de Bian Granularidad identificada.
	 */
    private Long validaBGranularidad(Cell c, List<GranularidadBian> granularidadesbian) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") && 
				!c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){
    			for(GranularidadBian e:granularidadesbian){
    				if(e.getGranularidadBian() != null && removeAccents(c.getStringCellValue(), e.getGranularidadBian())){
    					if(e.getEstatus() == 1) {
    						return e.get_id();
    					}else {
    						error.setCampo("BIAN - Granularidad");
    						error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    						error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    					}
    				}
    			} 
				error.setCampo("BIAN - Granularidad");
	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
	    	} else {
	        	error.setCampo("BIAN - Granularidad");
	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));	    		
	    	}
    	} else {
        	error.setCampo("BIAN - Granularidad");
        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));    		
    	}   	
		this.erroresMapeo.add(error);  
    	return null;
    }
    
	public String get_id() {
		return _id;
	}
	public void set_id(String _id) {
		this._id = _id;
	}
	public String getCodigoFuncional() {
		return codigoFuncional;
	}
	public void setCodigoFuncional(String codigoFuncional) {
		this.codigoFuncional = codigoFuncional;
	}
	public String getNombreFuncional() {
		return nombreFuncional;
	}
	public void setNombreFuncional(String nombreFuncional) {
		this.nombreFuncional = nombreFuncional;
	}
	public String getEstadoAplicacion() {
		return estadoAplicacion;
	}
	public void setEstadoAplicacion(String estadoAplicacion) {
		this.estadoAplicacion = estadoAplicacion;
	}
	public Long getCategoriaAplicacion() {
		return categoriaAplicacion;
	}
	public void setCategoriaAplicacion(Long categoriaAplicacion) {
		this.categoriaAplicacion = categoriaAplicacion;
	}
	public String getDescFuncionalidad() {
		return descFuncionalidad;
	}
	public void setDescFuncionalidad(String descFuncionalidad) {
		this.descFuncionalidad = descFuncionalidad;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public Long getEntidad() {
		return entidad;
	}
	public void setEntidad(Long entidad) {
		this.entidad = entidad;
	}
	public Long getMapa() {
		return mapa;
	}
	public void setMapa(Long mapa) {
		this.mapa = mapa;
	}
	public Long getCapa() {
		return capa;
	}
	public void setCapa(Long capa) {
		this.capa = capa;
	}
	public Long getSistema() {
		return sistema;
	}
	public void setSistema(Long sistema) {
		this.sistema = sistema;
	}
	public Long getSubsistema() {
		return subsistema;
	}
	public void setSubsistema(Long subsistema) {
		this.subsistema = subsistema;
	}
	public String getConfidencialidadInfo() {
		return confidencialidadInfo;
	}
	public void setConfidencialidadInfo(String confidencialidadInfo) {
		this.confidencialidadInfo = confidencialidadInfo;
	}
	public String getIntegridadInfo() {
		return integridadInfo;
	}
	public void setIntegridadInfo(String integridadInfo) {
		this.integridadInfo = integridadInfo;
	}
	public String getDisponibilidadInfo() {
		return disponibilidadInfo;
	}
	public void setDisponibilidadInfo(String disponibilidadInfo) {
		this.disponibilidadInfo = disponibilidadInfo;
	}
	public String getVinculadoProceso() {
		return vinculadoProceso;
	}
	public void setVinculadoProceso(String vinculadoProceso) {
		this.vinculadoProceso = vinculadoProceso;
	}
	public String getUltimaPrueba() {
		return ultimaPrueba;
	}
	public void setUltimaPrueba(String ultimaPrueba) {
		this.ultimaPrueba = ultimaPrueba;
	}
	public String getExisteRecuperacion() {
		return existeRecuperacion;
	}
	public void setExisteRecuperacion(String existeRecuperacion) {
		this.existeRecuperacion = existeRecuperacion;
	}
	public String getTiempoRecuperacion() {
		return tiempoRecuperacion;
	}
	public void setTiempoRecuperacion(String tiempoRecuperacion) {
		this.tiempoRecuperacion = tiempoRecuperacion;
	}
	public String getPuntoRecuperacion() {
		return puntoRecuperacion;
	}
	public void setPuntoRecuperacion(String puntoRecuperacion) {
		this.puntoRecuperacion = puntoRecuperacion;
	}
	public String getPertenecePruebasCon() {
		return pertenecePruebasCon;
	}
	public void setPertenecePruebasCon(String pertenecePruebasCon) {
		this.pertenecePruebasCon = pertenecePruebasCon;
	}
	public String getAdecuadaNegocio() {
		return adecuadaNegocio;
	}
	public void setAdecuadaNegocio(String adecuadaNegocio) {
		this.adecuadaNegocio = adecuadaNegocio;
	}
	public String getDentroModelo() {
		return dentroModelo;
	}
	public void setDentroModelo(String dentroModelo) {
		this.dentroModelo = dentroModelo;
	}
	public String getPilaresAdecuado() {
		return pilaresAdecuado;
	}
	public void setPilaresAdecuado(String pilaresAdecuado) {
		this.pilaresAdecuado = pilaresAdecuado;
	}
	public String getDeploymentYear() {
		return deploymentYear;
	}
	public void setDeploymentYear(String deploymentYear) {
		this.deploymentYear = deploymentYear;
	}
	public String getApiEnabled() {
		return apiEnabled;
	}
	public void setApiEnabled(String apiEnabled) {
		this.apiEnabled = apiEnabled;
	}
	public String getPublicCloud() {
		return publicCloud;
	}
	public void setPublicCloud(String publicCloud) {
		this.publicCloud = publicCloud;
	}
	public Long getBnombre() {
		return bnombre;
	}
	public void setBnombre(Long bnombre) {
		this.bnombre = bnombre;
	}
	public Long getBcriticidad() {
		return bcriticidad;
	}
	public void setBcriticidad(Long bcriticidad) {
		this.bcriticidad = bcriticidad;
	}
	public Long getBnivel1() {
		return bnivel1;
	}
	public void setBnivel1(Long bnivel1) {
		this.bnivel1 = bnivel1;
	}
	public Long getBnivel2() {
		return bnivel2;
	}
	public void setBnivel2(Long bnivel2) {
		this.bnivel2 = bnivel2;
	}
	public Long getBnivel3() {
		return bnivel3;
	}
	public void setBnivel3(Long bnivel3) {
		this.bnivel3 = bnivel3;
	}
	public Long getBpaquete() {
		return bpaquete;
	}
	public void setBpaquete(Long bpaquete) {
		this.bpaquete = bpaquete;
	}
	public Long getBgranularidad() {
		return bgranularidad;
	}
	public void setBgranularidad(Long bgranularidad) {
		this.bgranularidad = bgranularidad;
	}
	public int getActivo() {
		return activo;
	}
	public void setActivo(int activo) {
		this.activo = activo;
	}
	public int getFinalizado() {
		return finalizado;
	}
	public void setFinalizado(int finalizado) {
		this.finalizado = finalizado;
	}


	public List<ErrorArchivo> getErroresMapeo() {
		return erroresMapeo;
	}


	public void setErroresMapeo(List<ErrorArchivo> erroresMapeo) {
		this.erroresMapeo = erroresMapeo;
	}

	public String getCodigoCapa() {
		return codigoCapa;
	}

	public void setCodigoCapa(String codigoCapa) {
		this.codigoCapa = codigoCapa;
	}

	public String getCodigoSistema() {
		return codigoSistema;
	}

	public void setCodigoSistema(String codigoSistema) {
		this.codigoSistema = codigoSistema;
	}

	public String getCodigoSubsistema() {
		return codigoSubsistema;
	}

	public void setCodigoSubsistema(String codigoSubsistema) {
		this.codigoSubsistema = codigoSubsistema;
	}

	public List<Responsables> getResponsables() {
		return responsables;
	}

	public void setResponsables(List<Responsables> responsables) {
		this.responsables = responsables;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}
}