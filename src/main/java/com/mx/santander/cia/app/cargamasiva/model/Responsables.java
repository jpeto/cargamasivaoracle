package com.mx.santander.cia.app.cargamasiva.model;

public class Responsables {
	private Long _id;
	private String nombre;
	private String extension;
	private String telefono;
	private String celular;
	private String correo;
	private String direccionEjecutiva;
	private String areaResponsable;
	private int idTipoResponsable;
	private String tipoResponsable;	
	private String idAplicativo;
	private int modificado;

	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getExtension() {
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getDireccionEjecutiva() {
		return direccionEjecutiva;
	}
	public void setDireccionEjecutiva(String direccionEjecutiva) {
		this.direccionEjecutiva = direccionEjecutiva;
	}
	public String getAreaResponsable() {
		return areaResponsable;
	}
	public void setAreaResponsable(String areaResponsable) {
		this.areaResponsable = areaResponsable;
	}
	public String getTipoResponsable() {
		return tipoResponsable;
	}
	public void setTipoResponsable(String tipoResponsable) {
		this.tipoResponsable = tipoResponsable;
	}
	public Long get_id() {
		return _id;
	}
	public void set_id(Long _id) {
		this._id = _id;
	}
	public String getIdAplicativo() {
		return idAplicativo;
	}
	public void setIdAplicativo(String idAplicativo) {
		this.idAplicativo = idAplicativo;
	}
	public int getIdTipoResponsable() {
		return idTipoResponsable;
	}
	public void setIdTipoResponsable(int idTipoResponsable) {
		this.idTipoResponsable = idTipoResponsable;
	}
	public int getModificado() {
		return modificado;
	}
	public void setModificado(int modificado) {
		this.modificado = modificado;
	}
	
}