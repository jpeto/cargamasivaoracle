package com.mx.santander.cia.app.cargamasiva;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import feign.Request;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
public class CiaAppCargaMasivaApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(CiaAppCargaMasivaApplication.class, args);
	}

   
}
