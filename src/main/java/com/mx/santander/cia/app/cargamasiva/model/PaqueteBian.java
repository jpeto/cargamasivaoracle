package com.mx.santander.cia.app.cargamasiva.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PaqueteBian {
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Long _id;
	private String paqueteBian;
	private String descripcion;
	private int estatus;
	
	public String getPaqueteBian() {
		return paqueteBian;
	}
	public void setPaqueteBian(String paqueteBian) {
		this.paqueteBian = paqueteBian;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public int getEstatus() {
		return estatus;
	}
	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}
	public Long get_id() {
		return _id;
	}
	public void set_id(Long _id) {
		this._id = _id;
	}
	
}
