package com.mx.santander.cia.app.cargamasiva.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.mx.santander.cia.app.cargamasiva.properties.Propiedades;

public class SheetApplicationData implements Comparable<SheetApplicationData>{
	
	private String orbisCodeTagNum;
	private String svcId;
	private String svcEnv;
	private int fila;
	private String idAT;
	private List<ErrorArchivo> erroresMapeo;
		
	/**
	 * Mapeo de una fila del excel de Infraestructura (hoja de Application Data) y validacion por campo.
	 * @param row Fila de Application Data.
	 * @param aplicativosTecnicos Lista de Aplicativos Tecnicos.
	 */
	public void mapeaValores(Row row, List<AplicativoTecnico> aplicativosTecnicos){
	   this.erroresMapeo = new ArrayList<>();
	   this.orbisCodeTagNum = valida(row.getCell(10),"Orbis Code_TagNum");
	   this.svcId = valida(row.getCell(0),"Svc ID");
	   this.svcEnv = valida(row.getCell(4),"Svc Env");
	   for (AplicativoTecnico at: aplicativosTecnicos) {
		   if (at.getCodigoOrbis().equals(this.orbisCodeTagNum)) {
			   this.idAT = at.get_id();
		   }
	   }
	}
	
	/**
	 * Valida valor de una celda de excel: campo vacio, generando las alertas correspondientes.
	 * @param c Celda de excel a validar.
	 * @param campo Nombre del campo de Application Data a validar.
	 * @return Valor de celda de excel.
	 */
    private String valida(Cell c, String campo) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null ){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") 
    				&& !c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){
    			return c.getStringCellValue();
    		}
    	}
    	error.setCampo(campo);
    	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VACIO));
    	this.erroresMapeo.add(error);
    	return null;

    }
    
    /**
	 * Compara 2 Application Data, para determinar si son iguales por Codigo Orbis y Service Component.
	 * @param o Application Data.
	 * @return Entero indicando si los Application Data son o no iguales.
	 */
    @Override
    public int compareTo(SheetApplicationData o) {
    	String a = new String(this.orbisCodeTagNum+this.svcId);
        String b = new String(o.getOrbisCodeTagNum()+o.svcId);
        return a.compareTo(b);
    }
	
	public String getOrbisCodeTagNum() {
		return orbisCodeTagNum;
	}
	public void setOrbisCodeTagNum(String orbisCodeTagNum) {
		this.orbisCodeTagNum = orbisCodeTagNum;
	}
	public String getSvcId() {
		return svcId;
	}
	public void setSvcId(String svcId) {
		this.svcId = svcId;
	}
	public String getSvcEnv() {
		return svcEnv;
	}
	public void setSvcEnv(String svcEnv) {
		this.svcEnv = svcEnv;
	}
	public int getFila() {
		return fila;
	}
	public void setFila(int fila) {
		this.fila = fila;
	}
	public String getIdAT() {
		return idAT;
	}
	public void setIdAT(String idAT) {
		this.idAT = idAT;
	}
	public List<ErrorArchivo> getErroresMapeo() {
		return erroresMapeo;
	}

	public void setErroresMapeo(List<ErrorArchivo> erroresMapeo) {
		this.erroresMapeo = erroresMapeo;
	}


}
