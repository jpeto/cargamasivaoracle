package com.mx.santander.cia.app.cargamasiva.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.mx.santander.cia.app.cargamasiva.properties.Propiedades;

public class SheetDB implements Comparable<SheetDB> {

	/** Nombre de la base de datos */
	private String dbName;
	/** Ambiente */
	private String funtionalEnviroment;
	/** Status */
	private String statusDb;
	/** Manejador de la BD */
	private String dbSystem;
	/** Versión */
	private String dbVersion;
	/** Nombre del Servidor */
	private String nameLServer;
	/** Service Component ID*/
	private String svcId;
	/** Numero Fila*/
    private int fila;
	/** EOL */
    private String eol;
	private List<ErrorArchivo> erroresMapeo;
    
	/**
	 * Mapeo de una fila del excel de Infraestructura (hoja de Base de Datos) y validacion por campo.
	 * @param row Fila de Base de Datos.
	 * @param software Lista de Softwares.
	 */
	public void mapeaValores(Row row, List<Software> software){
    	this.erroresMapeo = new ArrayList<>();
		this.dbName = valida(row.getCell(8), "DB_NAME");              
		this.funtionalEnviroment = valida(row.getCell(2), "FUNTIONALENVIRONMENT"); 
		this.statusDb = valida(row.getCell(3), "STATUS_DB");            
		this.dbSystem = valida(row.getCell(5), "DB_SYSTEM");          
		this.dbVersion = valida(row.getCell(6), "DB_VERSION");            
		this.nameLServer = valida(row.getCell(10), "NAME_LSERVER"); 
		this.svcId = valida(row.getCell(18), "ASSETID_SCOMM");
		this.eol = validaEOL(this.dbSystem, this.dbVersion, software);
	}
	
	/**
	 * Valida valor de una celda de excel: campo vacio, generando las alertas correspondientes.
	 * @param c Celda de excel a validar.
	 * @param campo Nombre del campo de Base de Datos a validar.
	 * @return Valor de celda de excel.
	 */
    private String valida(Cell c, String campo) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null ){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") 
    			&& !c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){
    			return c.getStringCellValue();    			
    		}
    	}    	
    	error.setCampo(campo);
    	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VACIO));
		this.erroresMapeo.add(error);
		return null;
    }
    
    /**
	 * Valida existencia de Sistema operativo y Version en catalogo de Software para obtener EoL, generando las alertas correspondientes.
	 * @param dbSystem String con sistema operativo.
	 * @param dbVersion String con version.
	 * @param software Lista de Softwares.
	 * @return String con valor de End Of Life.
	 */
    private String validaEOL(String dbSystem, String dbVersion, List<Software> software) {
    	ErrorArchivo error = new ErrorArchivo();    	
    	if (dbSystem != null && dbVersion != null) {
        	boolean esta = false;
        	String llavePM = dbSystem + " " + dbVersion;
    		for(Software s: software){
        		if(s.getName()!=null && s.getVersion()!=null) {
        			String llaveOS = s.getName() + " " + s.getVersion();
        				if(llavePM.equals(llaveOS)){
        					esta=true;
        					if(s.getObsolescence()!=null) {
        						return s.getObsolescence();
        					}
        					else {
        						error.setCampo("End of life");
        						error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
        						error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VACIO));
        						this.erroresMapeo.add(error);
        						return null;					
        					}

        				}
        		}
        	}
        	if(!esta) {
        		error.setCampo("DB_SYSTEM y DB_VERSION");
        		error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
        		error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    			this.erroresMapeo.add(error);
    			return null;
        	} 
    	}
		return null;
    }
    
    /**
	 * Compara 2 Bases de Datos, para determinar si son iguales por Nombre de Servidor, Nombre y Service Component.
	 * @param o Base de Datos.
	 * @return Entero indicando si las Bases de Datos son o no iguales.
	 */
    @Override
    public int compareTo(SheetDB o) {
    	String a = new String(this.nameLServer+this.svcId+this.dbName);
        String b = new String(o.nameLServer+o.svcId+o.dbName);
        return a.compareTo(b);
    }

	public String getDbName() {
		return dbName;
	}

	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	public String getFuntionalEnviroment() {
		return funtionalEnviroment;
	}

	public void setFuntionalEnviroment(String funtionalEnviroment) {
		this.funtionalEnviroment = funtionalEnviroment;
	}

	public String getStatusDb() {
		return statusDb;
	}

	public void setStatusDb(String statusDb) {
		this.statusDb = statusDb;
	}

	public String getDbSystem() {
		return dbSystem;
	}

	public void setDbSystem(String dbSystem) {
		this.dbSystem = dbSystem;
	}

	public String getDbVersion() {
		return dbVersion;
	}

	public void setDbVersion(String dbVersion) {
		this.dbVersion = dbVersion;
	}

	public String getNameLServer() {
		return nameLServer;
	}

	public void setNameLServer(String nameLServer) {
		this.nameLServer = nameLServer;
	}

	public int getFila() {
		return fila;
	}

	public void setFila(int fila) {
		this.fila = fila;
	}

	public String getScid() {
		return svcId;
	}

	public void setScid(String scid) {
		this.svcId = scid;
	}

	public List<ErrorArchivo> getErroresMapeo() {
		return erroresMapeo;
	}

	public void setErroresMapeo(List<ErrorArchivo> erroresMapeo) {
		this.erroresMapeo = erroresMapeo;
	}

	public String getEol() {
		return eol;
	}

	public void setEol(String eol) {
		this.eol = eol;
	}
}
