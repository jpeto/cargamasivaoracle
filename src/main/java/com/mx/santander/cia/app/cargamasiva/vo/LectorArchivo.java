package com.mx.santander.cia.app.cargamasiva.vo;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import com.monitorjbl.xlsx.StreamingReader;
import com.mx.santander.cia.app.cargamasiva.model.AplicativoFuncional;
import com.mx.santander.cia.app.cargamasiva.model.AplicativoTecnico;
import com.mx.santander.cia.app.cargamasiva.model.BianAfr;
import com.mx.santander.cia.app.cargamasiva.model.BianCriticidad;
import com.mx.santander.cia.app.cargamasiva.model.BianNivel2;
import com.mx.santander.cia.app.cargamasiva.model.BianNivel3;
import com.mx.santander.cia.app.cargamasiva.model.Capa;
import com.mx.santander.cia.app.cargamasiva.model.CategoriaAplicativo;
import com.mx.santander.cia.app.cargamasiva.model.CriticidadServicio;
import com.mx.santander.cia.app.cargamasiva.model.Dominio;
import com.mx.santander.cia.app.cargamasiva.model.Entidad;
import com.mx.santander.cia.app.cargamasiva.model.EntidadPropietaria;
import com.mx.santander.cia.app.cargamasiva.model.ErrorArchivo;
import com.mx.santander.cia.app.cargamasiva.model.GranularidadBian;
import com.mx.santander.cia.app.cargamasiva.model.IdentificadorCpd;
import com.mx.santander.cia.app.cargamasiva.model.InfraestructuraStatus;
import com.mx.santander.cia.app.cargamasiva.model.Mapa;
import com.mx.santander.cia.app.cargamasiva.model.PaqueteBian;
import com.mx.santander.cia.app.cargamasiva.model.SheetApplicationData;
import com.mx.santander.cia.app.cargamasiva.model.SheetDB;
import com.mx.santander.cia.app.cargamasiva.model.SheetDBP;
import com.mx.santander.cia.app.cargamasiva.model.SheetRemedy;
import com.mx.santander.cia.app.cargamasiva.model.SheetServidores;
import com.mx.santander.cia.app.cargamasiva.model.Sistema;
import com.mx.santander.cia.app.cargamasiva.model.Software;
import com.mx.santander.cia.app.cargamasiva.model.Subsistema;
import com.mx.santander.cia.app.cargamasiva.model.TipologiaSoftware;
import com.mx.santander.cia.app.cargamasiva.model.Was;
import com.mx.santander.cia.app.cargamasiva.model.Web;



public class LectorArchivo {
	
	private List<SheetApplicationData> applicationData = new ArrayList<>();
	private List<SheetServidores> servidoresLogicos = new ArrayList<>();
	private List<SheetServidores> servidoresAmbientesPrevios = new ArrayList<>();
	private List<SheetDB> DB = new ArrayList<>();
	private List<SheetDBP> DBAP = new ArrayList<>();
	private List<Was> WAS = new ArrayList<>();
	private List<Was> WASP = new ArrayList<>();
	private List<Web> WEB = new ArrayList<>();
	private List<Web> WEBP = new ArrayList<>();

	/**
	 * Lectura de archivo de Aplicativos Funcionales.
	 * @param rutaArchivo Ruta y nombre del archivo de Aplicativos Funcionales.
	 * @param entidades Lista de Entidades.
	 * @param mapas Lista de Mapas.
	 * @param capas Lista de Capas.
	 * @param sistemas Lista de Sistemas.
	 * @param subsistemas Lista de Subsistemas.
	 * @param categorias Lista de Categorias.
	 * @param dominios Lista de Dominios.
	 * @param biancriticidades Lista de Bian Criticidades.
	 * @param bianafrs Lista de Bian Afr.
	 * @param bianniveles2 Lista de Bian Niveles 2.
	 * @param bianniveles3 Lista de Bian Niveles 3.
	 * @param paquetesbian Lista de Paquetes Bian.
	 * @param granularidadesbian Lista de Granularidades Bian.
	 * @return Lista de Aplicativos Funcionales mapeados del archivo de Aplicativos Funcionales, con las alertas y/o errores que aplican por campo.
	 */
	public List<AplicativoFuncional> leerArchivoAplicacionesFuncionales(String rutaArchivo, List<Entidad> entidades, 
			List<Mapa> mapas, List<Capa> capas, List<Sistema> sistemas, List<Subsistema> subsistemas, List<CategoriaAplicativo> categorias, 
			List<Dominio> dominios, List<BianCriticidad> biancriticidades, List<BianAfr> bianafrs, List<BianNivel2> bianniveles2, 
			List<BianNivel3> bianniveles3, List<PaqueteBian> paquetesbian, List<GranularidadBian> granularidadesbian){
		List<AplicativoFuncional> listadoAF = new ArrayList<>();		
		try(
		    InputStream is = new FileInputStream(new File(rutaArchivo));
			Workbook workbook = StreamingReader.builder()
					 .rowCacheSize(100)    // 10
					 .bufferSize(4096)     // 1024
					 .open(is);
				){
			
			Sheet tabAplicacionFuncional = workbook.getSheetAt(0);

			for(Row row : tabAplicacionFuncional){
               if(row.getRowNum()>4){
             	  AplicativoFuncional af = new AplicativoFuncional(); 
                  af.mapeaValores(row, entidades, mapas, capas, sistemas, subsistemas,
                  		categorias, dominios, biancriticidades,bianafrs, bianniveles2, 
                  		bianniveles3, paquetesbian, granularidadesbian);
                  listadoAF.add(af);
               }
			}
			
		} catch (Exception e) {
			e.getMessage();
		}
		
		return listadoAF;
	}
	
	/**
	 * Lectura de archivo de Aplicativos Tecnicos.
	 * @param rutaArchivo Ruta y nombre del archivo de Aplicativos Tecnicos.
	 * @param criticidades Lista de Criticidades.
	 * @param entidades Lista de Entidades Propietarias.
	 * @param tipologias Lista de Tipologias.
	 * @param identificadores Lista de Identificadores Cpd.
	 * @return Lista de Aplicativos Tecnicos mapeados del archivo de Aplicativos Tecnicos, con las alertas y/o errores que aplican por campo.
	 */
	public List<AplicativoTecnico> leerArchivoAplicacionesTecnicas(String rutaArchivo, List<CriticidadServicio> criticidades, 
			List<EntidadPropietaria> entidades, List<TipologiaSoftware> tipologias, List<IdentificadorCpd> identificadores){
		List<AplicativoTecnico> listadoAT = new ArrayList<>();		
		try(
		    InputStream is = new FileInputStream(new File(rutaArchivo));
			Workbook workbook = StreamingReader.builder()
			        .rowCacheSize(100)    // 10
			        .bufferSize(4096)     // 1024
			        .open(is);
		){
			
			Sheet tabAplicacionTecnica = workbook.getSheetAt(0);
		    
			for(Row row : tabAplicacionTecnica){
               if(row.getRowNum()>4){
            	  AplicativoTecnico at = new AplicativoTecnico(); 
                  at.mapeaValores(row, criticidades, entidades, tipologias, identificadores);
                  listadoAT.add(at);
               }
			}
			
		} catch (Exception e) {
			e.getMessage();
		}
		return listadoAT;
	}
	
	/**
	 * Lectura de archivo de Aplicativos Remedy.
	 * @param rutaArchivo Ruta y nombre del archivo de Aplicativos Remedy.
	 * @return Lista de Aplicativos Remedy mapeados del archivo de Aplicativos Remedy, con las alertas y/o errores que aplican por campo.
	 */
	public List<SheetRemedy> leerArchivoAplicacionesRemedy(String rutaArchivo){
		List<SheetRemedy> listadoRemedy = new ArrayList<>();		
		try(
		    InputStream is = new FileInputStream(new File(rutaArchivo));
			Workbook workbook = StreamingReader.builder()
					 .rowCacheSize(100)    // 10
					 .bufferSize(4096)     // 1024
					 .open(is);
				){
			
			Sheet tabRemedy = workbook.getSheetAt(0);

			for(Row row : tabRemedy){
               if(row.getRowNum()>0){
             	  SheetRemedy ar = new SheetRemedy(); 
                  ar.mapeaValores(row);
                  listadoRemedy.add(ar);
               }
			}
			
		} catch (Exception e) {
			e.getMessage();
		}
		return listadoRemedy;
	}

	/**
	 * Lectura de archivo de Aplicativos Tecnicos.
	 * @param rutaArchivo Ruta y nombre del archivo de Infraestructura.
	 * @param aplicativosTecnicos Lista de Aplicativos Tecnicos.
	 * @param software Lista de Softwares.
	 * @param infraestructuraStatus Lista de Estatus de Infraestructura.
	 */
	public void leerArchivoCMDB(String rutaArchivo, List<AplicativoTecnico> aplicativosTecnicos, List<Software> software,
			List<InfraestructuraStatus> infraestructuraStatus){		
		
		try(
		    InputStream is = new FileInputStream(new File(rutaArchivo));
				Workbook wb = StreamingReader.builder()
				        .rowCacheSize(100)    // 10
				        .bufferSize(4096)     // 1024
				        .open(is);
			){

			
			Sheet tabApplicationData = wb.getSheetAt(4);
			Sheet tabServidoresLogicos = wb.getSheetAt(0);
			Sheet tabServidoresAmbientesPrevios = wb.getSheetAt(5);
			Sheet tabBasesDatos = wb.getSheetAt(1);
			Sheet tabDBPrevios = wb.getSheetAt(6);
			Sheet tabWas = wb.getSheetAt(2);
			Sheet tabWasPrevios = wb.getSheetAt(7);
			Sheet tabWeb = wb.getSheetAt(3);
			Sheet tabWebPrevios = wb.getSheetAt(8);
			
			// Application Data
			for(Row row : tabApplicationData){
               if(row.getRowNum()>0){            	  
                  SheetApplicationData sad = new SheetApplicationData();
                  sad.mapeaValores(row, aplicativosTecnicos);
                  sad.setFila(row.getRowNum()+1);                 
                  this.applicationData.add(sad);
               }
			}
			// Servidores Logicos Produccion
			for(Row row : tabServidoresLogicos){
               if(row.getRowNum()>0){
                  SheetServidores ssl = new SheetServidores();
                  ssl.mapeaValores(row, software);
                  ssl.setFila(row.getRowNum()+1);
                  this.servidoresLogicos.add(ssl);
               }
			}
			// Servidores Ambientes Previos
			for(Row row : tabServidoresAmbientesPrevios){
               if(row.getRowNum()>0){
                  SheetServidores sap = new SheetServidores();
                  sap.mapeaValores(row, software);
                  sap.setFila(row.getRowNum()+1);
                  this.servidoresAmbientesPrevios.add(sap);
               }
			}
			// Bases de Datos
			for(Row row : tabBasesDatos){
               if(row.getRowNum()>0){
                  SheetDB db = new SheetDB();
                  db.mapeaValores(row, software);
                  db.setFila(row.getRowNum()+1);
                  this.DB.add(db);
               }
			}
			// Bases de Datos Ambientes Previos
			for(Row row : tabDBPrevios){
               if(row.getRowNum()>0){
                  SheetDBP dbap = new SheetDBP();
                  dbap.mapeaValores(row, software);
                  dbap.setFila(row.getRowNum()+1);
                  this.DBAP.add(dbap);
               }
			}
			// WAS
			for(Row row : tabWas){
               if(row.getRowNum()>0){
                  Was was = new Was();
                  was.mapeaValores(row, software, infraestructuraStatus);
                  was.setFila(row.getRowNum()+1);
                  this.WAS.add(was);
               }
			}
			// WAS Previos
			for(Row row : tabWasPrevios){
               if(row.getRowNum()>0){
            	  Was wasp = new Was();
                  wasp.mapeaValoresPrevios(row, software, infraestructuraStatus);
                  wasp.setFila(row.getRowNum()+1);
                  this.WASP.add(wasp);
               }
			}
			// WEB
			for(Row row : tabWeb){
               if(row.getRowNum()>0){
                  Web web = new Web();
                  web.mapeaValores(row, software, infraestructuraStatus);
                  web.setFila(row.getRowNum()+1);
                  this.WEB.add(web);
               }
			}
			// WEB Previos
			for(Row row : tabWebPrevios){
               if(row.getRowNum()>0){
            	   Web webp = new Web();
                   webp.mapeaValores(row, software, infraestructuraStatus);
                   webp.setFila(row.getRowNum()+1);
                  this.WEBP.add(webp);
               }
			}
			
		} catch (Exception e) {
			e.getMessage();
		}
	}

	public List<SheetApplicationData> getApplicationData() {
		return applicationData;
	}

	public void setApplicationData(List<SheetApplicationData> applicationData) {
		this.applicationData = applicationData;
	}

	public List<SheetServidores> getServidoresLogicos() {
		return servidoresLogicos;
	}

	public void setServidoresLogicos(List<SheetServidores> servidoresLogicos) {
		this.servidoresLogicos = servidoresLogicos;
	}

	public List<SheetServidores> getServidoresAmbientesPrevios() {
		return servidoresAmbientesPrevios;
	}

	public void setServidoresAmbientesPrevios(List<SheetServidores> servidoresAmbientesPrevios) {
		this.servidoresAmbientesPrevios = servidoresAmbientesPrevios;
	}

	public List<SheetDB> getDB() {
		return DB;
	}

	public void setDB(List<SheetDB> dB) {
		DB = dB;
	}

	public List<SheetDBP> getDBAP() {
		return DBAP;
	}

	public void setDBAP(List<SheetDBP> dBAP) {
		DBAP = dBAP;
	}

	public List<Was> getWAS() {
		return WAS;
	}

	public void setWAS(List<Was> wAS) {
		WAS = wAS;
	}

	public List<Was> getWASP() {
		return WASP;
	}

	public void setWASP(List<Was> wASP) {
		WASP = wASP;
	}

	public List<Web> getWEB() {
		return WEB;
	}

	public void setWEB(List<Web> wEB) {
		WEB = wEB;
	}

	public List<Web> getWEBP() {
		return WEBP;
	}

	public void setWEBP(List<Web> wEBP) {
		WEBP = wEBP;
	}


}
