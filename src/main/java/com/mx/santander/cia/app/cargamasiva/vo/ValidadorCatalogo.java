package com.mx.santander.cia.app.cargamasiva.vo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.mx.santander.cia.app.cargamasiva.model.AplicativoFuncional;
import com.mx.santander.cia.app.cargamasiva.model.AplicativoTecnico;
import com.mx.santander.cia.app.cargamasiva.model.EntidadMapa;
import com.mx.santander.cia.app.cargamasiva.model.ErrorArchivo;
import com.mx.santander.cia.app.cargamasiva.model.EstructuraCatalogacion;
import com.mx.santander.cia.app.cargamasiva.model.StadiumBian;
import com.mx.santander.cia.app.cargamasiva.properties.Propiedades;
import com.mx.santander.cia.app.cargamasiva.model.SheetRemedy;


public class ValidadorCatalogo {
    
	private List<ErrorArchivo> afRechazados = new ArrayList<>();
	private List<ErrorArchivo> atRechazados = new ArrayList<>();
	private List<ErrorArchivo> arRechazados = new ArrayList<>();
	
	/**
	 * Validacion de catalogos combinados y errores de Aplicativos Funcionales, obtencion de alertas y/o errores ya existentes.
	 * @param listado Lista de Aplicativos Funcionales.
	 * @param entidadMapa Lista de Entidades-Mapas.
	 * @param estructuraCatalogacion Lista de Estructura Catalogacion.
	 * @param stadiumBian Lista de Stadium Bian.
	 * @return Lista de Aplicativos Funcionales con alertas y/o errores que apliquen.
	 */
	public List<AplicativoFuncional> validaCatalogosAplicativoFuncional(List<AplicativoFuncional> listado,List<EntidadMapa> entidadMapa,
			                                                            List<EstructuraCatalogacion> estructuraCatalogacion,
			                                                            List<StadiumBian> stadiumBian){
		List<AplicativoFuncional> AF = new ArrayList<>();
		boolean existeEM;
		boolean existeEC;
		boolean existeSB;
		int contador = 5;
		for(AplicativoFuncional af:listado){
			contador+=1;
			
			if(af.getCodigoFuncional() != null && af.getNombreFuncional()!=null && 
					af.getCodigoFuncional().matches("[0-9]*")){
				//Valida Entidad Mapa
				existeEM = false;
				if(af.getEntidad()!=null && af.getMapa()!=null){
				   for(EntidadMapa em:entidadMapa){
				      if(em.getEntidad().equals(af.getEntidad()) && em.getMapa().equals(af.getMapa())){
				    	  existeEM = true;
				    	  break;
				      }
				   }
				}
				//Valida Estructura Catalogacion
				existeEC = false;
				if(af.getCapa()!=null && af.getSistema()!=null && af.getSubsistema()!=null){
				   for(EstructuraCatalogacion ec:estructuraCatalogacion){
				      if(ec.getDominio().equals(af.getCapa()) && ec.getAreaFuncional().equals(af.getSistema()) && 
				    		  ec.getSubareaFuncional().equals(af.getSubsistema())){
				    	  existeEC = true;
				    	  break;
				      }
				   }
				}
				//Valida Stadium BIAN
				existeSB = false;
				if(af.getBnombre()!=null && af.getBcriticidad()!=null && af.getBnivel1()!=null && af.getBnivel2()!=null && af.getBnivel3()!=null){
				   for(StadiumBian sb:stadiumBian){
				      if(sb.getIdDominio().equals(af.getBnivel1()) && sb.getIdBianNivel2().equals(af.getBnivel2()) && sb.getIdBianNivel3().equals(af.getBnivel3())
				    		  && sb.getIdBianAfr().equals(af.getBnombre()) && sb.getIdBianCriticidad().equals(af.getBcriticidad())){
				    	  existeSB = true;
				    	  break;
				      }
				   }
				}
				
				if(!existeEM){
					af.setEntidad(null);
					af.setMapa(null);
					ErrorArchivo errorCombinacion = new ErrorArchivo();
					errorCombinacion.setFila(contador+"");
					errorCombinacion.setCampo("Combinación Entidad-Mapa");
					errorCombinacion.setIdentificador(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_ID_AF) + " " + af.getCodigoFuncional());
					errorCombinacion.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
					errorCombinacion.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO_COMBINACION));
					this.afRechazados.add(errorCombinacion);
				}
				if(!existeEC){
					af.setCapa(null);
					af.setSistema(null);
					af.setSubsistema(null);
					ErrorArchivo errorCombinacion = new ErrorArchivo();
					errorCombinacion.setFila(contador+"");
					errorCombinacion.setCampo("Combinación Estructura Catalogación");
					errorCombinacion.setIdentificador(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_ID_AF) + " " + af.getCodigoFuncional());
					errorCombinacion.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
					errorCombinacion.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO_COMBINACION));
					this.afRechazados.add(errorCombinacion);
				}
				if(!existeSB){
					af.setBnombre(null);
					af.setBcriticidad(null);
					af.setBnivel1(null);
					af.setBnivel2(null);
					af.setBnivel3(null);
					ErrorArchivo errorCombinacion = new ErrorArchivo();
					errorCombinacion.setFila(contador+"");
					errorCombinacion.setCampo("Combinación Stadium BIAN");
					errorCombinacion.setIdentificador(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_ID_AF) + " " + af.getCodigoFuncional());
					errorCombinacion.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
					errorCombinacion.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO_COMBINACION));
					this.afRechazados.add(errorCombinacion);
				}	
				for (ErrorArchivo error: af.getErroresMapeo()) {
					error.setFila(contador+"");
					error.setIdentificador(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_ID_AF) + " " + af.getCodigoFuncional());
					this.afRechazados.add(error);					
				}
			   AF.add(af);
			}else{
				
				if(af.getCodigoFuncional() == null) {
					ErrorArchivo errorObligatorio = new ErrorArchivo();
					errorObligatorio.setFila(contador+"");
					errorObligatorio.setIdentificador("N/A");
					errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
					errorObligatorio.setCampo("Código Funcional");
					errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ERROR));
					this.afRechazados.add(errorObligatorio);
					
				} else if (!af.getCodigoFuncional().matches("[0-9]*")) {
					ErrorArchivo errorOrbis = new ErrorArchivo();
					errorOrbis.setFila(contador+"");
					errorOrbis.setIdentificador("N/A");
					errorOrbis.setCampo("Código Funcional");
					errorOrbis.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
					errorOrbis.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VALIDO_CODIGO));
					this.afRechazados.add(errorOrbis);
				}
				if(af.getNombreFuncional() == null) {
					ErrorArchivo errorObligatorio = new ErrorArchivo();
					errorObligatorio.setFila(contador+"");
					errorObligatorio.setIdentificador("N/A");
					errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
					errorObligatorio.setCampo("Nombre Funcional");
					errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ERROR));
					this.afRechazados.add(errorObligatorio);
				}				
			}	
		}	
		return AF;
	}
	
	/**
	 * Validacion de errores de Aplicativos Tecnicos, obtencion de alertas y/o errores ya existentes.
	 * @param listado Lista de Aplicativos Tecnicos.
	 * @return Lista de Aplicativos Tecnicos con alertas y/o errores que apliquen.
	 */
	public List<AplicativoTecnico> validaCatalogosAplicativoTecnico(List<AplicativoTecnico> listado){
	   List<AplicativoTecnico> AT = new ArrayList<>();
	   int contador = 5;
	   for(AplicativoTecnico at:listado){
		   contador+=1;
		   
		   if(at.getCodigoOrbis() != null && at.getCodigoTecnico() != null && at.getCodigoAF() != null && 
				   at.getCodigoOrbis().matches("[0-9]*") && at.getCodigoTecnico().matches("[0-9]*") && 
				   at.getCodigoAF().matches("[0-9]*")){
			   at.setFila(contador);
				for (ErrorArchivo error: at.getErroresMapeo()) {
					error.setFila(contador+"");
					error.setIdentificador(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_ID_AT) + " " + at.getCodigoOrbis());
					this.atRechazados.add(error);
				}	
			   AT.add(at);
		   }else{
				if(at.getCodigoTecnico() == null) { 
					ErrorArchivo errorObligatorio = new ErrorArchivo();
					errorObligatorio.setFila(contador+"");
					errorObligatorio.setIdentificador("N/A");
					errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
					errorObligatorio.setCampo("Código Técnico");
					errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ERROR));
					this.atRechazados.add(errorObligatorio);
				} else if (!at.getCodigoTecnico().matches("[0-9]*")) {
					ErrorArchivo errorOrbis = new ErrorArchivo();
					errorOrbis.setFila(contador+"");
					errorOrbis.setIdentificador("N/A");
					errorOrbis.setCampo("Código Técnico");
					errorOrbis.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
					errorOrbis.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VALIDO_CODIGO));
					this.atRechazados.add(errorOrbis);
				}
				if(at.getCodigoOrbis() == null) { 
					ErrorArchivo errorObligatorio = new ErrorArchivo();
					errorObligatorio.setFila(contador+"");
					errorObligatorio.setIdentificador("N/A");
					errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
					errorObligatorio.setCampo("Código Orbis");
					errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ERROR));
					this.atRechazados.add(errorObligatorio);
				} else if (!at.getCodigoOrbis().matches("[0-9]*")) {
					ErrorArchivo errorOrbis = new ErrorArchivo();
					errorOrbis.setFila(contador+"");
					errorOrbis.setIdentificador("N/A");
					errorOrbis.setCampo("Código Orbis");
					errorOrbis.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
					errorOrbis.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VALIDO_CODIGO));
					this.atRechazados.add(errorOrbis);
				}
				if(at.getCodigoAF() == null) { 
					ErrorArchivo errorObligatorio = new ErrorArchivo();
					errorObligatorio.setFila(contador+"");
					errorObligatorio.setIdentificador("N/A");
					errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
					errorObligatorio.setCampo("Código Aplicación Funcional asociada");
					errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ERROR));
					this.atRechazados.add(errorObligatorio);
				} else if (!at.getCodigoAF().matches("[0-9]*")) {
					ErrorArchivo errorOrbis = new ErrorArchivo();
					errorOrbis.setFila(contador+"");
					errorOrbis.setIdentificador("N/A");
					errorOrbis.setCampo("Código Aplicación Funcional asociada");
					errorOrbis.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
					errorOrbis.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VALIDO_CODIGO));
					this.atRechazados.add(errorOrbis);
				}
				
			}		   
	   }
       return AT;
    }
	
	public Map<String,List<SheetRemedy>> validaAplicacionesRemedy(List<SheetRemedy> remedy) {
		Map<String,List<SheetRemedy>> apRem = new HashMap<>(); 
		List<SheetRemedy> listRem = new ArrayList<>();
		List<SheetRemedy> listRemedy = new ArrayList<>();
		String codigoOrbis = null;
		String llaveRemedy = "";
		
		int contador = 1;
		for(SheetRemedy rem:  remedy){
			contador+=1;
			if(rem.getIdOrbis() != null && rem.getAplicacionRemedy() != null && 
					!rem.getIdOrbis().equals("") && !rem.getAplicacionRemedy().equals("") &&
					rem.getIdOrbis().matches("[0-9]*")){		
				for (ErrorArchivo error: rem.getErroresMapeo()) {
					error.setFila(contador+"");
					error.setIdentificador(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_ID_AT) + " " + rem.getIdOrbis());					
					this.arRechazados.add(error);
				}
				rem.setFila(contador);
                listRem.add(rem);
			}else {						
				if(rem.getIdOrbis() == null || rem.getIdOrbis().equals("")) { 
					ErrorArchivo errorObligatorio = new ErrorArchivo();
					errorObligatorio.setFila(contador+"");
					errorObligatorio.setIdentificador("N/A");
					errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
					errorObligatorio.setCampo("Código Orbis");
					errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ERROR));
					this.arRechazados.add(errorObligatorio);
				} else if (!rem.getIdOrbis().matches("[0-9]*")) {
					ErrorArchivo errorOrbis = new ErrorArchivo();
					errorOrbis.setFila(contador+"");
					errorOrbis.setIdentificador("N/A");
					errorOrbis.setCampo("Código Orbis");
					errorOrbis.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
					errorOrbis.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VALIDO_CODIGO));
					this.arRechazados.add(errorOrbis);
				}
				if(rem.getAplicacionRemedy() == null || rem.getAplicacionRemedy().equals("")) {
					ErrorArchivo errorObligatorio = new ErrorArchivo();
					errorObligatorio.setFila(contador+"");
					errorObligatorio.setIdentificador("N/A");
					errorObligatorio.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
					errorObligatorio.setCampo("Aplicación Remedy");
					errorObligatorio.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ERROR));
					this.arRechazados.add(errorObligatorio);
				}
				
			}
		}

		Collections.sort(listRem);
    	for(SheetRemedy rem:  listRem){
			String llaveLi = rem.getIdOrbis()+rem.getAplicacionRemedy();
			if(!llaveLi.equals(llaveRemedy)){
				llaveRemedy = llaveLi;
				listRemedy.add(rem);
			}
		}
		
		
		for(SheetRemedy rem:  listRemedy){
	       //String[] orbisArr = rem.getIdOrbis().split("_");
		   //String orbis = orbisArr[1];
		   String orbis = rem.getIdOrbis();
		   if(orbis!=codigoOrbis){
		      codigoOrbis = orbis;
			  List<SheetRemedy> lar = new ArrayList<>();
			  lar.add(rem);
			  apRem.put(orbis, lar);
		   }else {
		      apRem.get(orbis).add(rem);
		   }
		}

		
		return apRem;
	}


	public List<ErrorArchivo> getAfRechazados() {
		return afRechazados;
	}


	public void setAfRechazados(List<ErrorArchivo> afRechazados) {
		this.afRechazados = afRechazados;
	}


	public List<ErrorArchivo> getAtRechazados() {
		return atRechazados;
	}


	public void setAtRechazados(List<ErrorArchivo> atRechazados) {
		this.atRechazados = atRechazados;
	}


	public List<ErrorArchivo> getArRechazados() {
		return arRechazados;
	}


	public void setArRechazados(List<ErrorArchivo> arRechazados) {
		this.arRechazados = arRechazados;
	}
	
}
