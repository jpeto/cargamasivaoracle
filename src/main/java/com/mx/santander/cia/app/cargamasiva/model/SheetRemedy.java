package com.mx.santander.cia.app.cargamasiva.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.mx.santander.cia.app.cargamasiva.properties.Propiedades;

public class SheetRemedy implements Comparable<SheetRemedy> {

	/** Nombre de la Aplicacion Remedy */
	private String aplicacionRemedy;
	/** ID Orbis */
	private String idOrbis;
	/** Numero Fila*/
    private int fila;
    /** Lista de Errores**/
    private List<ErrorArchivo> erroresMapeo;
   
    /**
	 * Mapeo de una fila del excel de Aplicativos Remedy y validacion por campo.
	 * @param row Fila de Aplicativo Remedy.
	 */
	public void mapeaValores(Row row){
		this.erroresMapeo = new ArrayList<>();
		this.aplicacionRemedy = valida(row.getCell(1),250);              
		this.idOrbis = validaCodigoOrbis(row.getCell(10),10);
	}
	
	/**
	 * Valida valor de una celda de excel: obligatoriedad, campo vacio y longitud de campo, generando las alertas correspondientes.
	 * @param c Celda de excel a validar.
	 * @param campo Nombre del campo de Application Data a validar.
	 * @param longitud Longitud del campo de Aplicativo Tecnico a validar.
	 * @return Valor de celda de excel.
	 */
    private String valida(Cell c, int longitud) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") 
    				&& !c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().equals("")){
    			if(c.getStringCellValue().length()>longitud) {
    				String mensaje = Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_LONGITUD);
    				mensaje = mensaje.replaceAll("<LONG>", longitud+"");
    				error.setCampo("Aplicación Remedy");
    		    	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
                	error.setMensaje(mensaje);
    		    	this.erroresMapeo.add(error);
    		    	return c.getStringCellValue().substring(0, longitud);
    			}else {
    				return c.getStringCellValue();
    			}
    		}
    		else{
    			error.setCampo("Aplicacion Remedy");
            	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
            	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));
    		}
    		
    	}else{
    		error.setCampo("Aplicación Remedy");
        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));
    	}
    	this.erroresMapeo.add(error);
		return null;
    }
    
    /**
	 * Valida campo Codigo Orbis de una celda de excel: campo vacio, estructura numerica y longitud de campo, generando las alertas y errores correspondientes.
	 * @param c Celda de excel.
	 * @param longitud Longitud del campo de Aplicativo Remedy a validar.
	 * @return Valor de celda de excel.
	 */
    private String validaCodigoOrbis(Cell c, int longitud) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") 
    				&& !c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().equals("")){
    			String corb = null;
			    if(c.getStringCellValue().contains("_")){
				   String[] div = c.getStringCellValue().trim().split("_");
				   corb = div[1];
				}else {
				   corb = c.getStringCellValue();
				}
    			if (corb.matches("[0-9]*")) {
    				if(corb.length()>longitud) {
        				String mensaje = Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_LONGITUD);
        				mensaje = mensaje.replaceAll("<LONG>", longitud+"");
        				error.setCampo("Código Orbis");
        		    	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
        		    	error.setMensaje(mensaje);
        				this.erroresMapeo.add(error);
        				return corb.substring(0, longitud);
        			}else {
        				return corb;
        			}
    		    } else {
    		    	error.setCampo("Código Orbis");
    	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
    	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VALIDO_CODIGO));
    	        	this.erroresMapeo.add(error);
    	    		return corb;
    		    }
    		}else{
    			error.setCampo("Código Orbis");
            	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
            	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));
    		}
    	}else{
    		error.setCampo("Código Orbis");
        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));
    	}
    	this.erroresMapeo.add(error);
		return null;
    }
    
    /**
	 * Compara 2 Aplicativos Remedy, para determinar si son iguales por Codigo Orbis y Nombre Remedy.
	 * @param o Aplicativo Remedy.
	 * @return Entero indicando si los Aplicativos Remedy son o no iguales.
	 */
    @Override
    public int compareTo(SheetRemedy o) {
    	String a = new String(this.idOrbis+this.aplicacionRemedy);
        String b = new String(o.idOrbis+o.aplicacionRemedy);
        return a.compareTo(b);
    }

	public String getAplicacionRemedy() {
		return aplicacionRemedy;
	}

	public void setAplicacionRemedy(String aplicacionRemedy) {
		this.aplicacionRemedy = aplicacionRemedy;
	}

	public String getIdOrbis() {
		return idOrbis;
	}

	public void setIdOrbis(String idOrbis) {
		this.idOrbis = idOrbis;
	}

	public int getFila() {
		return fila;
	}

	public void setFila(int fila) {
		this.fila = fila;
	}
	public List<ErrorArchivo> getErroresMapeo() {
		return erroresMapeo;
	}

	public void setErroresMapeo(List<ErrorArchivo> erroresMapeo) {
		this.erroresMapeo = erroresMapeo;
	}
}
