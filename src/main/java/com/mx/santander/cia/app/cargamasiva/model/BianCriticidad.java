package com.mx.santander.cia.app.cargamasiva.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BianCriticidad {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long _id;
    private String bianCriticidad;
    private int estatus;

    public Long get_id() {
        return _id;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }

    public String getBianCriticidad() {
        return bianCriticidad;
    }

    public void setBianCriticidad(String bianCriticidad) {
        this.bianCriticidad = bianCriticidad;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }
}
