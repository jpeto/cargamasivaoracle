package com.mx.santander.cia.app.cargamasiva.model;

import org.springframework.data.annotation.PersistenceConstructor;

public class Servidor {

	private String _id;
	private String idAplicativoTecnico;
	private String compania;
	private String nombreServidor;
	private String ambienteServidor;
	private Long estatusServidor;
	private String funcionServidor;
	private String plataforma;
	private String so;
	private String eolServidor;
	private String nombreDB;
	private String ambienteDB;
	private Long estatusDB;
	private String manejadorDB;
	private String versionDB;
	private String eolDB;
	private String scid;
	private int fila;
	private boolean existeEnArchivo;
	
	@PersistenceConstructor
	public Servidor(){
		//this._id = get_id();
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getCompania() {
		return compania;
	}

	public void setCompania(String compania) {
		this.compania = compania;
	}

	public String getNombreServidor() {
		return nombreServidor;
	}

	public void setNombreServidor(String nombreServidor) {
		this.nombreServidor = nombreServidor;
	}

	public String getAmbienteServidor() {
		return ambienteServidor;
	}

	public void setAmbienteServidor(String ambienteServidor) {
		this.ambienteServidor = ambienteServidor;
	}

	public Long getEstatusServidor() {
		return estatusServidor;
	}

	public void setEstatusServidor(Long estatusServidor) {
		this.estatusServidor = estatusServidor;
	}

	public String getFuncionServidor() {
		return funcionServidor;
	}

	public void setFuncionServidor(String funcionServidor) {
		this.funcionServidor = funcionServidor;
	}

	public String getPlataforma() {
		return plataforma;
	}

	public void setPlataforma(String plataforma) {
		this.plataforma = plataforma;
	}

	public String getSo() {
		return so;
	}

	public void setSo(String so) {
		this.so = so;
	}

	public String getEolServidor() {
		return eolServidor;
	}

	public void setEolServidor(String eolServidor) {
		this.eolServidor = eolServidor;
	}

	public String getNombreDB() {
		return nombreDB;
	}

	public void setNombreDB(String nombreDB) {
		this.nombreDB = nombreDB;
	}

	public String getAmbienteDB() {
		return ambienteDB;
	}

	public void setAmbienteDB(String ambienteDB) {
		this.ambienteDB = ambienteDB;
	}

	public Long getEstatusDB() {
		return estatusDB;
	}

	public void setEstatusDB(Long estatusDB) {
		this.estatusDB = estatusDB;
	}

	public String getManejadorDB() {
		return manejadorDB;
	}

	public void setManejadorDB(String manejadorDB) {
		this.manejadorDB = manejadorDB;
	}

	public String getVersionDB() {
		return versionDB;
	}

	public void setVersionDB(String versionDB) {
		this.versionDB = versionDB;
	}

	public String getEolDB() {
		return eolDB;
	}

	public void setEolDB(String eolDB) {
		this.eolDB = eolDB;
	}

	public String getScid() {
		return scid;
	}

	public void setScid(String scid) {
		this.scid = scid;
	}

	public String getIdAplicativoTecnico() {
		return idAplicativoTecnico;
	}

	public void setIdAplicativoTecnico(String idAplicativoTecnico) {
		this.idAplicativoTecnico = idAplicativoTecnico;
	}

	public int getFila() {
		return fila;
	}

	public void setFila(int fila) {
		this.fila = fila;
	}

	public boolean isExisteEnArchivo() {
		return existeEnArchivo;
	}

	public void setExisteEnArchivo(boolean existeEnArchivo) {
		this.existeEnArchivo = existeEnArchivo;
	}	
}