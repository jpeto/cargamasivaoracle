package com.mx.santander.cia.app.cargamasiva.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
public class IdentificadorCpd {
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Long _id;
	private String cpd;
	private int estatus;
	
	public String getCpd() {
		return cpd;
	}
	public void setCpd(String cpd) {
		this.cpd = cpd;
	}
	public int getEstatus() {
		return estatus;
	}
	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}
	public Long get_id() {
		return _id;
	}
	public void set_id(Long _id) {
		this._id = _id;
	}
	
}
