package com.mx.santander.cia.app.cargamasiva.db;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.mx.santander.cia.app.cargamasiva.list.Response;
import com.mx.santander.cia.app.cargamasiva.model.AplicativoFuncional;
import com.mx.santander.cia.app.cargamasiva.model.AplicativoFuncionalBan;
import com.mx.santander.cia.app.cargamasiva.model.AplicativoRemedyXAT;
import com.mx.santander.cia.app.cargamasiva.model.AplicativoTecnico;
import com.mx.santander.cia.app.cargamasiva.model.AplicativoTecnicoBan;
import com.mx.santander.cia.app.cargamasiva.model.ATXAF;
import com.mx.santander.cia.app.cargamasiva.model.Documento;
import com.mx.santander.cia.app.cargamasiva.model.Servidor;
import com.mx.santander.cia.app.cargamasiva.model.StadiumBianXAf;
import com.mx.santander.cia.app.cargamasiva.model.Was;
import com.mx.santander.cia.app.cargamasiva.model.Web;
import com.mx.santander.cia.app.cargamasiva.model.Responsables;



@FeignClient("cia-database")
public interface DatabaseConn {
	
	/** Manda a llamar el servicio de creación de responsables.
	 * @param res Responsable a crear.
	 * @return Responsable responsable creado.
	 */
    @PostMapping(value = "/responsablesxaplicativofuncional",
    		consumes="application/json")
    public Responsables creaResponsables(Responsables res);
	
    /** Manda a llamar el servicio de borrar responsables por Id de Aplicativo Funcional.
	 * @param idAplicativo Id de Aplicativo Funcional.
	 * @return Entero indicando si el borrado del responsable tuvo exito.
	 */	 
    @GetMapping(value = "/responsablesxaplicativofuncional/search/deleteByIdAplicativoFuncional?idAplicativo={idAplicativo}", 
    		consumes="application/json")
    public Object borraResponsablesXAfXIdAf(
    		@PathVariable(value="idAplicativo", required=true) String idAplicativo);
	
    /** Manda a llamar el servicio de obtener responsables por id de Aplicativo Funcional.
	 * @param idAplicativo Id de Aplicativo Funcional.
	 * @return Responsables asociados al id de Aplicativo Funcional enviado.
	 */	
    @GetMapping(value = "/responsablesxaplicativofuncional/search/findResponsalbesByIdAplicativo?idAplicativo={idAplicativo}", 
    		consumes="application/json")
    public Response obtieneResponsablesXAfXIdAf(
    		@PathVariable(value="idAplicativo", required=true) String idAplicativo);
	
    /** Manda a llamar el servicio de consulta de Catalogo de Estatus de Infraestructura.
	 * @param N/A.
	 * @return Catalogo de Estatus de Infraestructura.
	 */	
	@GetMapping(value="/infraestructurastatus/search/consultaListadoCatInfraestructuraStatus",
			consumes="application/json")
	Response consultaListadoCatInfraestructuraStatus();
	
    /** Manda a llamar el servicio de consulta de Catalogo de Estatus de Infraestructura por estatus.
	 * @param estatus Estatus a buscar.
	 * @return Estatus de Infraestructura asociados al estatus enviado.
	 */	
	@GetMapping(value="/infraestructurastatus/search/findInfraestructuraStatusByEstatus?estatus={estatus}",
			consumes="application/json")
	Response consultaListadoCatInfraestructuraStatus(@PathVariable(value="estatus", required=true) String estatus);
	
    /** Manda a llamar el servicio de consulta de Catalogo de Mapa.
	 * @param N/A.
	 * @return Catalogo de Mapa.
	 */	
	@GetMapping(value="/mapa/search/consultaListadoCatMapa",
			consumes="application/json")
	Response consultaListadoMapa();
	
	/** Manda a llamar el servicio de consulta de Catalogo de Entidad.
	 * @param N/A.
	 * @return Catalogo de Entidad.
	 */	
	@GetMapping(value="/entidad/search/consultaListadoCatEntidad",
			consumes="application/json")
	Response consultaListadoEntidad();
	
	/** Manda a llamar el servicio de consulta de Catalogo de Entidad-Mapa.
	 * @param N/A.
	 * @return Catalogo de Entidad-Mapa.
	 */	
	@GetMapping(value="/entidadmapa/search/consultaListadoCatEntidadMapa",
			consumes="application/json")
	Response consultaEntidadMapa();
	
	/** Manda a llamar el servicio de consulta de Catalogo de Capa.
	 * @param N/A.
	 * @return Catalogo de Capa.
	 */	
	@GetMapping(value="/capa/search/consultaListadoCatCapaRepository",
			consumes="application/json")
	Response consultaListadoCatCapa();
	
	/** Manda a llamar el servicio de consulta de Catalogo de Sistema.
	 * @param N/A.
	 * @return Catalogo de Sistema.
	 */	
	@GetMapping(value="/sistema/search/consultaListadoCatSistemaRepository",
			consumes="application/json")
	Response consultaListadoCatSistema();
	
	/** Manda a llamar el servicio de consulta de Catalogo de Subsistema.
	 * @param N/A.
	 * @return Catalogo de Subsistema.
	 */	
	@GetMapping(value="/subsistema/search/consultaListadoCatSubsistemaRepository",
			consumes="application/json")
	Response consultaListadoCatSubsistema();
	
	/** Manda a llamar el servicio de consulta de Catalogo de Estructura Catalogacion.
	 * @param N/A.
	 * @return Catalogo de Estructura Catalogacion.
	 */	
	@RequestMapping(method= RequestMethod.GET,
			value="/estructuracatalogacion/search/consultaListadoCatEstructuraCatalogacion",
			consumes="application/json")
	Response consultaEstructuraCatalogacion();
	
	/** Manda a llamar el servicio de consulta de Catalogo de Categoria Aplicativo.
	 * @param N/A.
	 * @return Catalogo de Categoria Aplicativo.
	 */		
	@RequestMapping(method= RequestMethod.GET,
			value="/categoriaaplicativo/search/consultaListadoCatCategoriaAplicativo",
			consumes="application/json")
	Response consultaCategoriaAplicativo();
	
	/** Manda a llamar el servicio de consulta de Catalogo de Dominio.
	 * @param N/A.
	 * @return Catalogo de Dominio.
	 */	
    @GetMapping(value="/dominio/search/consultaListadoCatDominio",
            consumes="application/json")
    Response consultaListadoDominio();
	
    /** Manda a llamar el servicio de consulta de Catalogo de Bian Nivel 2.
	 * @param N/A.
	 * @return Catalogo de Bian Nivel 2.
	 */	
    @GetMapping(value="/bian_nivel2/search/consultaListadoCatBianNivel2",
            consumes="application/json")
    Response consultaListadoCatBianNivel2();
	
    /** Manda a llamar el servicio de consulta de Catalogo de Bian Nivel 3.
	 * @param N/A.
	 * @return Catalogo de Bian Nivel 3.
	 */	
    @GetMapping(value="/bian_nivel3/search/consultaListadoCatBianNivel3",
            consumes="application/json")
    Response consultaListadoCatBianNivel3();
	
    /** Manda a llamar el servicio de consulta de Catalogo de Bian Afr.
	 * @param N/A.
	 * @return Catalogo de Bian Afr.
	 */	
    @GetMapping(value="/bian_afr/search/consultaListadoCatBianAfr",
            consumes="application/json")
    Response consultaListadoCatBianAfr();
	
    /** Manda a llamar el servicio de consulta de Catalogo de Bian Criticidad.
	 * @param N/A.
	 * @return Catalogo de Bian Criticidad.
	 */	
    @GetMapping(value="/bian_criticidad/search/consultaListadoCatBianCriticidad",
            consumes="application/json")
    Response consultaListadoCatBianCriticidad();
	
    /** Manda a llamar el servicio de consulta de Catalogo de Stadium Bian.
	 * @param N/A.
	 * @return Catalogo de Stadium Bian.
	 */	
	@RequestMapping(method= RequestMethod.GET,
			value="/stadiumbian/search/consultaListadoCatStadiumBian",
			consumes="application/json")
	Response consultaStadiumbian();
	
	/** Manda a llamar el servicio de consulta de Catalogo de Paquete Bian.
	 * @param N/A.
	 * @return Catalogo de Paquete Bian.
	 */		
	@RequestMapping(method= RequestMethod.GET,
			value="/paquetebian/search/consultaListadoCatPaqueteBian",
			consumes="application/json")
	Response consultaPaqueteBian();
	
	/** Manda a llamar el servicio de consulta de Catalogo de Granuralidad Bian.
	 * @param N/A.
	 * @return Catalogo de Granularidad Bian.
	 */	
	@RequestMapping(method= RequestMethod.GET,
			value="/granularidadbian/search/consultaListadoCatGranularidadBian",
			consumes="application/json")
	Response consultaGranularidadBian();
	
	/** Manda a llamar el servicio de consulta de Catalogo de Tipologia de Software.
	 * @param N/A.
	 * @return Catalogo de Tipologia de Software.
	 */	
	@RequestMapping(method= RequestMethod.GET,
			value="/tipologiasoftware/search/consultaListadoCatTipologiaSoftware",
			consumes="application/json")
	Response consultaTipologiaSoftware();
	
	/** Manda a llamar el servicio de consulta de Catalogo de Identificador Cpd.
	 * @param N/A.
	 * @return Catalogo de Identificador Cpd.
	 */	
	@RequestMapping(method= RequestMethod.GET,
			value="/identificadorcpd/search/consultaListadoCatIdentificadorCpd",
			consumes="application/json")
	Response consultaIdentificadorCpd();
	
	/** Manda a llamar el servicio de consulta de Catalogo de Entidad Propipetaria.
	 * @param N/A.
	 * @return Catalogo de Entidad Propietaria.
	 */	
    @RequestMapping(method= RequestMethod.GET,
            value="/entidadpropietaria/search/consultaListadoCatEntidadPropietaria",
            consumes="application/json")
    Response consultaEntidadPropietaria();
	
    /** Manda a llamar el servicio de consulta de Catalogo de Criticidad de Servicio.
	 * @param N/A.
	 * @return Catalogo de Criticidad de Servicio.
	 */	
	@RequestMapping(method= RequestMethod.GET,
			value="/criticidadservicio/search/consultaListadoCatCriticidadServicio",
			consumes="application/json")
	Response consultaCriticidadServicio();
	
	/** Manda a llamar el servicio de consulta de Catalogo de Software.
	 * @param N/A.
	 * @return Catalogo de Software.
	 */	
    @RequestMapping(method= RequestMethod.GET,
  	        value="/software/search/consultaListadoCatSoftwareCM",
  	        consumes="application/json")
      Response consultaSoftware();
	
    /** Manda a llamar el servicio de consulta de Aplicativos Funcionales.
	 * @param N/A.
	 * @return Lista de Aplicativos Funcionales.
	 */	
	@RequestMapping(method= RequestMethod.GET,
			value="/aplicativofuncional/search/consultaListadoAplicativoFuncional",
			consumes="application/json")
	Response consultaAplicativoFuncional();
	
    /** Manda a llamar el servicio de actualizar un Aplicativo Funcional.
	 * @param idaplicativofuncional Id de Aplicativo Funcional.
	 * @return Aplicativo Funcional actualizado.
	 */	
    @RequestMapping(method = RequestMethod.PUT,
    		value = "/aplicativofuncional/{idaplicativofuncional}",
    		consumes="application/json")
    public Object actualizaAplicativoFuncional(AplicativoFuncional aplicativofuncional,
    		@PathVariable(value="idaplicativofuncional", required=true) String idaplicativofuncional);
	
    /** Manda a llamar el servicio de agregar un Aplicativo Funcional.
	 * @param aplicativofuncional Aplicativo Funcional a agregar.
	 * @return Aplicativo Funcional agregado.
	 */	
    @RequestMapping(method = RequestMethod.POST,
    		value = "/aplicativofuncional",
    		consumes="application/json")
    public Object agregaAplicativoFuncional(AplicativoFuncional aplicativofuncional);
	
    /** Manda a llamar el servicio de borrar un Aplicativo Funcional Ban.
	 * @param idAplicativoFuncionalBan Id de Aplicativo Funcional Ban.
	 * @return Entero indicando si el borrado del Aplicativo Funcional Ban tuvo exito.
	 */	
    @DeleteMapping(value = "/aplicativofuncionalban/{idAplicativoFuncionalBan}",
    		consumes="application/json")
    public Object borraAplicativoFuncionalBan(@PathVariable(value="idAplicativoFuncionalBan", required=true) String idAplicativoFuncionalBan);
	
    /** Manda a llamar el servicio de borrar un Aplicativo Funcional.
	 * @param idAplicativoFuncional Id de Aplicativo Funcional.
	 * @return Entero indicando si el borrado del Aplicativo Funcional tuvo exito.
	 */
    @DeleteMapping(value = "/aplicativofuncional/{idAplicativoFuncional}",
    		consumes="application/json")
    public Object borraAplicativoFuncional(@PathVariable(value="idAplicativoFuncional", required=true) String idAplicativoFuncional);
	
    /** Manda a llamar el servicio de consulta de Aplicativos Tecnicos.
	 * @param N/A.
	 * @return Lista de Aplicativos Tecnicos.
	 */	
	@RequestMapping(method= RequestMethod.GET,
			value="/aplicativotecnico/search/consultaListadoAplicativoTecnico",
			consumes="application/json")
	Response consultaAplicativoTecnico();
	
	/** Manda a llamar el servicio de actualizar un Aplicativo Tecnico.
	 * @param idaplicativofuncional Id de Aplicativo Tecnico.
	 * @return Aplicativo Tecnico actualizado.
	 */	
    @RequestMapping(method = RequestMethod.PUT,
    		value = "/aplicativotecnico/{idaplicativotecnico}",
    		consumes="application/json")
    public Object actualizaAplicativoTecnico(AplicativoTecnico aplicativotecnico,
    		@PathVariable(value="idaplicativotecnico", required=true) String idaplicativotecnico);
	
    /** Manda a llamar el servicio de agregar un Aplicativo Tecnico.
	 * @param aplicativotecnico Aplicativo Tecnico a agregar.
	 * @return Aplicativo Tecnico agregado.
	 */	
    @RequestMapping(method = RequestMethod.POST,
    		value = "/aplicativotecnico",
    		consumes="application/json")
    public Object agregaAplicativoTecnico(AplicativoTecnico aplicativotecnico);
	
    /** Manda a llamar el servicio de consulta de Aplicativo Funcional por Codigo Funcional.
	 * @param codigoFuncional Codigo Funcional del Aplicativo Funcional.
	 * @return Aplicativo Funcional asociado al Codigo Funcional enviado.
	 */	
	@RequestMapping(method= RequestMethod.GET,
			value="/aplicativofuncional/search/findAplicativoByCodigoFuncional?codigoFuncional={codigoFuncional}",
			consumes="application/json")
	Response consultaAFXCodigoFuncional(@PathVariable(value="codigoFuncional", required=true) String codigoFuncional);
	
	/** Manda a llamar el servicio de consulta de Aplicativo Tecnico por Codigo Orbis.
	 * @param codigoOrbis Codigo Orbis del Aplicativo Tecnico.
	 * @return Aplicativo Tecnico asociado al Codigo Orbis enviado.
	 */	
	@RequestMapping(method = RequestMethod.GET,
    		value = "/aplicativotecnico/search/findAplicativoByCodigoOrbis?codigoOrbis={codigoOrbis}",
    		consumes="application/json")
    public Response consultaATXCodigoOrbis(@PathVariable(value="codigoOrbis", required=true) String codigoOrbis);
	
	/** Manda a llamar el servicio de consulta de ATxAF por Id de Aplicativo Funcional.
	 * @param idAplicativoFuncional Id de Aplicativo Funcional.
	 * @return ATxAF asociado al Id de Aplicativo Funcional enviado.
	 */	
	@GetMapping(value="atxaf/search/findByIdAplicativoFuncional?idAplicativoFuncional={idAplicativoFuncional}",
			consumes="application/json")
	Response consultaATxAF(@PathVariable(value="idAplicativoFuncional", required=true) String idAplicativoFuncional);
	
	/** Manda a llamar el servicio de consulta de ATxAF por Id de Aplicativo Tecnico.
	 * @param idAplicativoTecnico Id de Aplicativo Tecnico.
	 * @return ATxAF asociado al Id de Aplicativo Tecnico enviado.
	 */
	@GetMapping(value="atxaf/search/findByIdAplicativoTecnico?idAplicativoTecnico={idAplicativoTecnico}",
			consumes="application/json")
	Response consultaATxAFxAplicativoTecnico(@PathVariable(value="idAplicativoTecnico", required=true) String idAplicativoTecnico);
	
	/** Manda a llamar el servicio de borrar un ATxAF.
	 * @param idAplicativoFuncional Id de Aplicativo Funcional.
	 * @param idAplicativoTecnico Id de Aplicativo Tecnico.
	 * @return Entero indicando si el borrado del ATxAF tuvo exito.
	 */
	@GetMapping(value = "/atxaf/search/deleteByIdAplicativoFuncionalAndIdAplicativoTecnico?idAplicativoFuncional={idAplicativoFuncional}&idAplicativoTecnico={idAplicativoTecnico}", consumes="application/json")
    public int borraAtXAf(@PathVariable(value="idAplicativoFuncional", required=true) String idAplicativoFuncional,
    		@PathVariable(value="idAplicativoTecnico", required=true) String idAplicativoTecnico);
	
	/** Manda a llamar el servicio de agregar un ATxAF.
	 * @param aplicativotecnicoxaf ATxAF a agregar.
	 * @return ATxAF agregado.
	 */	
    @RequestMapping(method = RequestMethod.POST,
            value = "/atxaf",
    		consumes="application/json")
    public Object agregaAtXAf(ATXAF aplicativotecnicoxaf);    
	
    /** Manda a llamar el servicio de agregar un AplicativoRemedyxAT.
	 * @param aplicativoremedyxat AplicativoRemedyXAT a agregar.
	 * @return AplicativoRemedyXAT agregado.
	 */	
    @RequestMapping(method = RequestMethod.POST,
            value = "/aplicativoremedyxat",
    		consumes="application/json")
    public Object agregaRemedyXAT(AplicativoRemedyXAT aplicativoremedyxat);
	
    /** Manda a llamar el servicio de consulta de AplicativoRemedyXAT por Id de Aplicativo Tecnico.
	 * @param idAplicativoTecnico Id de Aplicativo Tecnico.
	 * @return AplicativoRemedyXAT asociado al Id de Aplicativo Tecnico enviado.
	 */
    @GetMapping(value="/aplicativoremedyxat/search/consultaRemedyXAT?idAplicativoTecnico={idAplicativoTecnico}",
			consumes="application/json")
	Response consultaRemedyXAT(@PathVariable(value="idAplicativoTecnico", required=true) String idAplicativoTecnico);
	
    /** Manda a llamar el servicio de borrar un AplicativoRemedyXAT.
	 * @param idRemedyxat Id de AplicativoRemedyXAT.
	 * @return Entero indicando si el borrado del AplicativoRemedyXAT tuvo exito.
	 */
    @DeleteMapping(value = "/aplicativoremedyxat/{idRemedyxat}", consumes="application/json")
    public Object borraRemedyXAt(@PathVariable(value="idRemedyxat", required=true) String idRemedyxat);
	
    /** Manda a llamar el servicio de agregar un Servidor.
	 * @param servidor Servidor a agregar.
	 * @return Servidor agregado.
	 */	
    @RequestMapping(method = RequestMethod.POST,
    		value = "/servidor",
    		consumes="application/json")
    public Object agregaServidor(Servidor servidor);
	
    /** Manda a llamar el servicio de actualizar un Servidor.
	 * @param idServidor Id de Servidor.
	 * @return Servidor actualizado.
	 */	
    @RequestMapping(method = RequestMethod.PUT,
    		value = "/servidor/{idServidor}",
    		consumes="application/json")
    public Object actualizaServidor(Servidor servidor,
    		@PathVariable(value="idServidor", required=true) String idServidor);
	
    /** Manda a llamar el servicio de consulta de Servidores.
	 * @param N/A.
	 * @return Listado de Servidores.
	 */	
	@RequestMapping(method= RequestMethod.GET,
			value="/servidor/search/consultaListadoServidor",
			consumes="application/json")
	Response consultaServidor();
	
	/** Manda a llamar el servicio de borrar Servidor por Id de Servidor.
	 * @param idServidor Id de Servidor.
	 * @return Entero indicando si el borrado del Servidor tuvo exito.
	 */	 
    @RequestMapping(method = RequestMethod.DELETE,
    		value = "/servidor/{idServidor}",
    		consumes="application/json")
    public Object borraServidor(@PathVariable(value="idServidor", required=true) String idServidor);
	
    /** Manda a llamar el servicio de agregar un Was.
	 * @param was Was a agregar.
	 * @return Was agregado.
	 */	
    @RequestMapping(method = RequestMethod.POST,
	        value = "/was",
	        consumes="application/json")
    public Object agregaWas(Was was);
	
    /** Manda a llamar el servicio de actualizar un Was.
	 * @param idwas Id de Was.
	 * @return Was actualizado.
	 */
    @RequestMapping(method = RequestMethod.PUT,
	       value = "/was/{idwas}",
	       consumes="application/json")
    public Object actualizaWas(Was was,
	        @PathVariable(value="idwas", required=true) String idwas);
	
    /** Manda a llamar el servicio de consulta de Was.
	 * @param N/A.
	 * @return Listado de Was.
	 */	
    @RequestMapping(method= RequestMethod.GET,
	       value="/was/search/findWas",
	       consumes="application/json")
    Response consultaWas();
	
    /** Manda a llamar el servicio de borrar Was por Id de Was.
	 * @param idwas Id de Was.
	 * @return Entero indicando si el borrado del Was tuvo exito.
	 */	 
    @RequestMapping(method = RequestMethod.DELETE,
    		value = "/was/{idwas}",
    		consumes="application/json")
    public Object borraWas(@PathVariable(value="idwas", required=true) String idwas);
	
    /** Manda a llamar el servicio de agregar un Web.
	 * @param web Web a agregar.
	 * @return Web agregado.
	 */	
    @RequestMapping(method = RequestMethod.POST,
	        value = "/web",
	        consumes="application/json")
    public Object agregaWeb(Web web);
	
    /** Manda a llamar el servicio de actualizar un Web.
	 * @param idweb Id de Web.
	 * @return Web actualizado.
	 */
    @RequestMapping(method = RequestMethod.PUT,
	       value = "/web/{idweb}",
	       consumes="application/json")
    public Object actualizaWeb(Web web,
	        @PathVariable(value="idweb", required=true) String idweb);
	
    /** Manda a llamar el servicio de consulta de Web.
	 * @param N/A.
	 * @return Listado de Web.
	 */
    @RequestMapping(method= RequestMethod.GET,
	       value="/web/search/findWeb",
	       consumes="application/json")
    Response consultaWeb();
	
    /** Manda a llamar el servicio de borrar Web por Id de Web.
	 * @param idweb Id de Web.
	 * @return Entero indicando si el borrado del Web tuvo exito.
	 */	 
    @RequestMapping(method = RequestMethod.DELETE,
    		value = "/web/{idweb}",
    		consumes="application/json")
    public Object borraWeb(@PathVariable(value="idweb", required=true) String idweb);
	
    /** Manda a llamar el servicio de consulta de Documentos de un Aplicativo Funcional por Codigo Funcional.
	 * @param codigoAF Codigo Funcional.
	 * @return Documentos asociados al Codigo Funcional enviado.
	 */	
    @GetMapping(value="/documento/search/findByCodigoAF?codigoAF={codigoAF}", consumes="application/json")
	Response consultaDocumentos(@PathVariable(value="codigoAF", required=true) String codigoAF);
	
    /** Manda a llamar el servicio de agregar un Documento.
	 * @param documento Documento a agregar.
	 * @return Documento agregado.
	 */	
    @PostMapping(value = "/documento", consumes="application/json")
    public Object agregaDocumento(Documento documento);
	
    /** Manda a llamar el servicio de borrar Documento por Id de Documento.
	 * @param iddocumento Id de Documento.
	 * @return Entero indicando si el borrado del Documento tuvo exito.
	 */
    @DeleteMapping(value = "/documento/{iddocumento}", consumes="application/json")
    public Object borraDocumento(@PathVariable(value="iddocumento", required=true) String iddocumento);
	
    /** Manda a llamar el servicio de actualizar un Documento.
	 * @param iddocumento Id de Documento.
	 * @return Documento actualizado.
	 */
    @PutMapping(value = "/documento/{iddocumento}", consumes="application/json")
    public Object actualizaDocumento(Documento documento,
    		@PathVariable(value="iddocumento", required=true) String iddocumento);
	
    /** Manda a llamar el servicio de guardar un StadiumBianXAf.
	 * @param stadiumBianXAf StadiumBianXAf a guardar.
	 * @return StadiumBianXAf guardado.
	 */	
    @PostMapping(value = "/stadiumbianxaf",
    		consumes="application/json")
    public Object guardaStadiumBianXAf(StadiumBianXAf stadiumBianXAf);
	
    /** Manda a llamar el servicio de consulta de StadiumBianXAf por Id de Aplicativo Funcional.
	 * @param idAplicativoFuncional Id de Aplicativo Funcional.
	 * @return StadiumBianXAf asociados al Id de Aplicativo Funcional enviado.
	 */	
	@GetMapping(value="/stadiumbianxaf/search/findByIdAplicativoFuncional?idAplicativoFuncional={idAplicativoFuncional}",
			consumes="application/json")
	Response consultaStadiumBianXAf(@PathVariable(value="idAplicativoFuncional", required=true) String idAplicativoFuncional);
	
	 /** Manda a llamar el servicio de borrar StadiumBianXAf por Id de Dominio, Bian Nivel 2, Bian Nivel 3, Bian Afr, Bian Criticidad y Aplicativo Funcional.
	 * @param idDominio Id de Dominio.
	 * @param idBianNivel2 Id de Bian Nivel 2.
	 * @param idBianNivel3 Id de Bian Nivel 3.
	 * @param idBianAfr Id de Bian Afr.
	 * @param idBianCriticidad Id de Bian Criticidad.
	 * @param idAplicativoFuncional Id de Aplicativo Funcional.
	 * @return Entero indicando si el borrado del StadiumBianXAf tuvo exito.
	 */	 
	@GetMapping(value = "/stadiumbianxaf/search/deleteByAll?idDominio={idDominio}&idBianNivel2={idBianNivel2}&idBianNivel3={idBianNivel3}&idBianAfr={idBianAfr}&idBianCriticidad={idBianCriticidad}&idAplicativoFuncional={idAplicativoFuncional}",
    		consumes="application/json")
    public int deleteStadiumBianXAf(@PathVariable(value="idDominio", required=true) Long idDominio, 
    		@PathVariable(value="idBianNivel2", required=true) Long idBianNivel2, 
    		@PathVariable(value="idBianNivel3", required=true) Long idBianNivel3,
    		@PathVariable(value="idBianAfr", required=true) Long idBianAfr, 
    		@PathVariable(value="idBianCriticidad", required=true) Long idBianCriticidad, 
    		@PathVariable(value="idAplicativoFuncional", required=true) String idAplicativoFuncional);
	
    /** Manda a llamar el servicio de actualizar Aplicativo Funcional Ban.
	 * @param idAplicativoFuncionalBan Id de Aplicativo Funcional Ban.
	 * @return Aplicativo Funcional Ban actualizado.
	 */	
	@RequestMapping(method = RequestMethod.PUT,
    		value = "/aplicativofuncionalban/{idAplicativoFuncionalBan}",
    		consumes="application/json")
    public Object actualizaAplicativoFuncionalBan(AplicativoFuncionalBan aplicativoFuncionalBan,
    		@PathVariable(value="idAplicativoFuncionalBan", required=true) String idAplicativoFuncionalBan);
	
	/** Manda a llamar el servicio de agregar Aplicativo Funcional Ban.
	 * @param aplicativoFuncionalBan Aplicativo Funcional Ban a agregar.
	 * @return Aplicativo Funcional Ban agregado.
	 */	
    @RequestMapping(method = RequestMethod.POST,
    		value = "/aplicativofuncionalban",
    		consumes="application/json")
    public Object agregaAplicativoFuncionalBan(AplicativoFuncionalBan aplicativoFuncionalBan);
	
    /** Manda a llamar el servicio de consulta de Aplicativo Funcional Ban por Id de Aplicativo Funcional Ban.
	 * @param idAplicativoFuncionalBan Id de Aplicativo Funcional Ban.
	 * @return Aplicativo Funcional Ban asociado al Id de Aplicativo Funcional Ban enviado.
	 */	
	@RequestMapping(method= RequestMethod.GET,
			value="/aplicativofuncionalban/search/findByIdAplicativoFuncionalBan?idAplicativoFuncionalBan={idAplicativoFuncionalBan}",
			consumes="application/json")
	Response findByIdAplicativoFuncionalBan(@PathVariable(value="idAplicativoFuncionalBan", required=true) String idAplicativoFuncionalBan);
	
	/** Manda a llamar el servicio de actualizar Aplicativo Tecnico Ban.
	 * @param idAplicativoTecnicoBan Id de Aplicativo Tecnico Ban.
	 * @return Aplicativo Tecnico Ban actualizado.
	 */	
	@RequestMapping(method = RequestMethod.PUT,
    		value = "/aplicativotecnicoban/{idAplicativoTecnicoBan}",
    		consumes="application/json")
    public Object actualizaAplicativoTecnicoBan(AplicativoTecnicoBan aplicativoFuncionalBan,
    		@PathVariable(value="idAplicativoTecnicoBan", required=true) String idAplicativoTecnicoBan);
	
	/** Manda a llamar el servicio de agregar Aplicativo Tecnico Ban.
	 * @param aplicativoTecnicoBan Aplicativo Tecnico Ban a agregar.
	 * @return Aplicativo Tecnico Ban agregado.
	 */	
    @RequestMapping(method = RequestMethod.POST,
    		value = "/aplicativotecnicoban",
    		consumes="application/json")
    public Object agregaAplicativoTecnicoBan(AplicativoTecnicoBan aplicativoTecnicoBan);
	
    /** Manda a llamar el servicio de consulta de Aplicativo Tecnico Ban por Id de Aplicativo Tecnico Ban.
	 * @param idAplicativoTecnicoBan Id de Aplicativo Tecnico Ban.
	 * @return Aplicativo Tecnico Ban asociado al Id de Aplicativo Tecnico Ban enviado.
	 */	
	@RequestMapping(method= RequestMethod.GET,
			value="/aplicativotecnicoban/search/findByIdAplicativoTecnicoBan?idAplicativoTecnicoBan={idAplicativoTecnicoBan}",
			consumes="application/json")
	Response findByIdAplicativoTecnicoBan(@PathVariable(value="idAplicativoTecnicoBan", required=true) String idAplicativoTecnicoBan);
}
