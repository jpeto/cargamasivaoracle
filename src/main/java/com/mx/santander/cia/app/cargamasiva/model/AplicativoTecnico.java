package com.mx.santander.cia.app.cargamasiva.model;

import java.text.Collator;
import java.util.ArrayList;
import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.data.annotation.PersistenceConstructor;

import com.mx.santander.cia.app.cargamasiva.properties.Propiedades;


public class AplicativoTecnico implements Comparable<AplicativoTecnico>{
	
	private String _id;
	private String codigoOrbis;
	private String codigoTecnico;
	private String nombreTecnico;
	private String siglasAltair;
	private String estadoAplicacion;
	private String nombreComercial;
	private String descripcion;
	private Long entidadPropietaria;
	private String categoriaBanco;
	private String nombreProveedor;
	private String ambitoUso;
	private String repProveedor;
	private String tipoSoporte;
	private Long tipologiaSoftware;
	private Long criticidad;
	private String resServicio;
	private String telServicio;
	private String emailServicio;
	private String resMantenimiento;
	private String telMantenimiento;
	private String emailMantenimiento;
	private String hosting;
	private Long cpd;
	private String codigoRepositorio;
	private String apPortalizada;
	private String repAutorizacion;
	private String mecAutenticacion;
	private String repAutenticacion;
	private String mecAutorizacion;
	private String tecnologia;
	private String version;
	private String release;
	private int aRemedy;
	private int activo;
	private int fila;
	private String codigoAF;	
	private List <String> aplicativosRemedy;
	private List<ErrorArchivo> erroresMapeo;
	private boolean yaTieneAF;
	
	@PersistenceConstructor
	public AplicativoTecnico(){
		this.activo = 1;
		//this.aRemedy = 0;
	}

	/**
	 * Mapeo de una fila del excel de Aplicativos Tecnicos y validacion por campo.
	 * @param row Fila de Aplicativo Tecnico.
	 * @param criticidades Lista de Criticidades.
	 * @param entidades Lista de Entidades Propietarias.
	 * @param tipologias Lista de Tipologias.
	 * @param identificadores Lista de Identificadores Cpd.
	 */
	public void mapeaValores(Row row, List<CriticidadServicio> criticidades, 
			List<EntidadPropietaria> entidades, List<TipologiaSoftware> tipologias, List<IdentificadorCpd> identificadores){
		this.erroresMapeo = new ArrayList<>();
		this.codigoOrbis = validaCodigo(row.getCell(3), "Código Orbis", 10);
		this.codigoTecnico = validaCodigo(row.getCell(7), "Código de la Aplicación",10);
		this.nombreTecnico = valida(row.getCell(8), "Nombre Aplicación",120);
		this.nombreComercial = validaNoObl(row.getCell(9), "Nombre Comercial",120);
		this.descripcion = valida(row.getCell(10), "Descripción de Aplicación (breve)",1200);
		this.criticidad = validaCriticidad(row.getCell(11), criticidades);
		this.ambitoUso = validaAmbitoUso(row.getCell(12), "Ámbito de Uso");
		this.entidadPropietaria = validaEntidadProp(row.getCell(13), entidades);
		this.estadoAplicacion = validaEstadoAplicacion(row.getCell(14), "Estado de Aplicación");
		this.codigoAF = validaCodigo(row.getCell(16), "Código Aplicación Funcional asociada",10);
		this.categoriaBanco = validaCategoria(row.getCell(17), "Categoría Banco");
		this.tipologiaSoftware = validaTipologia(row.getCell(19), tipologias);
		this.tecnologia = valida(row.getCell(22), "Tecnología",250);
		this.nombreProveedor = valida(row.getCell(24), "Nombre Proveedor",120);
		this.repProveedor = valida(row.getCell(25), "Representante Proveedor",250);
		this.tipoSoporte = validaTipoSoporte(row.getCell(26), "Tipo de Soporte");
		this.resMantenimiento = valida(row.getCell(27), "Area Responsable Contacto Mantenimiento",80);
		this.telMantenimiento = valida(row.getCell(28), "Numero de Contacto Mantenimiento",40);
		this.emailMantenimiento = valida(row.getCell(29), "Email de Contacto Mantenimiento",60);
		this.resServicio = valida(row.getCell(30), "Area Responsable  Contacto Servicio IT",80);
		this.telServicio = valida(row.getCell(31), "Numero de Contacto Servicio IT",40);
		this.emailServicio = valida(row.getCell(32), "Email de Contacto Servicio IT",60);
		this.codigoRepositorio = validaNoObl(row.getCell(33), "Id Repositorio Locales de Software",40);
		this.version = valida(row.getCell(34), "Versión de Software",20);
		this.release = valida(row.getCell(35), "Release",20);
		this.siglasAltair = validaNoObl(row.getCell(36), "Código Aplicación Sigla (Altair)",50);
		this.hosting = validaHostingApp(row.getCell(37), "Hosting de la Aplicación (Externa / Interna)");
		this.cpd = validaIdentificador(row.getCell(38), identificadores);
		this.mecAutenticacion = validaMecanismos(row.getCell(39), "Mecanismos de Autentificación");
		this.repAutenticacion = validaNoObl(row.getCell(40), "Repositorio de Autentificación",40);
		this.apPortalizada = validaAppPortalizada(row.getCell(41), "Aplicación Portalizada");
		this.mecAutorizacion = validaMecanismos(row.getCell(42), "Mecanismos de Autorización (Perfilado)");
		this.repAutorizacion = validaNoObl(row.getCell(43), "Repositorio de Autorización",40);
	}
	
	/**
	 * Valida valor de una celda de excel: obligatoriedad, campo vacio y longitud de campo, generando las alertas correspondientes.
	 * @param c Celda de excel a validar.
	 * @param campo Nombre del campo de Aplicativo Tecnico a validar.
	 * @param longitud Longitud del campo de Aplicativo Tecnico a validar.
	 * @return Valor de celda de excel.
	 */
    private String valida(Cell c, String campo, int longitud) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información")
    				&& !c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){
    			if(c.getStringCellValue().length()>longitud) {
    				String mensaje = Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_LONGITUD);
    				mensaje = mensaje.replaceAll("<LONG>", longitud+"");
    				error.setCampo(campo);
    		    	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    		    	error.setMensaje(mensaje);
    				this.erroresMapeo.add(error);
    				return c.getStringCellValue().substring(0, longitud);
    			}else {
    				return c.getStringCellValue();
    			}
    		}
    	}
    	error.setCampo(campo);
    	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));
		this.erroresMapeo.add(error); 
		return null;
    }
    
    /**
   	 * Valida valor de una celda de excel: campo vacio y longitud de campo, generando las alertas correspondientes.
   	 * @param c Celda de excel.
   	 * @param campo Nombre del campo de Aplicativo Tecnico a validar.
   	 * @param longitud Longitud del campo de Aplicativo Tecnico a validar.
   	 * @return Valor de celda de excel.
   	 */
    private String validaNoObl(Cell c, String campo, int longitud) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información")
    				&& !c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){
    			if(c.getStringCellValue().length()>longitud) {
    				String mensaje = Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_LONGITUD);
    				mensaje = mensaje.replaceAll("<LONG>", longitud+"");
    				error.setCampo(campo);
    		    	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    		    	error.setMensaje(mensaje);
    				this.erroresMapeo.add(error);
    				return c.getStringCellValue().substring(0, longitud);
    			}else {
    				return c.getStringCellValue();
    			}
    		}
    	}
    	error.setCampo(campo);
    	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VACIO));
		this.erroresMapeo.add(error); 
		return null;
    }
    
    /**
	 * Valida campo Categoria de una celda de excel: campo vacio, obligatoriedad y valores permitidos, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param campo Nombre del campo de Aplicativo Tecnico a validar.
	 * @return Valor de celda de excel.
	 */
    private String validaCategoria(Cell c, String campo) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") 
    				&& !c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){

    			if(c.getStringCellValue().equals("Adoptada") || c.getStringCellValue().equals("Corporativa") 
    					|| c.getStringCellValue().equals("Local No Adoptada")) {
    				return c.getStringCellValue();
    			}
    			else {
    				error.setCampo(campo);
    				error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    				error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    			}

    		}
    		else {
    			error.setCampo(campo);
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));
    		}
    	}else {
    		error.setCampo(campo);
    		error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    		error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));
    	}
		this.erroresMapeo.add(error);
		return null;
    }

    /**
	 * Valida campo Estado de Aplicacion de una celda de excel: campo vacio y valores permitidos, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param campo Nombre del campo de Aplicativo Tecnico a validar.
	 * @return Valor de celda de excel.
	 */
    private String validaEstadoAplicacion(Cell c, String campo) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") 
    				&& !c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){

    			if(c.getStringCellValue().equals("Baja") || c.getStringCellValue().equals("Decomisionada") 
    					|| c.getStringCellValue().equals("Desarrollo") || c.getStringCellValue().trim().equals("Desuso/Inactiva")
    					|| c.getStringCellValue().equals("Producción") || c.getStringCellValue().equals("Preproducción")) {
    				return c.getStringCellValue();
    			}
    			else {
    				error.setCampo(campo);
    				error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    				error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    			}

    		}
    		else {
    			error.setCampo(campo);
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VACIO));
    		}

    	}else {
    		error.setCampo(campo);
    		error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    		error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VACIO));
    	}
		this.erroresMapeo.add(error);
		return null;
    }

    /**
	 * Valida campo Ambito de Uso de una celda de excel: campo vacio, obligatoriedad y valores permitidos, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param campo Nombre del campo de Aplicativo Tecnico a validar.
	 * @return Valor de celda de excel.
	 */
    private String validaAmbitoUso(Cell c, String campo) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") 
    				&& !c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")
    				&& !c.getStringCellValue().trim().equals("")){

    			if(c.getStringCellValue().equals("Propietario") || c.getStringCellValue().equals("Usuario")) {
    				return c.getStringCellValue();
    			}
    			else {
    				error.setCampo(campo);
    				error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    				error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    			}

    		}
    		else {
    			error.setCampo(campo);
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));
    		}

    	}else {
    		error.setCampo(campo);
    		error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    		error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));
    	}
		this.erroresMapeo.add(error);
		return null;
    }

    /**
	 * Valida campo Tipo de Soporte de una celda de excel: campo vacio, obligatoriedad y valores permitidos, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param campo Nombre del campo de Aplicativo Tecnico a validar.
	 * @return Valor de celda de excel.
	 */
    private String validaTipoSoporte(Cell c, String campo) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") 
    				&& !c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")
    				&& !c.getStringCellValue().trim().equals("")){

    			if(c.getStringCellValue().equals("24/7") || c.getStringCellValue().equals("Horario de Oficina")) {
    				return c.getStringCellValue();
    			}
    			else {
    				error.setCampo(campo);
    				error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    				error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    			}

    		}
    		else {
    			error.setCampo(campo);
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));
    		}

    	}else {
    		error.setCampo(campo);
    		error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    		error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));
    	}
		this.erroresMapeo.add(error);
		return null;
    }

    /**
	 * Valida campo Hosting de Aplicacion de una celda de excel: campo vacio, obligatoriedad y valores permitidos, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param campo Nombre del campo de Aplicativo Tecnico a validar.
	 * @return Valor de celda de excel.
	 */
    private String validaHostingApp(Cell c, String campo) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") 
    				&& !c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){

    			if(c.getStringCellValue().equals("Externo") || c.getStringCellValue().equals("Hybrid") ||
    					c.getStringCellValue().equals("Internal Cloud") || c.getStringCellValue().equals("Private Cloud (Single Tenant)") ||
    					c.getStringCellValue().equals("Public Cloud") || c.getStringCellValue().equals("On-Premises") ||
    					c.getStringCellValue().equals("Outsourced")) {
    				return c.getStringCellValue();
    			}
    			else {
    				error.setCampo(campo);
    				error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    				error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    			}

    		}
    		else {
    			error.setCampo(campo);
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));
    		}

    	}else {
    		error.setCampo(campo);
    		error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    		error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));
    	}
		this.erroresMapeo.add(error);
		return null;
    }

    /**
	 * Valida campo Aplicacion Portalizada de una celda de excel: campo vacio, obligatoriedad y valores permitidos, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param campo Nombre del campo de Aplicativo Tecnico a validar.
	 * @return Valor de celda de excel.
	 */
    private String validaAppPortalizada(Cell c, String campo) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") 
    				&& !c.getStringCellValue().trim().equals("")){

    			if(c.getStringCellValue().equals("Portalizado (SSO)") || c.getStringCellValue().equals("Portalizado (Sin SSO)") ||
    					c.getStringCellValue().equals("Sin Portalizar") || c.getStringCellValue().equals("Sin Datos")) {
    				return c.getStringCellValue();
    			}
    			else {
    				error.setCampo(campo);
    				error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    				error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    			}

    		}
    		else {
    			error.setCampo(campo);
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));
    		}

    	}else {
    		error.setCampo(campo);
    		error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    		error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));
    	}
		this.erroresMapeo.add(error);
		return null;
    }

    /**
	 * Valida campo Mecanismos de una celda de excel: campo vacio, obligatoriedad y valores permitidos, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param campo Nombre del campo de Aplicativo Tecnico a validar.
	 * @return Valor de celda de excel.
	 */
    private String validaMecanismos(Cell c, String campo) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") 
    				&& !c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){

    			if(c.getStringCellValue().equals("LDAP") || c.getStringCellValue().equals("DA") ||
    					c.getStringCellValue().equals("Host (RACF)") || c.getStringCellValue().equals("Otros")) {
    				return c.getStringCellValue();
    			}
    			else {
    				error.setCampo(campo);
    				error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    				error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    			}

    		}
    		else {
    			error.setCampo(campo);
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));
    		}

    	}else {
    		error.setCampo(campo);
    		error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    		error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));
    	}
		this.erroresMapeo.add(error);
		return null;
    }
    
    /**
	 * Valida campo Codigo de una celda de excel: campo vacio, estructura numerica y longitud de campo, generando las alertas y errores correspondientes.
	 * @param c Celda de excel.
	 * @param campo Nombre del campo de Aplicativo Tecnico a validar.
	 * @param longitud Longitud del campo de Aplicativo Tecnico a validar.
	 * @return Valor de celda de excel.
	 */
    private String validaCodigo(Cell c, String campo, int longitud) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") 
    				&& !c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().equals("")){
    			String corb = null;
			    if(c.getStringCellValue().contains("_")){
				   String[] div = c.getStringCellValue().trim().split("_");
				   corb = div[1];
				}else {
				   corb = c.getStringCellValue();
				}
    			if (corb.matches("[0-9]*")) {
    				if(corb.length()>longitud) {
        				String mensaje = Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_LONGITUD);
        				mensaje = mensaje.replaceAll("<LONG>", longitud+"");
        				error.setCampo(campo);
        		    	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
        		    	error.setMensaje(mensaje);
        				this.erroresMapeo.add(error);
        				return corb.substring(0, longitud);
        			}else {
        				return corb;
        			}
    		    }
    			 else {
     		    	error.setCampo(campo);
     	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
     	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VALIDO_CODIGO));
     	        	this.erroresMapeo.add(error);
    				return corb;
     		    }
    		} else {
	        	error.setCampo(campo);
	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ERROR));	    		
	    	}
    	} else {
        	error.setCampo(campo);
        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ERROR));    		
    	}
		this.erroresMapeo.add(error);  
		return null;
    }

    final static Collator instance = Collator.getInstance();
    
    /**
	 * Compara 2 strings removiendo sus acentos, para determinar si son iguales.
	 * @param a String.
	 * @param b String.
	 * @return Booleano indicando si los strings son o no iguales.
	 */
    private boolean removeAccents(String a, String b) {
        instance.setStrength(Collator.NO_DECOMPOSITION);  
        return (instance.compare(a,b) == 0 || a.equals(b));
    }
    
    /**
	 * Valida campo Criticidad de una celda de excel: campo vacio, obligatoriedad y valores permitidos, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param criticidades Lista de Criticidades.
	 * @return Valor de Id de Criticidad identificada.
	 */
    private Long validaCriticidad(Cell c, List<CriticidadServicio> criticidades) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") 
    				&& !c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){
    			for(CriticidadServicio e:criticidades){
    				if(e.getCategoria() != null && removeAccents(c.getStringCellValue().replace("CAT ", ""), e.getCategoria())){
    					if(e.getEstatus() == 1) {
    						return e.get_id();
    					}else {
    						error.setCampo("Criticidad");
    						error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    						error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    					}
    				}
    			}
    			error.setCampo("Criticidad");
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    		} else {
    			error.setCampo("Criticidad");
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));	    		
    		}
    	} else {
        	error.setCampo("Criticidad");
        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));    		
    	}
		this.erroresMapeo.add(error);    
		return null;
    }

    /**
	 * Valida campo Entidad Propietaria de una celda de excel: campo vacio, obligatoriedad y valores permitidos, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param entidades Lista de Entidades Propietarias.
	 * @return Valor de Id de Entidad Propietaria identificada.
	 */
    private Long validaEntidadProp(Cell c, List<EntidadPropietaria> entidades) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") 
    				&& !c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){
    			for(EntidadPropietaria e:entidades){
    				if(e.getEntidadPropietariaAp() != null && removeAccents(c.getStringCellValue(), e.getEntidadPropietariaAp())){
    					if(e.getEstatus() == 1) {
    						return e.get_id();
    					}else {
    						error.setCampo("Entidad Propietaria");
    						error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    						error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    					}
    				}
    			}
    			error.setCampo("Entidad Propietaria");
    			error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    			error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
	    	} else {
	        	error.setCampo("Entidad Propietaria");
	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));	    		
	    	}
    	} else {
        	error.setCampo("Entidad Propietaria");
        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));    		
    	}
		this.erroresMapeo.add(error);    
    	return null;
    }

    /**
	 * Valida campo Tipologia de una celda de excel: campo vacio, obligatoriedad y valores permitidos, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param tipologias Lista de Tiplogias.
	 * @return Valor de Id de Tipologia identificada.
	 */
    private Long validaTipologia(Cell c, List<TipologiaSoftware> tipologias) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") 
    				&& !c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){
    			for(TipologiaSoftware e:tipologias){
    				if(e.getTipologia() != null && removeAccents(c.getStringCellValue(), e.getTipologia())){
    					if(e.getEstatus() == 1) {
    						return e.get_id();
    					}else {
    						error.setCampo("Tipología de Software");
    						error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    						error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    					}
    				}
    			}
				error.setCampo("Tipología de Software");
	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
	    	} else {
	        	error.setCampo("Tipología de Software");
	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));	    		
	    	}
    	} else {
        	error.setCampo("Tipología de Software");
        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));    		
    	}
		this.erroresMapeo.add(error);    
    	return null;
    }

    /**
	 * Valida campo Identificador Cpd de una celda de excel: campo vacio, obligatoriedad y valores permitidos, generando las alertas correspondientes.
	 * @param c Celda de excel.
	 * @param identificadores Lista de Identificadores Cpd.
	 * @return Valor de Id de Identificador Cpd identificado.
	 */
    private Long validaIdentificador(Cell c, List<IdentificadorCpd> identificadores) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null && c.getStringCellValue() != null){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") 
    				&& !c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){
    			for(IdentificadorCpd e:identificadores){
    				if(e.getCpd() != null && removeAccents(c.getStringCellValue(), e.getCpd())){
    					if(e.getEstatus() == 1) {
    						return e.get_id();
    					}else {
    						error.setCampo("Identificador CPD");
    						error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    						error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));

    					}
    				}
    			}
				error.setCampo("Identificador CPD");
	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
	    	} else {
	        	error.setCampo("Identificador CPD");
	        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
	        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));	    		
	    	}
    	} else {
        	error.setCampo("Identificador CPD");
        	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
        	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_OBLIGATORIO_ALERTA));    		
    	}
		this.erroresMapeo.add(error);    
    	return null;
    }

    /**
	 * Compara 2 Aplicativos Tecnicos, para determinar si son iguales por Codigo de AF.
	 * @param o Aplicativo Tecnico.
	 * @return Entero indicando si los Aplicativos Tecnicos son o no iguales.
	 */
	@Override
	public int compareTo(AplicativoTecnico o) {
		String a = this.codigoAF;
		String b = o.getCodigoAF();
		return a.compareTo(b);
	}
	
	public String get_id() {
		return _id;
	}
	public void set_id(String _id) {
		this._id = _id;
	}
	public String getSiglasAltair() {
		return siglasAltair;
	}
	public void setSiglasAltair(String siglasAltair) {
		this.siglasAltair = siglasAltair;
	}
	public String getEstadoAplicacion() {
		return estadoAplicacion;
	}
	public void setEstadoAplicacion(String estadoAplicacion) {
		this.estadoAplicacion = estadoAplicacion;
	}
	public String getNombreComercial() {
		return nombreComercial;
	}
	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Long getEntidadPropietaria() {
		return entidadPropietaria;
	}
	public void setEntidadPropietaria(Long entidadPropietaria) {
		this.entidadPropietaria = entidadPropietaria;
	}
	public String getCategoriaBanco() {
		return categoriaBanco;
	}
	public void setCategoriaBanco(String categoriaBanco) {
		this.categoriaBanco = categoriaBanco;
	}
	public String getNombreProveedor() {
		return nombreProveedor;
	}
	public void setNombreProveedor(String nombreProveedor) {
		this.nombreProveedor = nombreProveedor;
	}
	public String getAmbitoUso() {
		return ambitoUso;
	}
	public void setAmbitoUso(String ambitoUso) {
		this.ambitoUso = ambitoUso;
	}
	public String getRepProveedor() {
		return repProveedor;
	}
	public void setRepProveedor(String repProveedor) {
		this.repProveedor = repProveedor;
	}
	public String getTipoSoporte() {
		return tipoSoporte;
	}
	public void setTipoSoporte(String tipoSoporte) {
		this.tipoSoporte = tipoSoporte;
	}
	public Long getTipologiaSoftware() {
		return tipologiaSoftware;
	}
	public void setTipologiaSoftware(Long tipologiaSoftware) {
		this.tipologiaSoftware = tipologiaSoftware;
	}
	public Long getCriticidad() {
		return criticidad;
	}
	public void setCriticidad(Long criticidad) {
		this.criticidad = criticidad;
	}
	public String getResServicio() {
		return resServicio;
	}
	public void setResServicio(String resServicio) {
		this.resServicio = resServicio;
	}
	public String getTelServicio() {
		return telServicio;
	}
	public void setTelServicio(String telServicio) {
		this.telServicio = telServicio;
	}
	public String getEmailServicio() {
		return emailServicio;
	}
	public void setEmailServicio(String emailServicio) {
		this.emailServicio = emailServicio;
	}
	public String getResMantenimiento() {
		return resMantenimiento;
	}
	public void setResMantenimiento(String resMantenimiento) {
		this.resMantenimiento = resMantenimiento;
	}
	public String getTelMantenimiento() {
		return telMantenimiento;
	}
	public void setTelMantenimiento(String telMantenimiento) {
		this.telMantenimiento = telMantenimiento;
	}
	public String getEmailMantenimiento() {
		return emailMantenimiento;
	}
	public void setEmailMantenimiento(String emailMantenimiento) {
		this.emailMantenimiento = emailMantenimiento;
	}
	public String getHosting() {
		return hosting;
	}
	public void setHosting(String hosting) {
		this.hosting = hosting;
	}
	public Long getCpd() {
		return cpd;
	}
	public void setCpd(Long cpd) {
		this.cpd = cpd;
	}
	public String getRepAutorizacion() {
		return repAutorizacion;
	}
	public void setRepAutorizacion(String repAutorizacion) {
		this.repAutorizacion = repAutorizacion;
	}
	public String getMecAutenticacion() {
		return mecAutenticacion;
	}
	public void setMecAutenticacion(String mecAutenticacion) {
		this.mecAutenticacion = mecAutenticacion;
	}
	public String getRepAutenticacion() {
		return repAutenticacion;
	}
	public void setRepAutenticacion(String repAutenticacion) {
		this.repAutenticacion = repAutenticacion;
	}
	public String getMecAutorizacion() {
		return mecAutorizacion;
	}
	public void setMecAutorizacion(String mecAutorizacion) {
		this.mecAutorizacion = mecAutorizacion;
	}
	public String getNombreTecnico() {
		return nombreTecnico;
	}
	public void setNombreTecnico(String nombreTecnico) {
		this.nombreTecnico = nombreTecnico;
	}
	public String getTecnologia() {
		return tecnologia;
	}
	public void setTecnologia(String tecnologia) {
		this.tecnologia = tecnologia;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getRelease() {
		return release;
	}
	public void setRelease(String release) {
		this.release = release;
	}

	public String getCodigoRepositorio() {
		return codigoRepositorio;
	}
	public void setCodigoRepositorio(String codigoRepositorio) {
		this.codigoRepositorio = codigoRepositorio;
	}

	public String getApPortalizada() {
		return apPortalizada;
	}
	public void setApPortalizada(String apPortalizada) {
		this.apPortalizada = apPortalizada;
	}
	public List <String> getAplicativosRemedy() {
		return aplicativosRemedy;
	}
	public void setAplicativosRemedy(List <String> aplicativosRemedy) {
		this.aplicativosRemedy = aplicativosRemedy;
	}
	public String getCodigoTecnico() {
		return codigoTecnico;
	}
	public void setCodigoTecnico(String codigoTecnico) {
		this.codigoTecnico = codigoTecnico;
	}
	public int getaRemedy() {
		return aRemedy;
	}
	public void setaRemedy(int aRemedy) {
		this.aRemedy = aRemedy;
	}
	public int getActivo() {
		return activo;
	}
	public void setActivo(int activo) {
		this.activo = activo;
	}
	public void setCodigoOrbis(String codigoOrbis) {
		this.codigoOrbis = codigoOrbis;
	}
	public String getCodigoOrbis() {
		return codigoOrbis;
	}
	
	public int getFila() {
		return fila;
	}


	public void setFila(int fila) {
		this.fila = fila;
	}


	public String getCodigoAF() {
		return codigoAF;
	}


	public void setCodigoAF(String codigoAF) {
		this.codigoAF = codigoAF;
	}

	public List<ErrorArchivo> getErroresMapeo() {
		return erroresMapeo;
	}

	public void setErroresMapeo(List<ErrorArchivo> erroresMapeo) {
		this.erroresMapeo = erroresMapeo;
	}

	public boolean isYaTieneAF() {
		return yaTieneAF;
	}

	public void setYaTieneAF(boolean yaTieneAF) {
		this.yaTieneAF = yaTieneAF;
	}
}