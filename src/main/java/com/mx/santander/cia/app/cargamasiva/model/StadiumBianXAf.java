package com.mx.santander.cia.app.cargamasiva.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StadiumBianXAf {
	
	private Long idDominio;
	private Long idBianNivel2;
	private Long idBianNivel3;
	private Long idBianAfr;
	private Long idBianCriticidad;
	private String idAplicativoFuncional;
	private int modificado;

	public Long getIdDominio() {
		return idDominio;
	}

	public void setIdDominio(Long idDominio) {
		this.idDominio = idDominio;
	}

	public Long getIdBianNivel2() {
		return idBianNivel2;
	}

	public void setIdBianNivel2(Long idBianNivel2) {
		this.idBianNivel2 = idBianNivel2;
	}

	public Long getIdBianNivel3() {
		return idBianNivel3;
	}

	public void setIdBianNivel3(Long idBianNivel3) {
		this.idBianNivel3 = idBianNivel3;
	}

	public Long getIdBianAfr() {
		return idBianAfr;
	}

	public void setIdBianAfr(Long idBianAfr) {
		this.idBianAfr = idBianAfr;
	}

	public Long getIdBianCriticidad() {
		return idBianCriticidad;
	}

	public void setIdBianCriticidad(Long idBianCriticidad) {
		this.idBianCriticidad = idBianCriticidad;
	}
	
	public String getIdAplicativoFuncional() {
		return idAplicativoFuncional;
	}

	public void setIdAplicativoFuncional(String idAplicativoFuncional) {
		this.idAplicativoFuncional = idAplicativoFuncional;
	}

	public int getModificado() {
		return modificado;
	}

	public void setModificado(int modificado) {
		this.modificado = modificado;
	}
}