package com.mx.santander.cia.app.cargamasiva.rest;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Map.Entry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.mx.santander.cia.app.cargamasiva.db.DatabaseConn;
import com.mx.santander.cia.app.cargamasiva.model.*;
import com.mx.santander.cia.app.cargamasiva.properties.Propiedades;
import com.mx.santander.cia.app.cargamasiva.vo.LectorArchivo;
import com.mx.santander.cia.app.cargamasiva.vo.ProcesaCMDB;
import com.mx.santander.cia.app.cargamasiva.vo.ValidadorCatalogo;
import static com.mx.santander.cia.app.cargamasiva.vo.IdGenerator.getId;
import static com.mx.santander.cia.app.cargamasiva.vo.ArchivoUtil.generaReporte;
import static com.mx.santander.cia.app.cargamasiva.vo.ArchivoUtil.getPrefix;
import static com.mx.santander.cia.app.cargamasiva.env.Environment.cmPath;

@RestController
public class RestServices {
	@Autowired
	DatabaseConn databaseConn;
    String base_path = cmPath;
	
    /**
	 * Valida nombre de los archivos que se van a cargar masivamente.
	 * @param peticion Peticion con nombre de archivos y su ubicacion.
	 * @return Respuesta con estatus de validacion y mensaje de error si aplica.
	 */
    @PutMapping(value = "/validarNombres")
    public @ResponseBody
    Object validarNombres(@RequestBody Peticion peticion) {
    	Respuesta resp = new Respuesta();    
    	resp.setStatus("200");
    	
    	if (peticion.getRuta().equals("CM-AF-AT")) {
    		if (!peticion.getArchivoA().endsWith(".xlsx") || !peticion.getArchivoB().endsWith(".xlsx")) {
    			resp.setStatus("500");
                resp.setMessage("Los archivos deben tener extensión '.xlsx'.");
    		} else if (peticion.getArchivoA().contains("Loaded") || peticion.getArchivoB().contains("Loaded")) {
    			resp.setStatus("500");
                resp.setMessage("Los archivos no deben tener la palabra 'Loaded' en su nombre.");
    		} else if (!peticion.getArchivoA().contains("func_apps_list")) {
    			resp.setStatus("500");
                resp.setMessage("El archivo AF debe tener 'func_apps_list' en su nombre.");
    		} else if (!peticion.getArchivoB().contains("TechView")) {
    			resp.setStatus("500");
                resp.setMessage("El archivo AT debe tener 'TechView' en su nombre.");
    		}
    	} else if (peticion.getRuta().equals("CM-AR")) {
    		if (!peticion.getArchivoA().endsWith(".xlsx")) {
    			resp.setStatus("500");
                resp.setMessage("El archivo Remedy debe tener extensión '.xlsx'.");
    		} else if (peticion.getArchivoA().contains("Loaded")) {
    			resp.setStatus("500");
                resp.setMessage("El archivo Remedy no debe tener la palabra 'Loaded' en su nombre.");
    		} else if (!peticion.getArchivoA().contains("Remedys")) {
    			resp.setStatus("500");
                resp.setMessage("El archivo Remedy debe tener 'Remedys' en su nombre.");
    		}
    	} else if (peticion.getRuta().equals("CM-CMDB")) {
    		if (!peticion.getArchivoA().endsWith(".xlsx")) {
    			resp.setStatus("500");
                resp.setMessage("El archivo CMDB debe tener extensión '.xlsx'.");
    		} else if (peticion.getArchivoA().contains("Loaded")) {
    			resp.setStatus("500");
                resp.setMessage("El archivo CMDB no debe tener la palabra 'Loaded' en su nombre.");
    		} else if (!peticion.getArchivoA().contains("Extracto_Semanal")) {
    			resp.setStatus("500");
                resp.setMessage("El archivo CMDB debe tener 'Extracto_Semanal' en su nombre.");
    		}
    	}   	
    	
        return resp;
    }
    
    /**
	 * Agrega documento a la ruta de la variable base_path en el servidor.
	 * @param documento Documento a agregar.
	 * @return Documento agregado.
	 */
    @PutMapping(value = "/subirArchivo")
    public @ResponseBody
    Object agregardocumento(@RequestBody Documento documento) {
        Documento doc = documento;
        String doc_path = base_path + File.separator + documento.getNombre();
        try {
            FileUtils.writeByteArrayToFile(new File(doc_path), doc.getContenido());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return doc;
    }

    /**
	 * Carga masivamente documentos de Aplicativos Funcionales y Tecnicos.
	 * @param peticion Peticion con nombre de archivos y su ubicacion.
	 * @return Respuesta con estatus de carga, mensaje de error si aplica y archivos de alertas-errores.
	 */
	@RequestMapping(value = "/cargaaft", method = RequestMethod.PUT)
	public @ResponseBody Object cargaaft(@RequestBody Peticion peticion){
        Respuesta resp = new Respuesta();
        
        String rutaAF = peticion.getRuta() + peticion.getArchivoA();
        String rutaAT = peticion.getRuta() + peticion.getArchivoB();
        String id = null;
        String idGenerada = null;
        String idInsertada = null;
        String prefix = getPrefix();
        int afAgregados = 0;
        int afActualizados = 0;
        boolean actualiza = false;
        List<ErrorArchivo> listaReporteAF = new ArrayList<>();
        
        LectorArchivo la = new LectorArchivo();
        ValidadorCatalogo vc = new ValidadorCatalogo();
        
        List<Entidad> entidades = databaseConn.consultaListadoEntidad().get_embedded().getEntidad();
		List<Mapa> mapas = databaseConn.consultaListadoMapa().get_embedded().getMapa();
		List<Capa> capas = databaseConn.consultaListadoCatCapa().get_embedded().getCapa();
		List<Sistema> sistemas = databaseConn.consultaListadoCatSistema().get_embedded().getSistema();
		List<Subsistema> subsistemas = databaseConn.consultaListadoCatSubsistema().get_embedded().getSubsistema();
		List<CategoriaAplicativo> categorias = databaseConn.consultaCategoriaAplicativo().get_embedded().getCategoriaaplicativo();
		List<Dominio> dominios = databaseConn.consultaListadoDominio().get_embedded().getDominio();
		List<BianCriticidad> biancriticidades = databaseConn.consultaListadoCatBianCriticidad().get_embedded().getBian_criticidad();
		List<BianAfr> bianafrs = databaseConn.consultaListadoCatBianAfr().get_embedded().getBian_afr();
		List<BianNivel2> bianniveles2 = databaseConn.consultaListadoCatBianNivel2().get_embedded().getBian_nivel2();
		List<BianNivel3> bianniveles3 = databaseConn.consultaListadoCatBianNivel3().get_embedded().getBian_nivel3();
		List<PaqueteBian> paquetesbian = databaseConn.consultaPaqueteBian().get_embedded().getPaquetebian();
		List<GranularidadBian> granularidadesbian = databaseConn.consultaGranularidadBian().get_embedded().getGranularidadbian();
		
        List<AplicativoFuncional> AF = la.leerArchivoAplicacionesFuncionales(rutaAF, entidades, mapas, capas, sistemas, subsistemas,
        		categorias, dominios, biancriticidades,bianafrs, bianniveles2, bianniveles3, paquetesbian, granularidadesbian);
        
        List<EntidadMapa> EM = databaseConn.consultaEntidadMapa().get_embedded().getEntidadmapa();
        List<EstructuraCatalogacion> EC = databaseConn.consultaEstructuraCatalogacion().get_embedded().getEstructuracatalogacion();
        List<StadiumBian> SB = databaseConn.consultaStadiumbian().get_embedded().getStadiumbian();
        List<AplicativoFuncional> listaAplicativoFuncional = databaseConn.consultaAplicativoFuncional().get_embedded().getAplicativofuncional();
        List<String> codigosAgregados = new ArrayList<>();
        AF = vc.validaCatalogosAplicativoFuncional(AF,EM,EC,SB);
        
        for(AplicativoFuncional af:AF){
           for(AplicativoFuncional a: listaAplicativoFuncional){
        	   if(af.getCodigoFuncional().equals(a.getCodigoFuncional())){
        		   actualiza = true;
        		   id = a.get_id();
        		   break;
        	   }
           }
           if(actualiza){
        	   af.set_id(id);
        	   af.setActivo(1);
        	   databaseConn.actualizaAplicativoFuncional(af, id);
          	   AplicativoFuncionalBan afBan = new AplicativoFuncionalBan();
          	   afBan.setIdAplicativoFuncionalBan(id);
          	   databaseConn.actualizaAplicativoFuncionalBan(afBan, id);
        	   af.setEstado(1);
        	   afActualizados+=1;
        	   System.out.println("AF: " + af.getCodigoFuncional()+ " Actualizado");
           }else {
        	   Random rnd = new Random();
        	   idGenerada = getId() + String.valueOf(rnd.nextInt(999));
 	           while(idGenerada.equals(idInsertada)){
 	        	  idGenerada = getId();
 	           }
 	           af.set_id(idGenerada);
        	   databaseConn.agregaAplicativoFuncional(af);
          	   AplicativoFuncionalBan afBan = new AplicativoFuncionalBan();
          	   afBan.setIdAplicativoFuncionalBan(idGenerada);
          	   databaseConn.agregaAplicativoFuncionalBan(afBan);
        	   af.setEstado(0);
        	   afAgregados+=1;
        	   idInsertada = idGenerada;
        	   System.out.println("AF: " + af.getCodigoFuncional()+ " Agregado");
               listaAplicativoFuncional.add(af);
           }
           if (!codigosAgregados.contains(af.getCodigoFuncional())) {
        	   codigosAgregados.add(af.getCodigoFuncional());
           }
           actualiza = false;
           id = null;
    	   if(af.getBnombre()!=null && af.getBcriticidad()!=null && af.getBnivel1()!=null && af.getBnivel2()!=null && af.getBnivel3()!=null){
    		  StadiumBianXAf stadium = new StadiumBianXAf();
	    	  stadium.setIdAplicativoFuncional(af.get_id());
	    	  stadium.setIdBianAfr(af.getBnombre());
	    	  stadium.setIdBianNivel2(af.getBnivel2());
	    	  stadium.setIdBianNivel3(af.getBnivel3());
	    	  stadium.setIdDominio(af.getBnivel1());
	    	  stadium.setIdBianCriticidad(af.getBcriticidad());
	    	  stadium.setModificado(0);
	          List<StadiumBianXAf> stadiumBian = databaseConn.consultaStadiumBianXAf(af.get_id()).get_embedded().getStadiumbianxaf();
	          if (!stadiumBian.isEmpty()) {
		          for (StadiumBianXAf sb: stadiumBian) {
				      if(!(sb.getIdDominio().equals(af.getBnivel1()) && sb.getIdBianNivel2().equals(af.getBnivel2()) && sb.getIdBianNivel3().equals(af.getBnivel3())
				    		  && sb.getIdBianAfr().equals(af.getBnombre()) && sb.getIdBianCriticidad().equals(af.getBcriticidad()))){			    	  
				    	  databaseConn.guardaStadiumBianXAf(stadium);
				      }
		          }
	          } else {
	        	  databaseConn.guardaStadiumBianXAf(stadium);
	          }
    	   }
    	   
    	   if (af.getResponsables() != null) {
    		   databaseConn.borraResponsablesXAfXIdAf(af.get_id());
    		   for (Responsables responsable: af.getResponsables()) {
    			   responsable.setIdAplicativo(af.get_id());
    			   responsable.setModificado(0);
    			   databaseConn.creaResponsables(responsable);
    		   }
    	   }
        }
        
        id = null;
        idGenerada = null;
        idInsertada = null; 
        String codigoAF = "";
        int atAgregados = 0;
        int atActualizados = 0;
        actualiza = false;
        Map<String,List<AplicacionAsociada>> map = new HashMap<>();
        List<TipologiaSoftware> TS = databaseConn.consultaTipologiaSoftware().get_embedded().getTipologiasoftware();
        List<IdentificadorCpd> IC = databaseConn.consultaIdentificadorCpd().get_embedded().getIdentificadorcpd();
        List<EntidadPropietaria> EP = databaseConn.consultaEntidadPropietaria().get_embedded().getEntidadpropietaria();
        List<CriticidadServicio> CS = databaseConn.consultaCriticidadServicio().get_embedded().getCriticidadservicio();
        listaAplicativoFuncional.clear();
        listaAplicativoFuncional = databaseConn.consultaAplicativoFuncional().get_embedded().getAplicativofuncional();
        List<AplicativoTecnico> listaAplicativoTecnico = databaseConn.consultaAplicativoTecnico().get_embedded().getAplicativotecnico();
                
        List<AplicativoTecnico> AT = la.leerArchivoAplicacionesTecnicas(rutaAT, CS, EP, TS, IC);
        List<ErrorArchivo> listaReporteAT = new ArrayList<>();
        
        AT = vc.validaCatalogosAplicativoTecnico(AT);
        
        Collections.sort(AT);
        for(AplicativoTecnico at: AT){
        	boolean yaTienAF = false;
        	at.setYaTieneAF(false);
        	//validando si ya tiene un AF asignado
    		for(AplicativoTecnico atInterno: AT){
    			if (atInterno.getCodigoOrbis().equals(at.getCodigoOrbis()) && atInterno.isYaTieneAF()) {
    				ErrorArchivo errorAT = new ErrorArchivo();
	              	errorAT.setFila(at.getFila()+"");
	              	errorAT.setIdentificador(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_ID_AT) + " " + at.getCodigoOrbis());
	              	errorAT.setCampo("Código Orbis");
	              	errorAT.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
	              	errorAT.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_AT_REPETIDO));
         		    listaReporteAT.add(errorAT);
         		    yaTienAF = true;
         		    break;
    			}        			
    		}        	
        	if (!yaTienAF) {
            	boolean vinculado = false;
            	for(AplicativoFuncional af: listaAplicativoFuncional){
            	   //esta vinculado a un AF
                  if(at.getCodigoAF().equals(af.getCodigoFuncional())){
                	 vinculado = true;
                     if(!codigoAF.equals(at.getCodigoAF())){
                    	 List<AplicacionAsociada> laa = new ArrayList<>();
                    	 AplicacionAsociada aa = new AplicacionAsociada();
                    	 aa.setCodigoOrbis(at.getCodigoOrbis());
                    	 aa.setNombreTecnico(at.getNombreTecnico());
                    	 laa.add(aa);
                    	 map.put(at.getCodigoAF(), laa);
                    	 codigoAF = at.getCodigoAF();
                     }else{
                    	 AplicacionAsociada aa = new AplicacionAsociada();
                    	 aa.setCodigoOrbis(at.getCodigoOrbis());
                    	 aa.setNombreTecnico(at.getNombreTecnico());
                    	 map.get(at.getCodigoAF()).add(aa);
                     }
                     
                     for(AplicativoTecnico t: listaAplicativoTecnico){
                    	 if(at.getCodigoOrbis().equals(t.getCodigoOrbis())){
                    		 actualiza = true;
                    		 id = t.get_id();
                    		 at.setaRemedy(t.getaRemedy());
                    		 break;
                    	 }
                     }
                     
                     if(actualiza){
                  	   at.set_id(id);
                  	   databaseConn.actualizaAplicativoTecnico(at, id);
                  	   AplicativoTecnicoBan atBan = new AplicativoTecnicoBan();
                  	   atBan.setIdAplicativoTecnicoBan(id);
                  	   databaseConn.actualizaAplicativoTecnicoBan(atBan, id);
                  	   atActualizados+=1;
                  	   System.out.println("AT: " + at.getCodigoOrbis()+ " Actualizado");
                     }else {
                       Random rnd = new Random();
                  	   idGenerada = getId() + String.valueOf(rnd.nextInt(999));
           	           while(idGenerada.equals(idInsertada)){
           	        	  idGenerada = getId();
           	           }
           	           at.set_id(idGenerada);
                  	   databaseConn.agregaAplicativoTecnico(at);
                  	   AplicativoTecnicoBan atBan = new AplicativoTecnicoBan();
                  	   atBan.setIdAplicativoTecnicoBan(idGenerada);
                  	   databaseConn.agregaAplicativoTecnicoBan(atBan);
                  	   atAgregados+=1;
              	       idInsertada = idGenerada;
                  	   System.out.println("AT: " + at.getCodigoOrbis()+ " Agregado");
                     }
                     actualiza = false;
                     id = null;
                     vinculado = true;
                     at.setYaTieneAF(true);
                     break;
                  }
               }
               
               if(!vinculado) {
            	  ErrorArchivo errorAT = new ErrorArchivo();
            	  errorAT.setFila(at.getFila()+"");
            	  errorAT.setIdentificador(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_ID_AT) + " " + at.getCodigoOrbis());
            	  errorAT.setCampo("Código Aplicación Funcional asociada");
            	  errorAT.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
            	  errorAT.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_AT));
       		      listaReporteAT.add(errorAT);
       		   }
        	}
        }
        
        //eliminacion de ATxAF e insercion de relaciones de carga
        for(Entry<String,List<AplicacionAsociada>> e : map.entrySet()){
        	List<AplicativoFuncional> af = new ArrayList<>();
        	af = databaseConn.consultaAFXCodigoFuncional(e.getKey()).get_embedded().getAplicativofuncional();
        	List<ATXAF> listaatxaf = databaseConn.consultaATxAF(af.get(0).get_id()).get_embedded().getAtxaf();
        	if(!listaatxaf.isEmpty()){
        		for(ATXAF atf: listaatxaf){
        			databaseConn.borraAtXAf(atf.getIdAplicativoFuncional(), atf.getIdAplicativoTecnico());
        		}
        	}
        	
        	for(AplicacionAsociada aa: e.getValue()){
        		List<AplicativoTecnico> at = databaseConn.consultaATXCodigoOrbis(aa.getCodigoOrbis()).get_embedded().getAplicativotecnico();
        		ATXAF atxaf = new ATXAF();
        		atxaf.setIdAplicativoFuncional(af.get(0).get_id());
				atxaf.setIdAplicativoTecnico(at.get(0).get_id());
				atxaf.setVinculado(1);				
        		databaseConn.agregaAtXAf(atxaf);
        	}
        }

		int contador = 5;
        for(AplicativoFuncional af:AF){
        	contador+=1;
        	List<ATXAF> atxaf = databaseConn.consultaATxAF(af.get_id()).get_embedded().getAtxaf();
        	if (atxaf.isEmpty()) {
        		ErrorArchivo aFSinAT = new ErrorArchivo();
        		aFSinAT.setFila(contador+"");
        		aFSinAT.setIdentificador(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_ID_AF) + " " + af.getCodigoFuncional());
        		aFSinAT.setCampo("Código Funcional");
        		aFSinAT.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
        		aFSinAT.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_AF));
                listaReporteAF.add(aFSinAT);
                if (af.getEstado() == 1) {
                	afActualizados = afActualizados-1;
                } else {
                	afAgregados = afAgregados-1;
                }
                List<StadiumBianXAf> stadiumBian = databaseConn.consultaStadiumBianXAf(af.get_id()).get_embedded().getStadiumbianxaf();
				if (!stadiumBian.isEmpty()) {
					for (StadiumBianXAf sb: stadiumBian) {		    	  
						databaseConn.deleteStadiumBianXAf(sb.getIdDominio(), sb.getIdBianNivel2(), sb.getIdBianNivel3(), 
								sb.getIdBianAfr(), sb.getIdBianCriticidad(), af.get_id());
					}
				}
				if (af.getResponsables() != null) {
					databaseConn.borraResponsablesXAfXIdAf(af.get_id());
				}
		        if (codigosAgregados.contains(af.getCodigoFuncional())) {
					databaseConn.borraAplicativoFuncionalBan(af.get_id());
	    			databaseConn.borraAplicativoFuncional(af.get_id());
	    			codigosAgregados.remove(af.getCodigoFuncional());
	        	}
        	}
        }
        
        String archivoReporteAF = peticion.getRuta() + prefix + peticion.getArchivoA().replace(".xlsx", ".csv");
	  	ErrorArchivo repAF = new ErrorArchivo();
	  	repAF.setFila("N/A");
	  	repAF.setIdentificador("N/A");
	  	repAF.setCampo("N/A");
	  	repAF.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_REPORTE));
	  	repAF.setMensaje(afAgregados + " Agregados y " + afActualizados + " Actualizados");    
        listaReporteAF.addAll(vc.getAfRechazados());    
        listaReporteAF.add(0,repAF);
        
        String archivoReporteAT = peticion.getRuta() + prefix + peticion.getArchivoB().replace(".xlsx", ".csv");
        ErrorArchivo repAT = new ErrorArchivo();
        repAT.setFila("N/A");
        repAT.setIdentificador("N/A");
        repAT.setCampo("N/A");
        repAT.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_REPORTE));
        repAT.setMensaje(atAgregados + " Agregados y " + atActualizados + " Actualizados");  
        listaReporteAT.addAll(vc.getAtRechazados());
        listaReporteAT.add(0,repAT);
        
        generaReporte(archivoReporteAF,listaReporteAF); 
        generaReporte(archivoReporteAT,listaReporteAT);  
        
        File archivoA = new File(peticion.getRuta() + peticion.getArchivoA());
        archivoA.delete();
        File archivoB = new File(peticion.getRuta() + peticion.getArchivoB());  
        archivoB.delete();      
    
        resp.setTimestamp(new DateTime().toString());
        resp.setStatus("200");
        resp.setMessage("Terminado Exitosamente");
        
        File archivoErroresA = new File(archivoReporteAF);
        File archivoErroresB = new File(archivoReporteAT);
        byte[] doc_bytes_A = null;
        byte[] doc_bytes_B = null;
        try {
        	doc_bytes_A = FileUtils.readFileToByteArray(archivoErroresA);
        	doc_bytes_B = FileUtils.readFileToByteArray(archivoErroresB);
        } catch (IOException e) {
            e.printStackTrace();
        }
        resp.setNombreA(prefix + peticion.getArchivoA().replace(".xlsx", ".csv"));
        resp.setNombreB(prefix + peticion.getArchivoB().replace(".xlsx", ".csv"));
        resp.setArchivoErroresA(doc_bytes_A);
        resp.setArchivoErroresB(doc_bytes_B);
        archivoErroresA.delete();
        archivoErroresB.delete();
                
        System.out.println("====> Carga AFT Finalizada <====");
        return resp;
	}
	
	/**
	 * Carga masivamente documento de Aplicativos Remedy.
	 * @param peticion Peticion con nombre de archivo y su ubicacion.
	 * @return Respuesta con estatus de carga, mensaje de error si aplica y archivo de alertas-errores.
	 */
	@RequestMapping(value = "/cargaaplicativoremedy", method = RequestMethod.PUT)
	public @ResponseBody Object cargaaplicativoremedy(@RequestBody Peticion peticion){
        Respuesta resp = new Respuesta();
        
        String ruta = peticion.getRuta() + peticion.getArchivoA();
        String prefix = getPrefix();
        String idGenerada = null;
        String idInsertada = null;
        LectorArchivo la = new LectorArchivo();
        ValidadorCatalogo vc = new ValidadorCatalogo();
        int actualizados = 0;
        List<ErrorArchivo> listaReporteAR = new ArrayList<>();
        
        Map<String,List<SheetRemedy>> apRemedy = vc.validaAplicacionesRemedy(la.leerArchivoAplicacionesRemedy(ruta));
        
        List<AplicativoTecnico> listaAplicativoTecnico = databaseConn.consultaAplicativoTecnico().get_embedded().getAplicativotecnico();
        
        for(Entry<String,List<SheetRemedy>> e : apRemedy.entrySet()){
           boolean tieneAT = false;
           for(AplicativoTecnico at : listaAplicativoTecnico){
        	   if(at.getCodigoOrbis().equals(e.getKey())){
        		  tieneAT = true;
         		  List<AplicativoRemedyXAT> remedy = databaseConn.consultaRemedyXAT(at.get_id()).get_embedded().getAplicativoremedyxat();
        		  if(!remedy.isEmpty()){
        		     for(AplicativoRemedyXAT r: remedy){
        		        databaseConn.borraRemedyXAt(r.get_id());
        		     } 
        		  }
        		  for(SheetRemedy s: e.getValue()){
        			  AplicativoRemedyXAT remxat = new AplicativoRemedyXAT();
              		  Random rnd = new Random();
             	      idGenerada = getId() + String.valueOf(rnd.nextInt(999));
      	              while(idGenerada.equals(idInsertada)){
      	        	    idGenerada = getId();
      	              }
      	              remxat.set_id(idGenerada);
        		      remxat.setIdAplicativoTecnico(at.get_id());
        		      remxat.setNombreRemedy(s.getAplicacionRemedy());
        		      remxat.setModificado(0);
        		      databaseConn.agregaRemedyXAT(remxat);
        		      idInsertada = idGenerada;
               	   	  System.out.println("AT: " + at.getCodigoOrbis() + " Afectado");
        		  }
        	      at.setaRemedy(1);
        	      databaseConn.actualizaAplicativoTecnico(at, at.get_id());
        	      actualizados+=1;
        	      break;
        	   }
           }	
           if (!tieneAT) {
        	   for(SheetRemedy s: e.getValue()){
	     		  ErrorArchivo errorAR = new ErrorArchivo();
	     	      errorAR.setFila(s.getFila()+"");
	         	  errorAR.setIdentificador(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_ID_AT) + " " + e.getKey());
	         	  errorAR.setCampo("Código Orbis");
	         	  errorAR.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ERROR));
	         	  errorAR.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_AR));
	    		  listaReporteAR.add(errorAR);
    		  }        	   
           }
        }
        
        String archivoReporte = peticion.getRuta() + prefix + peticion.getArchivoA().replace(".xlsx", ".csv");
        ErrorArchivo repAR = new ErrorArchivo();
        repAR.setFila("N/A");
        repAR.setIdentificador("N/A");
        repAR.setCampo("N/A");
        repAR.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_REPORTE));
        repAR.setMensaje("Aplicativos Tecnicos Afectados: " + actualizados);
        
        listaReporteAR.addAll(vc.getArRechazados());
        listaReporteAR.add(0,repAR);
         
        generaReporte(archivoReporte,listaReporteAR);     
        File archivoA = new File(peticion.getRuta() + peticion.getArchivoA());
        archivoA.delete();
    
        resp.setTimestamp(new DateTime().toString());
        resp.setStatus("200");
        resp.setMessage("Terminado Exitosamente");
        System.out.println("====> Carga Remedy Finalizada <====");
        
        File archivoErroresA = new File(archivoReporte);
        byte[] doc_bytes_A = null;
        try {
        	doc_bytes_A = FileUtils.readFileToByteArray(archivoErroresA);
        } catch (IOException e) {
            e.printStackTrace();
        }
        resp.setNombreA(prefix + peticion.getArchivoA().replace(".xlsx", ".csv"));
        resp.setArchivoErroresA(doc_bytes_A);
        archivoErroresA.delete();
        
        return resp;
	}       
	
	/**
	 * Carga masivamente documento de Infraestructura.
	 * @param peticion Peticion con nombre de archivo y su ubicacion.
	 * @return Respuesta con estatus de carga, mensaje de error si aplica y archivo de alertas-errores.
	 */
	@RequestMapping(value = "/cargacmdb", method = RequestMethod.PUT)
	public @ResponseBody Object cargacmdb(@RequestBody Peticion peticion){
		Respuesta resp = new Respuesta();

		String ruta = peticion.getRuta() + peticion.getArchivoA();
        String id = null;
        String idGenerada = null;
        String idInsertada = null;
        String prefix = getPrefix();
        int infAgregados = 0;
        int wasAgregados = 0;
        int webAgregados = 0;
        int infActualizados = 0;
        int wasActualizados = 0;
        int webActualizados = 0;
        boolean actualiza = false;
        
        LectorArchivo la = new LectorArchivo();
        ProcesaCMDB pc = new ProcesaCMDB();

        List<ErrorArchivo> listaReporte = new ArrayList<>();
        List<AplicativoTecnico> AT = databaseConn.consultaAplicativoTecnico().get_embedded().getAplicativotecnico();
        List<Software> software = databaseConn.consultaSoftware().get_embedded().getSoftware();
        List<InfraestructuraStatus> infraestructuraStatus = databaseConn.consultaListadoCatInfraestructuraStatus().get_embedded().getInfraestructurastatus();
        
        la.leerArchivoCMDB(ruta, AT, software, infraestructuraStatus);
       
        pc.obtieneInfraestructura(AT,la.getApplicationData(),la.getServidoresLogicos(),
        		                      la.getServidoresAmbientesPrevios(),la.getDB(),la.getDBAP(),
        		                      la.getWAS(), la.getWASP(),la.getWEB(), la.getWEBP(), infraestructuraStatus);
        
        List<Servidor> servidores = pc.getInfraestructura();
        List<Servidor> listaServidores = databaseConn.consultaServidor().get_embedded().getServidor();
        
        for(Servidor i : servidores){ 	
            if(i.getScid().contains("_")) {
            	   String[] sc = i.getScid().split("_");
                i.setScid(sc[1]);
            }
            for(Servidor a: listaServidores){
         	   if(i.getScid().equals(a.getScid()) &&
         		  i.getIdAplicativoTecnico().equals(a.getIdAplicativoTecnico()) &&
         		  i.getNombreServidor().equals(a.getNombreServidor())){
         		   a.setExisteEnArchivo(true);
         		   actualiza = true;
         		   id = a.get_id();
         		   break;
         	   }
            }
            if(actualiza){
				i.set_id(id);
				databaseConn.actualizaServidor(i, id);
				infActualizados+=1;
				System.out.println("Servidor: " + i.getScid() + " Actualizado");
            }else {
				Random rnd = new Random();
				idGenerada = getId() + String.valueOf(rnd.nextInt(999));
				while(idGenerada.equals(idInsertada)){
					idGenerada = getId();
				}
				i.set_id(idGenerada);
				databaseConn.agregaServidor(i);
				infAgregados+=1;
				idInsertada = idGenerada;
				System.out.println("Servidor: " + i.getScid() + " Agregado");
            }
            actualiza = false;
            id = null;
        }
        
        ErrorArchivo repInf = new ErrorArchivo();
        repInf.setFila("N/A");
        repInf.setIdentificador("N/A");
        repInf.setCampo("N/A");
        repInf.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_REPORTE));
        repInf.setMensaje("Infraestructura: " + infAgregados + " Agregados y " + infActualizados + " Actualizados");
        listaReporte.add(repInf);

        List<Was> was = pc.getWAS();   
        List<Was> listaWas = databaseConn.consultaWas().get_embedded().getWas();

        for(Was w: was){ 	
        	if(w.getScid().contains("_")) {
        		String[] sc = w.getScid().split("_");
        		w.setScid(sc[1]);
        	}
        	for(Servidor i : servidores){
        		String llaveSer = i.getNombreServidor() + i.getScid();
        		String lwas = w.getNombreServidor()+w.getScid(); 
        		if(lwas.equals(llaveSer)){
        			w.setIdServidor(i.get_id());
        			break;
        		}
        	}
            for(Was a: listaWas){
         	   if(w.getIdServidor().equals(a.getIdServidor()) &&
         		  w.getNombreWAS().equals(a.getNombreWAS())){
         		   a.setExisteEnArchivo(true);
         		   actualiza = true;
         		   id = a.get_id();
         		   break;
         	   }
            }
            if(actualiza){
				w.set_id(id);
				databaseConn.actualizaWas(w, id);
				wasActualizados+=1;
				System.out.println("Was: " + w.getScid() + " Actualizado");
            }else {
				Random rnd = new Random();
				idGenerada = getId() + String.valueOf(rnd.nextInt(999));
				while(idGenerada.equals(idInsertada)){
					idGenerada = getId();
				}
				w.set_id(idGenerada);
				databaseConn.agregaWas(w);
				wasAgregados+=1;
				idInsertada = idGenerada;
				System.out.println("Was: " + w.getScid() + " Agregado");
            }
            actualiza = false;
            id = null;
        }
        
        ErrorArchivo repWas = new ErrorArchivo();
        repWas.setFila("N/A");
        repWas.setIdentificador("N/A");
        repWas.setCampo("N/A");
        repWas.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_REPORTE));
        repWas.setMensaje("Was: " + wasAgregados + " Agregados y " + wasActualizados + " Actualizados");
        listaReporte.add(repWas);
        
        List<Web> web = pc.getWEB();
        List<Web> listaWeb = databaseConn.consultaWeb().get_embedded().getWeb();

        for(Web wb: web){     
           if(wb.getScid().contains("_")) {
              String[] sc = wb.getScid().split("_");
              wb.setScid(sc[1]);
           }           
           for(Servidor i : servidores){
        	   String llaveSer = i.getNombreServidor() + i.getScid();
        	   String lwas = wb.getNombreServidor()+wb.getScid(); 
				  if(lwas.equals(llaveSer)){
				     wb.setIdServidor(i.get_id());
				     break;
				  }
           }
           for(Web a: listaWeb){
         	  if(wb.getIdServidor().equals(a.getIdServidor()) &&
                 wb.getNombreWEB().equals(a.getNombreWEB())){
         		  a.setExisteEnArchivo(true);
         		  actualiza = true;
         		  id = a.get_id();
         		  break;
         	  }
           }
           if(actualiza){
				wb.set_id(id);
				databaseConn.actualizaWeb(wb, id);
				webActualizados+=1;
				System.out.println("Web: " + wb.getScid() + " Actualizado");
           }else {
				Random rnd = new Random();
				idGenerada = getId() + String.valueOf(rnd.nextInt(999));
				while(idGenerada.equals(idInsertada)){
					idGenerada = getId();
				}
				wb.set_id(idGenerada);
				databaseConn.agregaWeb(wb);
				webAgregados+=1;
				idInsertada = idGenerada;
				System.out.println("Web: " + wb.getScid() + " Agregado");
           }
           actualiza = false;
           id = null;
        }
        
        ErrorArchivo repWeb = new ErrorArchivo();
        repWeb.setFila("N/A");
        repWeb.setIdentificador("N/A");
        repWeb.setCampo("N/A");
        repWeb.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_REPORTE));
        repWeb.setMensaje("Web: " + webAgregados + " Agregados y " + webActualizados + " Actualizados");
        listaReporte.add(repWeb);        
        listaReporte.addAll(pc.getServRechazados());
        
        for(Servidor a: listaServidores){
        	if (!a.isExisteEnArchivo()) {
        		a.setEstatusServidor((long) 1);
                if (a.getNombreDB() != null) {
            		a.setEstatusDB((long) 1);
                }
        		databaseConn.actualizaServidor(a, a.get_id());
        	}
        }
        
        for(Was a: listaWas){
        	if (!a.isExisteEnArchivo()) {
        		a.setEstatusWAS((long) 1);
				databaseConn.actualizaWas(a, a.get_id());
        	}
        }
        
        for(Web a: listaWeb){
        	if (!a.isExisteEnArchivo()) {
        		a.setEstatusWEB((long) 1);
				databaseConn.actualizaWeb(a, a.get_id());
        	}
        }
        
        String archivoReporte = peticion.getRuta() + prefix + peticion.getArchivoA().replace(".xlsx", ".csv");
        
        generaReporte(archivoReporte,listaReporte); 
        File archivoA = new File(peticion.getRuta() + peticion.getArchivoA());
        archivoA.delete();
        
        resp.setTimestamp(new DateTime().toString());
        resp.setStatus("200");
        resp.setMessage("Terminado Exitosamente");
        System.out.println("====> Carga CMDB Finalizada <====");
        
        File archivoErroresA = new File(archivoReporte);
        byte[] doc_bytes_A = null;
        try {
        	doc_bytes_A = FileUtils.readFileToByteArray(archivoErroresA);
        } catch (IOException e) {
            e.printStackTrace();
        }
        resp.setNombreA(prefix + peticion.getArchivoA().replace(".xlsx", ".csv"));
        resp.setArchivoErroresA(doc_bytes_A);
        archivoErroresA.delete();
        
		return resp;
	}
	
	
  
}