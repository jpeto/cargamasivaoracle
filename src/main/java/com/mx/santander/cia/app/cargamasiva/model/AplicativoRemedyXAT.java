package com.mx.santander.cia.app.cargamasiva.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AplicativoRemedyXAT {

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String _id;
	private String idAplicativoTecnico;
	private String nombreRemedy;
	private int modificado;
	
	public String get_id() {
		return _id;
	}
	public void set_id(String _id) {
		this._id = _id;
	}
	public String getIdAplicativoTecnico() {
		return idAplicativoTecnico;
	}
	public void setIdAplicativoTecnico(String idAplicativoTecnico) {
		this.idAplicativoTecnico = idAplicativoTecnico;
	}
	public String getNombreRemedy() {
		return nombreRemedy;
	}
	public void setNombreRemedy(String nombreRemedy) {
		this.nombreRemedy = nombreRemedy;
	}
	public int getModificado() {
		return modificado;
	}
	public void setModificado(int modificado) {
		this.modificado = modificado;
	}
	
}
