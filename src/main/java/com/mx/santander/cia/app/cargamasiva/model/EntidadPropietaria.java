package com.mx.santander.cia.app.cargamasiva.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EntidadPropietaria {
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Long _id;
	private String entidadPropietariaAp;
	private int estatus;
	

	public int getEstatus() {
		return estatus;
	}
	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}
	public String getEntidadPropietariaAp() {
		return entidadPropietariaAp;
	}
	public void setEntidadPropietariaAp(String entidadPropietariaAp) {
		this.entidadPropietariaAp = entidadPropietariaAp;
	}
	public Long get_id() {
		return _id;
	}
	public void set_id(Long _id) {
		this._id = _id;
	}
	
}
