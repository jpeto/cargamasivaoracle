package com.mx.santander.cia.app.cargamasiva.model;

import com.fasterxml.jackson.annotation.JsonInclude;

public class Subsistema {
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Long _id;
	private String subArea;
	private String codigoSubsistema;
	private int estatus;
	public Long get_id() {
		return _id;
	}
	public void set_id(Long _id) {
		this._id = _id;
	}
	public String getSubArea() {
		return subArea;
	}
	public void setSubArea(String subArea) {
		this.subArea = subArea;
	}
	public String getCodigoSubsistema() {
		return codigoSubsistema;
	}
	public void setCodigoSubsistema(String codigoSubsistema) {
		this.codigoSubsistema = codigoSubsistema;
	}
	public int getEstatus() {
		return estatus;
	}
	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}
}