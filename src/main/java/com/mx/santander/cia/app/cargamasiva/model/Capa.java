package com.mx.santander.cia.app.cargamasiva.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
@JsonIgnoreProperties(ignoreUnknown = true)
public class Capa {
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Long _id;
	private String dominio;
	private String codigoCapa;
	private int estatus;
	public Long get_id() {
		return _id;
	}
	public void set_id(Long _id) {
		this._id = _id;
	}
	public String getDominio() {
		return dominio;
	}
	public void setDominio(String dominio) {
		this.dominio = dominio;
	}
	public String getCodigoCapa() {
		return codigoCapa;
	}
	public void setCodigoCapa(String codigoCapa) {
		this.codigoCapa = codigoCapa;
	}
	public int getEstatus() {
		return estatus;
	}
	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}
}