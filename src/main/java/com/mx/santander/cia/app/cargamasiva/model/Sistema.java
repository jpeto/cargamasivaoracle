package com.mx.santander.cia.app.cargamasiva.model;

import com.fasterxml.jackson.annotation.JsonInclude;

public class Sistema {
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Long _id;
	private String areaFuncional;
	private String codigoSistema;
	private int estatus;
	public Long get_id() {
		return _id;
	}
	public void set_id(Long _id) {
		this._id = _id;
	}
	public String getAreaFuncional() {
		return areaFuncional;
	}
	public void setAreaFuncional(String areaFuncional) {
		this.areaFuncional = areaFuncional;
	}
	public String getCodigoSistema() {
		return codigoSistema;
	}
	public void setCodigoSistema(String codigoSistema) {
		this.codigoSistema = codigoSistema;
	}
	public int getEstatus() {
		return estatus;
	}
	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}
}