package com.mx.santander.cia.app.cargamasiva.vo;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import org.joda.time.DateTime;

import com.mx.santander.cia.app.cargamasiva.model.ErrorArchivo;
import com.mx.santander.cia.app.cargamasiva.properties.Propiedades;


public class ArchivoUtil {
	
	/**
	 * Genera nombre para archivo de Alertas-Errores con la fecha de la carga.
	 * @param N/A.
	 * @return Nombre de archivo de Alertas-Errores con la fecha de la carga.
	 */
	public static String getPrefix(){
		DateTime today = new DateTime();
        String fecha = today.toString("yyyy-MMM-dd");
        return "[Alertas-Errores_" + fecha + "]_";
	}
	
	/**
	 * Genera reporte de Alertas-Errores.
	 * @param archivoReporte Nombre de reporte.
	 * @param listaReporte Lista de alertas o errores.
	 */
	public static void generaReporte(String archivoReporte,List<ErrorArchivo> listaReporte){
        File archivo = null;
        FileWriter fw = null;
        BufferedWriter bw = null;
        try{
        	archivo = new File (archivoReporte);
        	
        	if (!archivo.exists()) {
        		archivo.createNewFile();
            }
            fw = new FileWriter (archivo);
            bw = new BufferedWriter(fw);
            bw.write(Propiedades.getInstance().getPropiedad(Propiedades.HEADER_FILA)+","+
            		Propiedades.getInstance().getPropiedad(Propiedades.HEADER_IDENTIFICADOR)+","+
            		Propiedades.getInstance().getPropiedad(Propiedades.HEADER_TIPO_MSG)+","+
            		Propiedades.getInstance().getPropiedad(Propiedades.HEADER_CAMPO)+","+
            		Propiedades.getInstance().getPropiedad(Propiedades.HEADER_MENSAJE));
            bw.newLine();
            for(ErrorArchivo rep: listaReporte){
            	bw.write(rep.getFila()+","+rep.getIdentificador()+","+rep.getTipoMsj()+","+rep.getCampo()+","+rep.getMensaje());
            	bw.newLine();
            }
        	
        }catch (Exception e) {
        	e.printStackTrace();
        }finally {
        	try {
        		if(bw != null) {
        		   bw.close();
        		}
        		if(fw != null) {
				   fw.close();
        		}
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
	}

}
