package com.mx.santander.cia.app.cargamasiva.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StadiumBian {

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Long idDominio;
	private String dominio;
	private Long idBianNivel2;
	private String bian_nivel2;
	private Long idBianNivel3;
	private String bian_nivel3;
	private Long idBianAfr;
	private String bian_afr;
	private Long idBianCriticidad;
	private String bian_criticidad;
	private Long estatus;

	public Long getIdDominio() {
		return idDominio;
	}

	public void setIdDominio(Long idDominio) {
		this.idDominio = idDominio;
	}

	public Long getIdBianNivel2() {
		return idBianNivel2;
	}

	public void setIdBianNivel2(Long idBianNivel2) {
		this.idBianNivel2 = idBianNivel2;
	}

	public Long getIdBianNivel3() {
		return idBianNivel3;
	}

	public void setIdBianNivel3(Long idBianNivel3) {
		this.idBianNivel3 = idBianNivel3;
	}

	public Long getIdBianAfr() {
		return idBianAfr;
	}

	public void setIdBianAfr(Long idBianAfr) {
		this.idBianAfr = idBianAfr;
	}

	public Long getIdBianCriticidad() {
		return idBianCriticidad;
	}

	public void setIdBianCriticidad(Long idBianCriticidad) {
		this.idBianCriticidad = idBianCriticidad;
	}

	public Long getEstatus() {
		return estatus;
	}

	public void setEstatus(Long estatus) {
		this.estatus = estatus;
	}

	public String getDominio() {
		return dominio;
	}

	public void setDominio(String dominio) {
		this.dominio = dominio;
	}

	public String getBian_nivel2() {
		return bian_nivel2;
	}

	public void setBian_nivel2(String bian_nivel2) {
		this.bian_nivel2 = bian_nivel2;
	}

	public String getBian_nivel3() {
		return bian_nivel3;
	}

	public void setBian_nivel3(String bian_nivel3) {
		this.bian_nivel3 = bian_nivel3;
	}

	public String getBian_afr() {
		return bian_afr;
	}

	public void setBian_afr(String bian_afr) {
		this.bian_afr = bian_afr;
	}

	public String getBian_criticidad() {
		return bian_criticidad;
	}

	public void setBian_criticidad(String bian_criticidad) {
		this.bian_criticidad = bian_criticidad;
	}
}
