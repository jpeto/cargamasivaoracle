package com.mx.santander.cia.app.cargamasiva.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.mx.santander.cia.app.cargamasiva.properties.Propiedades;

public class SheetServidores implements Comparable<SheetServidores>{
	
    /** Service Component ID */
	private String svcId;
	/** Compañia */
	private String lsCompany;
	/** Nombre del Servidor */
	private String ciName;
	/** Ambiente */
	private String funtionalEnviroment;
	/** Status */
	private String lsStatus;
	/** Funcion Servidor */
	private String lsFunction;
	/** Plataforma */
	private String platform;
	/** Sistema Operativo */
	private String operatingSystem;
	/** Numero Fila*/
    private int fila;
	/** EOL */
    private String eol;
	private List<ErrorArchivo> erroresMapeo;
	
	/**
	 * Mapeo de una fila del excel de Infraestructura (hoja de Servidores y Servidores Amb. Prev.) y validacion por campo.
	 * @param row Fila de Servidor.
	 * @param software Lista de Softwares.
	 */
	public void mapeaValores(Row row, List<Software> software){
    	this.erroresMapeo = new ArrayList<>();
		this.ciName = valida(row.getCell(1), "CI Name");              
		this.lsCompany = valida(row.getCell(2), "LS Company");           
		this.funtionalEnviroment = valida(row.getCell(3), "FUNTIONALENVIROMENT"); 
		this.lsStatus = valida(row.getCell(4), "LS Status");            
		this.lsFunction = valida(row.getCell(6), "LS Function");          
		this.platform = valida(row.getCell(7), "PLATFORM");            
		this.operatingSystem = valida(row.getCell(8), "OPERATINGSYSTEM");     
		this.svcId = valida(row.getCell(22), "SC ID");
		this.eol = validaEOL(this.operatingSystem, software);
	}
	
	/**
	 * Valida valor de una celda de excel: campo vacio, generando las alertas correspondientes.
	 * @param c Celda de excel a validar.
	 * @param campo Nombre del campo de Servidor a validar.
	 * @return Valor de celda de excel.
	 */
    private String valida(Cell c, String campo) {
    	Object o = c;
    	ErrorArchivo error = new ErrorArchivo();
    	if (o != null ){
    		if(!c.getStringCellValue().equals("Sin Informar") && !c.getStringCellValue().equals("Sin Información") 
    			&& !c.getStringCellValue().equals("Sin Datos") && !c.getStringCellValue().trim().equals("")){
    			return c.getStringCellValue();    			
    		}
    	}    	
    	error.setCampo(campo);
    	error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
    	error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VACIO));
		this.erroresMapeo.add(error);
		return null;
    }
    
    /**
	 * Valida existencia de Sistema operativo y Version en catalogo de Software para obtener EoL, generando las alertas correspondientes.
	 * @param operatingSystem String con sistema operativo y version.
	 * @param software Lista de Softwares.
	 * @return String con valor de End Of Life.
	 */
    private String validaEOL(String operatingSystem, List<Software> software) {
    	ErrorArchivo error = new ErrorArchivo();    	
    	if (operatingSystem != null) {
        	boolean esta = false;
    		for(Software s: software){
        		if(s.getName()!=null && s.getVersion()!=null) {
        			String llaveOS = s.getName() + " " + s.getVersion();
        				if(operatingSystem.equals(llaveOS)){
        					esta=true;
        					if(s.getObsolescence()!=null) {
        						return s.getObsolescence();
        					}
        					else {
        						error.setCampo("End of life");
        						error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
        						error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_VACIO));
        						this.erroresMapeo.add(error);
        						return null;					
        					}

        				}
        		}
        	}
        	if(!esta) {
        		error.setCampo("OPERATINGSYSTEM");
        		error.setTipoMsj(Propiedades.getInstance().getPropiedad(Propiedades.TIPO_MSG_ALERTA));
        		error.setMensaje(Propiedades.getInstance().getPropiedad(Propiedades.MENSAJE_CATALOGO));
    			this.erroresMapeo.add(error);
    			return null;
        	} 
    	}
		return null;
    }
    
    /**
	 * Compara 2 Servidores, para determinar si son iguales por Nombre y Service Component.
	 * @param o Servidor.
	 * @return Entero indicando si los Servidores son o no iguales.
	 */
    @Override
    public int compareTo(SheetServidores o) {
    	String a = new String(this.ciName+this.svcId);
        String b = new String(o.ciName+o.getSvcId());
        return a.compareTo(b);
    }

	public String getSvcId() {
		return svcId;
	}

	public void setSvcId(String svcId) {
		this.svcId = svcId;
	}

	public String getLsCompany() {
		return lsCompany;
	}

	public void setLsCompany(String lsCompany) {
		this.lsCompany = lsCompany;
	}

	public String getCiName() {
		return ciName;
	}

	public void setCiName(String ciName) {
		this.ciName = ciName;
	}

	public String getFuntionalEnviroment() {
		return funtionalEnviroment;
	}

	public void setFuntionalEnviroment(String funtionalEnviroment) {
		this.funtionalEnviroment = funtionalEnviroment;
	}

	public String getLsStatus() {
		return lsStatus;
	}

	public void setLsStatus(String lsStatus) {
		this.lsStatus = lsStatus;
	}

	public String getLsFunction() {
		return lsFunction;
	}

	public void setLsFunction(String lsFunction) {
		this.lsFunction = lsFunction;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public String getOperatingSystem() {
		return operatingSystem;
	}

	public void setOperatingSystem(String operatingSystem) {
		this.operatingSystem = operatingSystem;
	}

	public int getFila() {
		return fila;
	}

	public void setFila(int fila) {
		this.fila = fila;
	}

	public List<ErrorArchivo> getErroresMapeo() {
		return erroresMapeo;
	}

	public void setErroresMapeo(List<ErrorArchivo> erroresMapeo) {
		this.erroresMapeo = erroresMapeo;
	}

	public String getEol() {
		return eol;
	}

	public void setEol(String eol) {
		this.eol = eol;
	}


}
