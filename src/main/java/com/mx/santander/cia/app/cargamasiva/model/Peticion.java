package com.mx.santander.cia.app.cargamasiva.model;

public class Peticion {

	private String ruta;
	private String archivoA;
	private String idArchivoA;
	private String archivoB;
	private String idArchivoB;
	
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	public String getArchivoA() {
		return archivoA;
	}
	public void setArchivoA(String archivoA) {
		this.archivoA = archivoA;
	}
	public String getArchivoB() {
		return archivoB;
	}
	public void setArchivoB(String archivoB) {
		this.archivoB = archivoB;
	}
	public String getIdArchivoA() {
		return idArchivoA;
	}
	public void setIdArchivoA(String idArchivoA) {
		this.idArchivoA = idArchivoA;
	}
	public String getIdArchivoB() {
		return idArchivoB;
	}
	public void setIdArchivoB(String idArchivoB) {
		this.idArchivoB = idArchivoB;
	}

}
