package com.mx.santander.cia.app.cargamasiva.list;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mx.santander.cia.app.cargamasiva.model.*;


public class Embedded {
	
	@JsonProperty("entidadmapa")
	private List <EntidadMapa> entidadmapa;
	@JsonProperty("estructuracatalogacion")
	private List <EstructuraCatalogacion> estructuracatalogacion;
	@JsonProperty("categoriaaplicativo")
	private List <CategoriaAplicativo> categoriaaplicativo;
	@JsonProperty("stadiumbian")
	private List <StadiumBian> stadiumbian;
	@JsonProperty("paquetebian")
	private List <PaqueteBian> paquetebian;
	@JsonProperty("granularidadbian")
	private List <GranularidadBian> granularidadbian;
	@JsonProperty("aplicativofuncional")
	private List <AplicativoFuncional> aplicativofuncional;
	@JsonProperty("entidadpropietaria")
	private List <EntidadPropietaria> entidadpropietaria;
	@JsonProperty("tipologiasoftware")
	private List <TipologiaSoftware> tipologiasoftware;
	@JsonProperty("identificadorcpd")
	private List <IdentificadorCpd> identificadorcpd;
	@JsonProperty("criticidadservicio")
	private List <CriticidadServicio> criticidadservicio;
	@JsonProperty("aplicativotecnico")
	private List <AplicativoTecnico> aplicativotecnico;
	@JsonProperty("servidor")
	private List <Servidor> servidor;
	@JsonProperty("was")
	private List <Was> was;
	@JsonProperty("web")
	private List <Web> web;
	@JsonProperty("infraestructurastatus")
	private List <InfraestructuraStatus> infraestructurastatus;
	@JsonProperty("software")
	private List <Software> software;
	@JsonProperty("atxaf")
	private List <ATXAF> atxaf;
	@JsonProperty("aplicativoremedyxat")
	private List <AplicativoRemedyXAT> aplicativoremedyxat;
	@JsonProperty("entidad")
	private List <Entidad> entidad;
	@JsonProperty("mapa")
	private List <Mapa> mapa;
	@JsonProperty("dominio")
	private List <Dominio> dominio;
	@JsonProperty("bian_nivel2")
	private List <BianNivel2> bian_nivel2;
	@JsonProperty("bian_nivel3")
	private List <BianNivel3> bian_nivel3;
	@JsonProperty("bian_afr")
	private List <BianAfr> bian_afr;
	@JsonProperty("bian_criticidad")
	private List <BianCriticidad> bian_criticidad;
	@JsonProperty("capa")
	private List <Capa> capa;
	@JsonProperty("sistema")
	private List <Sistema> sistema;
	@JsonProperty("subsistema")
	private List <Subsistema> subsistema;
	@JsonProperty("documento")
	private List <Documento> documento;
	@JsonProperty("stadiumbianxaf")
	private List <StadiumBianXAf> stadiumbianxaf;
	@JsonProperty("responsablesxaplicativofuncional")
	private List <Responsables> responsables;
	
	public List<EntidadMapa> getEntidadmapa() {
		return entidadmapa;
	}
	public void setEntidadmapa(List<EntidadMapa> entidadmapa) {
		this.entidadmapa = entidadmapa;
	}
	public List<EstructuraCatalogacion> getEstructuracatalogacion() {
		return estructuracatalogacion;
	}
	public void setEstructuracatalogacion(List<EstructuraCatalogacion> estructuracatalogacion) {
		this.estructuracatalogacion = estructuracatalogacion;
	}
	public List<CategoriaAplicativo> getCategoriaaplicativo() {
		return categoriaaplicativo;
	}
	public void setCategoriaaplicativo(List<CategoriaAplicativo> categoriaaplicativo) {
		this.categoriaaplicativo = categoriaaplicativo;
	}
	public List<StadiumBian> getStadiumbian() {
		return stadiumbian;
	}
	public void setStadiumbian(List<StadiumBian> stadiumbian) {
		this.stadiumbian = stadiumbian;
	}
	public List<PaqueteBian> getPaquetebian() {
		return paquetebian;
	}
	public void setPaquetebian(List<PaqueteBian> paquetebian) {
		this.paquetebian = paquetebian;
	}
	public List<GranularidadBian> getGranularidadbian() {
		return granularidadbian;
	}
	public void setGranularidadbian(List<GranularidadBian> granularidadbian) {
		this.granularidadbian = granularidadbian;
	}
	public List<AplicativoFuncional> getAplicativofuncional() {
		return aplicativofuncional;
	}
	public void setAplicativofuncional(List<AplicativoFuncional> aplicativofuncional) {
		this.aplicativofuncional = aplicativofuncional;
	}
	public List<EntidadPropietaria> getEntidadpropietaria() {
		return entidadpropietaria;
	}
	public void setEntidadpropietaria(List<EntidadPropietaria> entidadpropietaria) {
		this.entidadpropietaria = entidadpropietaria;
	}
	public List<TipologiaSoftware> getTipologiasoftware() {
		return tipologiasoftware;
	}
	public void setTipologiasoftware(List<TipologiaSoftware> tipologiasoftware) {
		this.tipologiasoftware = tipologiasoftware;
	}
	public List<IdentificadorCpd> getIdentificadorcpd() {
		return identificadorcpd;
	}
	public void setIdentificadorcpd(List<IdentificadorCpd> identificadorcpd) {
		this.identificadorcpd = identificadorcpd;
	}
	public List<CriticidadServicio> getCriticidadservicio() {
		return criticidadservicio;
	}
	public void setCriticidadservicio(List<CriticidadServicio> criticidadservicio) {
		this.criticidadservicio = criticidadservicio;
	}
	public List<AplicativoTecnico> getAplicativotecnico() {
		return aplicativotecnico;
	}
	public void setAplicativotecnico(List<AplicativoTecnico> aplicativotecnico) {
		this.aplicativotecnico = aplicativotecnico;
	}
	public List<Servidor> getServidor() {
		return servidor;
	}
	public void setServidor(List<Servidor> servidor) {
		this.servidor = servidor;
	}
	public List<Was> getWas() {
		return was;
	}
	public void setWas(List<Was> was) {
		this.was = was;
	}
	public List<Web> getWeb() {
		return web;
	}
	public void setWeb(List<Web> web) {
		this.web = web;
	}
	public List<Software> getSoftware() {
		return software;
	}
	public void setSoftware(List<Software> software) {
		this.software = software;
	}
	public List<ATXAF> getAtxaf() {
		return atxaf;
	}
	public void setAtxaf(List<ATXAF> atxaf) {
		this.atxaf = atxaf;
	}
	public List<AplicativoRemedyXAT> getAplicativoremedyxat() {
		return aplicativoremedyxat;
	}
	public void setAplicativoremedyxat(List<AplicativoRemedyXAT> aplicativoremedyxat) {
		this.aplicativoremedyxat = aplicativoremedyxat;
	}

	public List<Entidad> getEntidad() {
		return entidad;
	}

	public void setEntidad(List<Entidad> entidad) {
		this.entidad = entidad;
	}

	public List<Mapa> getMapa() {
		return mapa;
	}

	public void setMapa(List<Mapa> mapa) {
		this.mapa = mapa;
	}

	public List<Dominio> getDominio() {
		return dominio;
	}

	public void setDominio(List<Dominio> dominio) {
		this.dominio = dominio;
	}

	public List<BianNivel2> getBian_nivel2() {
		return bian_nivel2;
	}

	public void setBian_nivel2(List<BianNivel2> bian_nivel2) {
		this.bian_nivel2 = bian_nivel2;
	}

	public List<BianNivel3> getBian_nivel3() {
		return bian_nivel3;
	}

	public void setBian_nivel3(List<BianNivel3> bian_nivel3) {
		this.bian_nivel3 = bian_nivel3;
	}

	public List<BianAfr> getBian_afr() {
		return bian_afr;
	}

	public void setBian_afr(List<BianAfr> bian_afr) {
		this.bian_afr = bian_afr;
	}

	public List<BianCriticidad> getBian_criticidad() {
		return bian_criticidad;
	}

	public void setBian_criticidad(List<BianCriticidad> bian_criticidad) {
		this.bian_criticidad = bian_criticidad;
	}

	public List <Capa> getCapa() {
		return capa;
	}

	public void setCapa(List <Capa> capa) {
		this.capa = capa;
	}

	public List <Sistema> getSistema() {
		return sistema;
	}

	public void setSistema(List <Sistema> sistema) {
		this.sistema = sistema;
	}

	public List <Subsistema> getSubsistema() {
		return subsistema;
	}

	public void setSubsistema(List <Subsistema> subsistema) {
		this.subsistema = subsistema;
	}
	public List <Documento> getDocumento() {
		return documento;
	}
	public void setDocumento(List <Documento> documento) {
		this.documento = documento;
	}
	public List <StadiumBianXAf> getStadiumbianxaf() {
		return stadiumbianxaf;
	}
	public void setStadiumbianxaf(List <StadiumBianXAf> stadiumbianxaf) {
		this.stadiumbianxaf = stadiumbianxaf;
	}
	public List <InfraestructuraStatus> getInfraestructurastatus() {
		return infraestructurastatus;
	}
	public void setInfraestructurastatus(List <InfraestructuraStatus> infraestructurastatus) {
		this.infraestructurastatus = infraestructurastatus;
	}

	public List <Responsables> getResponsables() {
		return responsables;
	}

	public void setResponsables(List <Responsables> responsables) {
		this.responsables = responsables;
	}
	
}
